Stronghold
==========


Software Development

Javier Carro

Application icon

Matthew Bice — Matthew@IheartNY.com — www.Iheart.com/Xicons

Document icon

Toolbar icons

Translations

Software components

RBSplitView — Rainer Brackerhoff — http://www.brockerhoff.net


Stronghols is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

Stronghold is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with Stronghold; if not, If not, see <http://www.gnu.org/licenses/> or write to the Free Software Foundation, Inc., Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
