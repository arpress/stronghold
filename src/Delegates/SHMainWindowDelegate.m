//
//  MainWindowDelegate.m
//  Stronghold
//
//  Created by Javier Carro on 13/05/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "SHMainWindowDelegate.h"


@implementation SHMainWindowDelegate

- (NSUndoManager*)windowWillReturnUndoManager:(NSWindow*)in_window {

	return(self);

}

- (BOOL)canUndo {

	if( [sqlDbController isLocked] ) {
		return(NO);
	} else {
		return([super canUndo]);
	}
}

- (BOOL)canRedo {

	if( [sqlDbController isLocked] ) {
		return(NO);
	} else {
		return([super canRedo]);
	}
}

@end
