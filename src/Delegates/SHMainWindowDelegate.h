//
//  MainWindowDelegate.h
//  Stronghold
//
//  Created by Javier Carro on 13/05/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SHSqlDbController.h"

@interface SHMainWindowDelegate : NSUndoManager {
	
	IBOutlet SHSqlDbController* sqlDbController;
}

- (NSUndoManager*)windowWillReturnUndoManager:(NSWindow*)in_window;

- (BOOL)canUndo;

- (BOOL)canRedo;

@end
