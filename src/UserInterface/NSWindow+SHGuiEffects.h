/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/
 
#import <Cocoa/Cocoa.h>


/*!
    @category    NSWindow ( SHGuiEffects )
	
    @abstract    Adds the following GUI effects to the NSWindow:
		
				 <ul>
					<li>
						<i>Shake</i>: Shakes the windows to report an invalid input, as it is done
						by the OSX login window.
					</li>
				 </ul>
    
	@discussion  <#(comprehensive description)#>
*/
@interface NSWindow ( SHGuiEffects )

/*!
    @method     shake

    @abstract   Shakes the window horizontally to report th euser an invalid input as done by the OSX
				login window.

    @discussion Moves the window in steps of 5 pixels horizontally without animation and refreshing the display 
				in each step.

*/
- (void)shake;

@end
