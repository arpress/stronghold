/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/
 
#import "NSWindow+SHGuiEffects.h"
#import "typedef.h"

@implementation NSWindow ( SHGuiEffects )

- (void) shake {
	NSRect frame = [self frame];
	
	float windowOriginShiftArray[4] = {-5.0 , 5.0, 5.0, -5.0};
	
	ulong32 i;
	ulong32 j;
		
	for ( i=0; i < 3; i++ ) {
	
		for( j=0; j < 4; j++ ) {
			frame.origin.x += windowOriginShiftArray[j];
			[self setFrame:frame display:YES animate:NO];
		}
	}
}

@end
