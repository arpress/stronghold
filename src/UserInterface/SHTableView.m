/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/
 
#import "SHTableView.h"

@implementation SHTableView

- (void)keyDown:(NSEvent*)in_event {

	// Get the characters that genrated the event.
	NSString *eventCharacters = [in_event characters];
	
	// Check that the string is not empty.
	if ([eventCharacters length]) {
		
		// Check the first character of the string to verify if we have to treat it.
		switch ([eventCharacters characterAtIndex:0]) {
		
			// Process the backspace character.
			case NSDeleteCharacter: {
							
				id tableViewDelegate = [self delegate];
		
				if( [tableViewDelegate respondsToSelector: @selector(delete:)] ) {
				
					[tableViewDelegate delete: self];
				
				} else {
					[super keyDown:in_event];
				}
				
			} break;
			
			// Process the return character.
			case NSCarriageReturnCharacter: {
				// TODO insert code to handle the edition of the cells with the intro character
			} break;

			// Else invoke the original NSTableView method.
			default:
				[super keyDown:in_event];
				break;					
		}
	}
}

@end
