/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/
 
#import <Cocoa/Cocoa.h>

/*!
    @class       SHTableView
	 
    @superclass  NSTableView
    
	@abstract    Subclass of NSTableView adding key bindings.
    
	@discussion  Overwrites the keyDown method of NSTableView to handle the following keys:
				
				 <ul>
					 <li>
						<i>Backspace (NSDeleteCharacter)</i>: Deletes the selected row, calling the delete: method of the 
						<i>delegate</i>.
					 </li>
				 </ul>
				
				 For all the other keys the original keyDown from NSTableView method is invoked.
				
	@updated	 09/03/2008
*/
@interface SHTableView : NSTableView {
}

/*!
    @method     keyDown:

    @abstract   Overwites the original NSTableView keyDown method.

    @discussion Overwrites the keyDown method of NSTableView to handle the following keys: 
	
				 <ul>
					 <li>
						<i>Backspace (NSDeleteCharacter)</i>: Deletes the selected row, calling the delete: method of the 
						<i>delegate</i>.
					 </li>
				 </ul>
				
				For all the other keys the original keyDown from NSTableView method is invoked.
*/
- (void)keyDown:(NSEvent*)in_event;

@end
