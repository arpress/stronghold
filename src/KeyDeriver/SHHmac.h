/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*****************************************************************************
 *	
 *	Acknowledgements:
 *
 *	The code in this file is based in the code published by Brian Gladman
 *	in his file encryption example with the following copyright notice.
 * 
 * --------------------------------------------------------------------------
 * Copyright (c) 2002, Dr Brian Gladman <                 >, Worcester, UK.
 * All rights reserved.
 *
 * LICENSE TERMS
 *
 * The free distribution and use of this software in both source and binary
 * form is allowed (with or without changes) provided that:
 *
 *  1. distributions of this source code include the above copyright
 *     notice, this list of conditions and the following disclaimer;
 *
 *  2. distributions in binary form include the above copyright
 *     notice, this list of conditions and the following disclaimer
 *     in the documentation and/or other associated materials;
 *
 *  3. the copyright holder's name is not used to endorse products
 *     built using this software without specific written permission.
 *
 * ALTERNATIVELY, provided that this notice is retained in full, this product
 * may be distributed under the terms of the GNU General Public License (GPL),
 * in which case the provisions of the GPL apply INSTEAD OF those given above.
 *
 * DISCLAIMER
 *
 * This software is provided 'as is' with no explicit or implied warranties
 * in respect of its properties, including, but not limited to, correctness
 * and/or fitness for purpose.
 * --------------------------------------------------------------------------
 *
 * Issue Date: 24/01/2003
 *
 * This is an implementation of HMAC, the FIPS standard keyed hash function.
 *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision: 71 $
 *
 *	$Author: jcarro $
 *
 *	$Date: 2008-03-30 03:24:17 +0200 (Sun, 30 Mar 2008) $
 *
 ****************************************************************************/

#import "SHSha1.h"

#define IN_BLOCK_LENGTH     SHA1_BLOCK_SIZE
#define OUT_BLOCK_LENGTH    SHA1_DIGEST_SIZE

/*!
    @class		SHHMac
	
    @abstract   Context to compute the HMAC-SHA1.

*/
@interface SHHMac : NSObject {

	/*!
		@var		key
		
		@abstract	Holds the key used
	*/
	NSMutableData* key;

	/*!
		@var		sha1
		
		@abstract	Holds the key used
	*/
	SHSha1* sha1;		

	/*!
		@var		inDataMode
		
		@abstract	Holds the key used
	*/
	BOOL inDataMode;
}

/*!
    @method     init

    @abstract   Initialises the internal attributes of the class, key and sha1, and then calls the method reset.
*/
- init;

/*!
    @method     dealloc

    @abstract   Liberates the internal class attributes key and sha1.
*/
- (void)dealloc;

/*!
    @method     description

    @abstract   Returns the desciption of the object as an NSString.

    @result     NSString composed by the key, the sha1 and the inDataMode.
*/
- (NSString*)description;

/*!
    @method     reset

    @abstract   Resets the SHHmac object to the initial state.

    @discussion Clears the key, resets the sha1 and set inDataMode to NO.

*/
- (void)reset;

/*!
    @method     resetWithMac:

    @abstract   Resets the SHHmac object with the values of the <i>in_hmac object</i>.

    @param      in_hmac HMAC-SHA1 context to use the final state of the reset.

*/
- (void)resetWithMac:(SHHMac*)in_hmac;

/*!
    @method     key

    @abstract   Returns the value of the key attribute.

    @result     Value of the key attribute.

*/
- (NSMutableData*)key;

/*!
    @method     sha1

    @abstract   Returns the pointer to the SHA-1 context.
	
    @result		Pointer to the SHA-1 context.

*/
- (SHSha1*)sha1;

/*!
    @method     inDataMode

    @abstract   Returns the value of the inDataMode attribute.

    @result     Value of the inDataMode attribute.

*/
- (BOOL)inDataMode;

/*!
    @method     addKey:

    @abstract   Adds the key to the HMAC-SHA1 context.

    @discussion The function can not be called when the HMAC-SHA1 context is in data mode
				(can be called when inDataMode == NO).
				The key can be added several times.
				If the addKey is called when the context is in data mode then an exception is raised.

    @param      in_key Key to add to the HMAC-SHA1 context.

*/
- (void)addKey:(NSData*)in_key;

/*!
    @method     addData:

    @abstract   Adds the data to the HMAC-SHA1 context.
	
    @discussion This function to add data can be called multiple times.
				The first time the function the key input phase is terminated (inDataMode becomes YES).

    @param      in_data Data to be added to the HMAC-SHA1 context.

*/
- (void)addData:(NSData*)in_data;

/*!
    @method     endWithMac:

    @abstract   Closes the SHHmac context and computes the HMAC-SHA1 with the key and data 
				added to the context.

    @discussion If the SHmac context is not in data mode, then a nil data is added to the context
				and the context change into data mode.

    @param      out_mac Contains the computed HMAC-SHA1.

*/
- (void)endWithMac:(NSMutableData*)out_mac;


@end