/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*****************************************************************************
 *	
 *	Acknowledgements:
 *
 *	The code in this file is based in the code published by Brian Gladman
 *	in his file encryption example with the following copyright notice.
 * 
 * --------------------------------------------------------------------------
 * Copyright (c) 2002, Dr Brian Gladman <                 >, Worcester, UK.
 * All rights reserved.
 *
 * LICENSE TERMS
 *
 * The free distribution and use of this software in both source and binary
 * form is allowed (with or without changes) provided that:
 *
 *  1. distributions of this source code include the above copyright
 *     notice, this list of conditions and the following disclaimer;
 *
 *  2. distributions in binary form include the above copyright
 *     notice, this list of conditions and the following disclaimer
 *     in the documentation and/or other associated materials;
 *
 *  3. the copyright holder's name is not used to endorse products
 *     built using this software without specific written permission.
 *
 * ALTERNATIVELY, provided that this notice is retained in full, this product
 * may be distributed under the terms of the GNU General Public License (GPL),
 * in which case the provisions of the GPL apply INSTEAD OF those given above.
 *
 * DISCLAIMER
 *
 * This software is provided 'as is' with no explicit or implied warranties
 * in respect of its properties, including, but not limited to, correctness
 * and/or fitness for purpose.
 * --------------------------------------------------------------------------
 *
 * Issue Date: 24/01/2003
 *
 * This is an implementation of RFC2898, which specifies key derivation from
 * a password and a salt value.
 *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision: 71 $
 *
 *	$Author: jcarro $
 *
 *	$Date: 2008-03-30 03:24:17 +0200 (Sun, 30 Mar 2008) $
 *
 ****************************************************************************/

#import "SHKeyDeriver.h"

@implementation SHKeyDeriver

- init {
	
	[super init];
	
	hmac[0] = [[SHHMac alloc] init];
	hmac[1] = [[SHHMac alloc] init];
	
	uu = [[NSMutableData alloc] init];
	ux = [[NSMutableData alloc] initWithCapacity: OUT_BLOCK_LENGTH];
	
	return(self);
}

- (void) dealloc {

	[hmac[0] release];
	[hmac[1] release];
	
	[uu release];
	[ux release];
	
	[super dealloc];
}

- (NSString*) description {
	NSString* result;
	
	result = [NSString stringWithFormat: @"uu %@\n\
		ux %@\n\
		hmac %@", uu, ux, hmac];
	
	return (result);
}

- (void) deriveKeyWithPasswdHMac: (SHHMac*) in_passwdHMac
				salt: (NSData*) in_salt iterations: (ulong32) in_iterations
				key: (NSMutableData*) out_key keyLength: (ulong32) out_keyLength {
				
	ulong32 i;
	ulong32 j;
	ulong32 blockIndex;
	ulong32 numBlocks;		
	
	[hmac[0] reset];
	[hmac[1] reset];
			
	/* set HMAC context (c2) for password and salt      */
	[hmac[0] resetWithMac: in_passwdHMac];
	[hmac[0] addData: in_salt];
	
	/* find the number of SHA blocks in the key         */
	numBlocks = 1 + (out_keyLength - 1) / OUT_BLOCK_LENGTH;

	[out_key setLength: out_keyLength];	
	uchar* keyPtr = (uchar*) [out_key mutableBytes];

	uchar* uuPtr;
	uchar* uxPtr;
	uxPtr = (uchar*) [ux mutableBytes];

	/* for each block in key */
	for(blockIndex = 0; blockIndex < numBlocks; blockIndex++)  {

		memset(uxPtr, 0, OUT_BLOCK_LENGTH);

		/* set HMAC context (c3) for password and salt  */
		[hmac[1] resetWithMac: hmac[0]];

		/* enter additional data for 1st block into uu  */
		[uu setLength: 4];

		uuPtr = (uchar*) [uu mutableBytes];			
		
		uuPtr[0] = (uchar)((blockIndex + 1) >> 24);
		uuPtr[1] = (uchar)((blockIndex + 1) >> 16);
		uuPtr[2] = (uchar)((blockIndex + 1) >> 8);
		uuPtr[3] = (uchar)(blockIndex + 1);
		
		/* this is the key mixing iteration         */
		for(i = 0; i < in_iterations; i++) {

			/* add previous round data to HMAC      */
			[hmac[1] addData: uu];

			/* obtain HMAC for uu[]                 */
			[hmac[1] endWithMac: uu];
			uuPtr = (uchar*) [uu mutableBytes];			

			/* xor into the running xor block       */
			for(j = 0; j < OUT_BLOCK_LENGTH; j++) {
					uxPtr[j] ^= uuPtr[j];
			}

			/* set HMAC context (c3) for password   */
			[hmac[1] resetWithMac: in_passwdHMac];
		}

		/* compile key blocks into the key output   */
		ulong32 hashIndex = 0; 
		ulong32 keyIndex = blockIndex * OUT_BLOCK_LENGTH;
		while( (hashIndex < OUT_BLOCK_LENGTH) &&
					 (keyIndex < out_keyLength) ) {
			keyPtr[keyIndex++] = uxPtr[hashIndex++];
		}
	}
}

- (void) deriveKeyWithPasswd: (NSData*) in_passwd
				salt: (NSData*) in_salt iterations: (ulong32) in_iterations
				key: (NSMutableData*) out_key keyLength: (ulong32) out_keyLength {

	/* set HMAC context for the password */
	SHHMac* passwordHmac;
	
	passwordHmac = [[SHHMac alloc] init];
	[passwordHmac addKey: in_passwd];
	
	[self deriveKeyWithPasswdHMac: passwordHmac salt: in_salt iterations: in_iterations 
		key: out_key keyLength: out_keyLength];
		
	[passwordHmac release];
}

@end