/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*****************************************************************************
 *	
 *	Acknowledgements:
 *
 *	The code in this file is based in the code published by Brian Gladman
 *	in his file encryption example with the following copyright notice.
 * 
 * --------------------------------------------------------------------------
 * Copyright (c) 2002, Dr Brian Gladman <                 >, Worcester, UK.
 * All rights reserved.
 *
 * LICENSE TERMS
 *
 * The free distribution and use of this software in both source and binary
 * form is allowed (with or without changes) provided that:
 *
 *  1. distributions of this source code include the above copyright
 *     notice, this list of conditions and the following disclaimer;
 *
 *  2. distributions in binary form include the above copyright
 *     notice, this list of conditions and the following disclaimer
 *     in the documentation and/or other associated materials;
 *
 *  3. the copyright holder's name is not used to endorse products
 *     built using this software without specific written permission.
 *
 * ALTERNATIVELY, provided that this notice is retained in full, this product
 * may be distributed under the terms of the GNU General Public License (GPL),
 * in which case the provisions of the GPL apply INSTEAD OF those given above.
 *
 * DISCLAIMER
 *
 * This software is provided 'as is' with no explicit or implied warranties
 * in respect of its properties, including, but not limited to, correctness
 * and/or fitness for purpose.
 * --------------------------------------------------------------------------
 *
 * Issue Date: 24/01/2003
 *
 * This is an implementation of HMAC, the FIPS standard keyed hash function.
 *
 *****************************************************************************/

/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*****************************************************************************
 *	
 *	Acknowledgements:
 *
 *	The code in this file is based in the code published by Brian Gladman
 *	in his file encryption example with the following copyright notice.
 * 
 * --------------------------------------------------------------------------
 * Copyright (c) 2002, Dr Brian Gladman <                 >, Worcester, UK.
 * All rights reserved.
 *
 * LICENSE TERMS
 *
 * The free distribution and use of this software in both source and binary
 * form is allowed (with or without changes) provided that:
 *
 *  1. distributions of this source code include the above copyright
 *     notice, this list of conditions and the following disclaimer;
 *
 *  2. distributions in binary form include the above copyright
 *     notice, this list of conditions and the following disclaimer
 *     in the documentation and/or other associated materials;
 *
 *  3. the copyright holder's name is not used to endorse products
 *     built using this software without specific written permission.
 *
 * ALTERNATIVELY, provided that this notice is retained in full, this product
 * may be distributed under the terms of the GNU General Public License (GPL),
 * in which case the provisions of the GPL apply INSTEAD OF those given above.
 *
 * DISCLAIMER
 *
 * This software is provided 'as is' with no explicit or implied warranties
 * in respect of its properties, including, but not limited to, correctness
 * and/or fitness for purpose.
 * --------------------------------------------------------------------------
 *
 * Issue Date: 24/01/2003
 *
 * This is an implementation of HMAC, the FIPS standard keyed hash function.
 *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision: 71 $
 *
 *	$Author: jcarro $
 *
 *	$Date: 2008-03-30 03:24:17 +0200 (Sun, 30 Mar 2008) $
 *
 ****************************************************************************/

#import "SHHmac.h"

@implementation SHHMac

- init {
	[super init];

	sha1 = [[SHSha1 alloc] init];
	key = [[NSMutableData alloc] init];

	[self reset];
	
	return(self);
}

- (void) dealloc {
	[sha1 release];
	[key  release];
	[super dealloc];
}

- (NSString*) description {
	NSString* result;
	
	result = [NSString stringWithFormat: @"key %@\n\
		sha1 %@\n\
		inDataMode %d", key, sha1, (ulong32) inDataMode];
		
	return (result);
}

/* initialise the HMAC context to zero */
-(void) reset {
	[sha1 reset];
	[key setLength: 0];
	inDataMode = NO;
}

- (void) resetWithMac: (SHHMac *) in_hmac {
	[sha1 resetWithSha: [in_hmac sha1]];
	
	[key setLength: 128];
	[key setData: [in_hmac key]];
	inDataMode = [in_hmac inDataMode];
}

- (NSMutableData*) key {
	return(key);
}

- (SHSha1*) sha1 {
	return(sha1);
}

- (BOOL) inDataMode {
	return(inDataMode);
}

/* input the HMAC key (can be called multiple times)    */
-(void) addKey: (NSData*) in_keyData {

	ulong32 keyDataLength = [in_keyData length];
	ulong32 keyLength = [key length];
	
	/* error if further key input   */
	/* is attempted in data mode    */ 
	if( inDataMode ) {
		// TODO: Raise an exception.
	}

	/* if the key has to be hashed  */
	if(keyLength + keyDataLength > IN_BLOCK_LENGTH) {

		/* if the hash has not yet been */
		/* started, initialise it and   */
		/* hash stored key characters   */	
		if(keyLength <= IN_BLOCK_LENGTH) {
			[sha1 reset];
			[sha1 hashWithData: key];
		}

		/* hash long key data into hash */
		[sha1 hashWithData: in_keyData];
	}	else {
		/* otherwise store key data     */
		[key appendData: in_keyData];
	}
}

/* input the HMAC data (can be called multiple times) - */
/* note that this call terminates the key input phase   */
- (void) addData: (NSData*) in_data {
	ulong32 i;

	ulong32 keyLength = [key length];

	/* if not yet in data phase */
	if( !inDataMode ) {
		
		/* if key is being hashed   */
		/* complete the hash and    */
		/* store the result as the  */
		/* key and set new length   */
		if(keyLength > IN_BLOCK_LENGTH) {		                                
			[sha1 endWithHashValue: key];         
			keyLength = [key length];
		}

		/* pad the key if necessary */
		if ( (IN_BLOCK_LENGTH - keyLength) > 0 ) {
			[key setLength: IN_BLOCK_LENGTH];
		}

		/* xor ipad into key value  */
		ulong32 * keyDwordAray = (ulong32*) [key mutableBytes];
		for(i = 0; i < (IN_BLOCK_LENGTH / 4); i++) {
			keyDwordAray[i] ^= 0x36363636;
		}

		/* and start hash operation */
		[sha1 reset];
		[sha1 hashWithData: key];
		
		inDataMode = YES;
	}

	/* hash the data (if any)*/
	if(in_data) {
		[sha1 hashWithData: in_data];
	}
}

/* compute and output the MAC value */
- (void) endWithMac: (NSMutableData*) out_mac {

	if( !inDataMode ) {
		[self addData: nil];
	}
	
	/* complete the inner hash      */
	[sha1 endWithHashValue: out_mac];
	
	/* set outer key value using opad and removing ipad */
	ulong32* keyDwordArray = (ulong32*) [key mutableBytes];
	ulong32 i;
	
	for(i = 0; i < (IN_BLOCK_LENGTH >> 2); ++i) {
		keyDwordArray[i] ^= 0x36363636 ^ 0x5c5c5c5c;
	}

	/* perform the outer hash operation */
	[sha1 reset];
	[sha1 hashWithData: key];
	[sha1 hashWithData: out_mac];
	[sha1 endWithHashValue: out_mac];
}

@end