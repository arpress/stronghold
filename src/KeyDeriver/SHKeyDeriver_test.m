/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision: 71 $
 *
 *	$Author: jcarro $
 *
 *	$Date: 2008-03-30 03:24:17 +0200 (Sun, 30 Mar 2008) $
 *
 ****************************************************************************/

#import <Foundation/Foundation.h>

#import "SHKeyDeriver.h"

int main (int argc, const char * argv[]) {

	SHKeyDeriver *keyDeriver;
	NSData* passwordData;
	NSData* saltData;
	NSMutableData* keyData;
	
	char* passwordString = "password";
	char  saltString[4] = {0x12, 0x34, 0x56, 0x78};
	
	passwordData = [[NSData alloc] initWithBytes: (void*)passwordString length: strlen(passwordString)];
	saltData = [[NSData alloc] initWithBytes: (void*)saltString length: 4];
	keyData = [[NSMutableData alloc] init];
	
	keyDeriver = [[SHKeyDeriver alloc] init];
	
	[keyDeriver deriveKeyWithPasswd: passwordData salt: saltData
		iterations: 5 key: keyData keyLength: 56];
	
	uchar* keyDataPtr;
	
	keyDataPtr = (uchar*) [keyData mutableBytes];
	
	NSMutableString *stringKey;
	
	stringKey = [[NSMutableString alloc] init];
	
	ulong32 i;
	
	for(i=0; i < 56 ; i++) {
		[stringKey appendFormat: @"%02X", keyDataPtr[i]];
	}
	
	NSString *expectedKey = @"5C75CEF01A960DF74CB6B49B9E38E6B53B1180E32FF7E0DDAACA8F8127F69F4F1DC82F482DDB1A0ACA90CB80B92E909EDF6D1F29C51DC501";

	if ([stringKey compare: expectedKey]) {
		NSLog(@"Failure expected key: %@", expectedKey);
		NSLog(@"and obtained key:     %@", stringKey);
		NSLog(@"are different."); 
	} else {
		NSLog(@"Successfull expected key: %@", expectedKey);
		NSLog(@"and obtained key:         %@", stringKey);
		NSLog(@"are identical."); 
	}
		
	return(0);
}