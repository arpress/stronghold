/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision: 71 $
 *
 *	$Author: jcarro $
 *
 *	$Date: 2008-03-30 03:24:17 +0200 (Sun, 30 Mar 2008) $
 *
 ****************************************************************************/

#import <Foundation/Foundation.h>

#import "SHHmac.h"

int main (int argc, const char * argv[]) {

	SHHMac *hmac;
	NSData *keyData;
	NSData* testData;
	NSMutableData* macValue;
	
	char* keyString = "Jefe";
	char* testString = "what do ya want for nothing?";
	
	keyData = [[NSData alloc] initWithBytes: (void*)keyString length: strlen(keyString)];
	testData = [[NSData alloc] initWithBytes: (void*)testString length: strlen(testString)];

	hmac = [[SHHMac alloc] init];
	
	[hmac addKey: keyData];
	
	[hmac addData: testData];
	
	macValue = [[NSMutableData alloc] init];
	
	[hmac endWithMac: macValue];
	
	uchar* macPtr;
	
	macPtr = (uchar*) [macValue mutableBytes];
	
	NSMutableString *stringMac;
	
	stringMac = [[NSMutableString alloc] init];
	
	ulong32 i;
	ulong32 hmacLength = [macValue length];
	
	for(i=0; i < hmacLength ; i++) {
		[stringMac appendFormat: @"%02X", macPtr[i]];
	}
	
	NSString *expectedMac = @"EFFCDF6AE5EB2FA2D27416D5F184DF9C259A7C79";

	if ([stringMac compare: expectedMac]) {
		NSLog(@"Failure expected hash: %@", expectedMac);
		NSLog(@"and obtained hash:     %@", stringMac);
		NSLog(@"are different."); 
	} else {
		NSLog(@"Successfull expected hash: %@", expectedMac);
		NSLog(@"and obtained hash:         %@", stringMac);
		NSLog(@"are identical."); 
	}

	return(0);
}