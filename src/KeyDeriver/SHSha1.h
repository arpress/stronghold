/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*****************************************************************************
 *	
 *	Acknowledgements:
 *
 *	The code in this file is based in the code published by Brian Gladman
 *	in his file encryption example with the following copyright notice.
 * 
 * --------------------------------------------------------------------------
 * Copyright (c) 2002, Dr Brian Gladman <                 >, Worcester, UK.
 * All rights reserved.
 *
 * LICENSE TERMS
 *
 * The free distribution and use of this software in both source and binary
 * form is allowed (with or without changes) provided that:
 *
 *  1. distributions of this source code include the above copyright
 *     notice, this list of conditions and the following disclaimer;
 *
 *  2. distributions in binary form include the above copyright
 *     notice, this list of conditions and the following disclaimer
 *     in the documentation and/or other associated materials;
 *
 *  3. the copyright holder's name is not used to endorse products
 *     built using this software without specific written permission.
 *
 * ALTERNATIVELY, provided that this notice is retained in full, this product
 * may be distributed under the terms of the GNU General Public License (GPL),
 * in which case the provisions of the GPL apply INSTEAD OF those given above.
 *
 * DISCLAIMER
 *
 * This software is provided 'as is' with no explicit or implied warranties
 * in respect of its properties, including, but not limited to, correctness
 * and/or fitness for purpose.
 * --------------------------------------------------------------------------
 *
 * Issue Date: 24/01/2003
 *
 * This file contains the definitions needed for SHA1
 *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision: 71 $
 *
 *	$Author: jcarro $
 *
 *	$Date: 2008-03-30 03:24:17 +0200 (Sun, 30 Mar 2008) $
 *
 ****************************************************************************/
 
#import "../typedef.h"

#define SHA1_BLOCK_SIZE  64
#define SHA1_DIGEST_SIZE 20

/*!
    @class       SHSha1
	
    @superclass  NSObject
	
    @abstract    Class implementing a context to compute a SHA1 hash.
*/
@interface SHSha1 : NSObject {

   ulong32 count[2];

   ulong32 hash[5];

   ulong32 wordBuffer[16];
}

/*!
    @method     init

    @abstract   Initialises the SHA1 context.

    @discussion Initialises the SHA1 context, calling the reset method.

*/
- init;

/*!
    @method     description

    @abstract   <#(brief description)#>

    @discussion <#(comprehensive description)#>

    @result     <#(description)#>

*/
- (NSString*)description;

/*!
    @method     reset

    @abstract   Resets the internal attributes of the SHA1 context.

    @discussion Resets the count and wordBuffer to attributes to 0 and the hash array to the default values.

*/
- (void)reset;

/*!
    @method     resetWithSha:

    @abstract   Reset the SHA1 context represented by the object with the values of the in_sha context.

    @param      in_sha SHA1 context to use to copy the values from.

*/
- (void)resetWithSha:(SHSha1*)in_sha;

/*!
    @method     count

    @abstract   Returns the value of the count attribute.

    @result		Value of the count attribute.

*/
- (ulong32*)count;

/*!
    @method     hash

    @abstract   Returns a pointer to the hash array attribute.

    @result     Pointer to the hash array attribute.

*/
- (ulong32*)hash;

/*!
    @method     wordBuffer

    @abstract   Returns a pointer to the wordBuffer array attribute.

    @result     Pointer to the wordBuffer array attribute.

*/
- (ulong32*)wordBuffer;

/*!
    @method     compile

    @abstract   Compiles the SHA1 context with the current values of the attributes.

    @discussion The function is for internal use of the SHA1 context, it is called by the
				hashWithData: and the endWithHashValue: methods.

*/
- (void)compile;

/*!
    @method     hashWithData:

    @abstract   Hashes the input data <i>in_data</i> calling the compile method as needed.

    @param      in_data Data to hash in the context.

*/
- (void)hashWithData:(NSData*)in_data;

/*!
    @method     endWithHashValue:

    @abstract   Computes the SHA1 value for the context.

    @param      out_hashValue Computed SHA1 hash value.

*/
- (void)endWithHashValue:(NSMutableData*)out_hashValue;

@end