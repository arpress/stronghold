/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*****************************************************************************
 *	
 *	Acknowledgements:
 *
 *	The code in this file is based in the code published by Brian Gladman
 *	in his file encryption example with the following copyright notice.
 * 
 * --------------------------------------------------------------------------
 * Copyright (c) 2002, Dr Brian Gladman <                 >, Worcester, UK.
 * All rights reserved.
 *
 * LICENSE TERMS
 *
 * The free distribution and use of this software in both source and binary
 * form is allowed (with or without changes) provided that:
 *
 *  1. distributions of this source code include the above copyright
 *     notice, this list of conditions and the following disclaimer;
 *
 *  2. distributions in binary form include the above copyright
 *     notice, this list of conditions and the following disclaimer
 *     in the documentation and/or other associated materials;
 *
 *  3. the copyright holder's name is not used to endorse products
 *     built using this software without specific written permission.
 *
 * ALTERNATIVELY, provided that this notice is retained in full, this product
 * may be distributed under the terms of the GNU General Public License (GPL),
 * in which case the provisions of the GPL apply INSTEAD OF those given above.
 *
 * DISCLAIMER
 *
 * This software is provided 'as is' with no explicit or implied warranties
 * in respect of its properties, including, but not limited to, correctness
 * and/or fitness for purpose.
 * --------------------------------------------------------------------------
 *
 * Issue Date: 24/01/2003
 *
 * This file contains the definitions needed for SHA1
 *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision: 71 $
 *
 *	$Author: jcarro $
 *
 *	$Date: 2008-03-30 03:24:17 +0200 (Sun, 30 Mar 2008) $
 *
 ****************************************************************************/
 
#import "SHSha1.h"

#define SHA1_MASK   (SHA1_BLOCK_SIZE - 1)

/* SHA1 final padding and digest calculation  */

#ifdef __LITTLE_ENDIAN__
static ulong32  MASK[4] = {0x00000000, 0x000000ff, 0x0000ffff, 0x00ffffff};
static ulong32  BITS[4] = {0x00000080, 0x00008000, 0x00800000, 0x80000000};
#else
static ulong32  MASK[4] = {0x00000000, 0xff000000, 0xffff0000, 0xffffff00};
static ulong32  BITS[4] = {0x80000000, 0x00800000, 0x00008000, 0x00000080};
#endif

#define rotl32(x,n) (((x) << n) | ((x) >> (32 - n)))

#define ch(x,y,z)       (((x) & (y)) ^ (~(x) & (z)))
#define parity(x,y,z)   ((x) ^ (y) ^ (z))
#define maj(x,y,z)      (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))

/* A normal version as set out in the FIPS  */

#define rnd(f,k)								\
    t = a;										\
	a = rotl32(a,5) + f(b,c,d) + e + k + w[i];	\
	e = d;										\
	d = c;										\
	c = rotl32(b, 30);							\
	b = t

@implementation SHSha1

- init {
	[super init];
	
	[self reset];
	
	return(self);
}

- (NSString*) description {
	NSString* result;
	
	result = [NSString stringWithFormat: @"count <%d %d>\n\
		hash <%d %d %d %d %d>\n\
		wordBuffer <%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d>",
		count[0], count [1],
		hash[0], hash[1], hash[2], hash[3], hash[4], 
		wordBuffer[0], wordBuffer[1], wordBuffer[2], wordBuffer[3], 
		wordBuffer[4], wordBuffer[5], wordBuffer[6], wordBuffer[7], 
		wordBuffer[8], wordBuffer[9], wordBuffer[10], wordBuffer[11], 
		wordBuffer[12], wordBuffer[13], wordBuffer[14], wordBuffer[15]];

	return (result);
}

- (void) reset {
	count[0] = 0;
	count[1] = 0;

	hash[0] = 0x67452301;
	hash[1] = 0xefcdab89;
	hash[2] = 0x98badcfe;
	hash[3] = 0x10325476;
	hash[4] = 0xc3d2e1f0;
	
	memset((void*)wordBuffer, 0, 16*sizeof(ulong32));
}

- (void) resetWithSha: (SHSha1*) in_sha {
	memcpy(count, [in_sha count], 2*sizeof(ulong32));
	memcpy(hash, [in_sha hash], 5*sizeof(ulong32));
	memcpy(wordBuffer, [in_sha wordBuffer], 16*sizeof(ulong32));
}

- (ulong32*) count {
	return(count);
}

- (ulong32*) hash {
	return(hash);
}

- (ulong32*) wordBuffer {
	return(wordBuffer);
}

- (void) compile {	
	ulong32	w[80];
	ulong32	i;
	ulong32	a;
	ulong32	b;
	ulong32	c;
	ulong32	d;
	ulong32	e;
	ulong32	t;

	/* note that words are compiled from the buffer into 32-bit */
	/* words in big-endian order so an order reversal is needed */
	/* here on little endian machines                           */
	for(i = 0; i < SHA1_BLOCK_SIZE / 4; ++i) {
		w[i] = NSSwapHostIntToBig(wordBuffer[i]);
	}

	for(i = SHA1_BLOCK_SIZE / 4; i < 80; ++i) {
		w[i] =	rotl32(w[i - 3] ^ w[i - 8] ^ w[i - 14] ^ w[i - 16], 1);
	}

	a = hash[0];
	b = hash[1];
	c = hash[2];
	d = hash[3];
	e = hash[4];

	for(i = 0; i < 20; ++i) {
        rnd(ch, 0x5a827999);
	}

	for(i = 20; i < 40; ++i) {
        rnd(parity, 0x6ed9eba1);
	}

	for(i = 40; i < 60; ++i) {
        rnd(maj, 0x8f1bbcdc);
	}

	for(i = 60; i < 80; ++i) {
        rnd(parity, 0xca62c1d6);	
	}

	hash[0] += a;
	hash[1] += b;
	hash[2] += c;
	hash[3] += d;
	hash[4] += e;
}

/* SHA1 hash data in an array of bytes into hash buffer and */
/* call the hash_compile function as required.              */
- (void) hashWithData: (NSData*) in_data {
	
	ulong32 pos = (ulong32)(count[0] & SHA1_MASK);
	ulong32 space = SHA1_BLOCK_SIZE - pos;

	const uchar *dataPtr;
	ulong32 dataLen;

	dataPtr = (const uchar*)[in_data bytes];
	dataLen = [in_data length];

	if( (count[0] += dataLen) < dataLen) {
		++count[1];
	}

	/* tranfer whole blocks if possible  */
	while(dataLen >= space) {
		memcpy(((uchar*)wordBuffer) + pos, dataPtr, space);
		dataPtr += space; 
		dataLen -= space; 
		space = SHA1_BLOCK_SIZE; 
		pos = 0;
		[self compile];
	}

	// there are two cases: the above while loop entered or not
	// entered. If not entered, 'space = SHA1_BLOCK_SIZE - pos'
  // and 'len < space' so that 'len + pos < SHA1_BLOCK_SIZE'.
  // If entered, 'pos = 0', 'space = SHA1_BLOCK_SIZE' and
  // 'len < space' so that 'pos + len < SHA1_BLOCK_SIZE'. In
	// both cases, therefore, the memory copy is in the buffer.
	
  memcpy(((uchar*)wordBuffer) + pos, dataPtr, dataLen);
}

- (void) endWithHashValue: (NSMutableData*) out_hashValue {
	ulong32 i;
	
	i = (ulong32) (count[0] & SHA1_MASK);

	// mask out the rest of any partial 32-bit word and then set
	// the next byte to 0x80. On big-endian machines any bytes in
	// the buffer will be at the top end of 32 bit words, on little
	// endian machines they will be at the bottom. Hence the AND
	// and OR masks above are reversed for little endian systems
	// Note that we can always add the first padding byte at this
	// point because the buffer always has at least one empty slot
	wordBuffer[i >> 2] = (wordBuffer[i >> 2] & MASK[i & 3]) | BITS[i & 3];

	// we need 9 or more empty positions, one for the padding byte  */
	// (above) and eight for the length count.  If there is not     */
	// enough space pad and empty the buffer                        */
	if(i > SHA1_BLOCK_SIZE - 9) {
			if(i < 60) {
				wordBuffer[15] = 0;
			}
		
			[self compile];
		
			i = 0;
	} else {
		//compute a word index for the empty buffer positions
		i = (i >> 2) + 1;
	}

	while(i < 14) {
		// and zero pad all but last two positions
		wordBuffer[i++] = 0;
	}

	// assemble the eight byte counter in in big-endian format
	wordBuffer[14] = NSSwapHostIntToBig((count[1] << 3) | (count[0] >> 29));
	wordBuffer[15] = NSSwapHostIntToBig(count[0] << 3);

	[self compile];

	// extract the hash value as bytes in case the hash buffer is
	// misaligned for 32-bit words
	[out_hashValue setLength: SHA1_DIGEST_SIZE];
	
	uchar* hashValuePtr;
	hashValuePtr = (uchar*)[out_hashValue mutableBytes];
	
	//memcpy(hashValuePtr, hash, SHA1_DIGEST_SIZE);
	for(i = 0; i < SHA1_DIGEST_SIZE; ++i) {
		hashValuePtr[i] = (uchar)(hash[i >> 2] >> (8 * (~i & 3)));
	}
}

@end