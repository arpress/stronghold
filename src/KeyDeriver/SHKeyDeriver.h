/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*****************************************************************************
 *	
 *	Acknowledgements:
 *
 *	The code in this file is based in the code published by Brian Gladman
 *	in his file encryption example with the following copyright notice.
 * 
 * --------------------------------------------------------------------------
 * Copyright (c) 2002, Dr Brian Gladman <                 >, Worcester, UK.
 * All rights reserved.
 *
 * LICENSE TERMS
 *
 * The free distribution and use of this software in both source and binary
 * form is allowed (with or without changes) provided that:
 *
 *  1. distributions of this source code include the above copyright
 *     notice, this list of conditions and the following disclaimer;
 *
 *  2. distributions in binary form include the above copyright
 *     notice, this list of conditions and the following disclaimer
 *     in the documentation and/or other associated materials;
 *
 *  3. the copyright holder's name is not used to endorse products
 *     built using this software without specific written permission.
 *
 * ALTERNATIVELY, provided that this notice is retained in full, this product
 * may be distributed under the terms of the GNU General Public License (GPL),
 * in which case the provisions of the GPL apply INSTEAD OF those given above.
 *
 * DISCLAIMER
 *
 * This software is provided 'as is' with no explicit or implied warranties
 * in respect of its properties, including, but not limited to, correctness
 * and/or fitness for purpose.
 * --------------------------------------------------------------------------
 *
 * Issue Date: 24/01/2003
 *
 * This is an implementation of RFC2898, which specifies key derivation from
 * a password and a salt value.
 *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision: 71 $
 *
 *	$Author: jcarro $
 *
 *	$Date: 2008-03-30 03:24:17 +0200 (Sun, 30 Mar 2008) $
 *
 ****************************************************************************/

#import "SHHmac.h"

/*!
    @class		SHKeyDeriver
	
    @abstract   Derives the key from a text password or the HMAC of a text password.
*/
@interface SHKeyDeriver : NSObject {

	/*!
		@var		uu
		
		@abstract	Internal auxiliary buffer used for the key derivation.
	*/
	NSMutableData* uu;

	/*!
		@var		ux
		
		@abstract	Internal auxiliary buffer used for the key derivation.
	*/
	NSMutableData* ux;
	
	/*!
		@var		hmac
		
		@abstract	HMAC SHA-1 variables used to compute the password derived key.
	*/
	SHHMac* hmac[2];
}

/*!
    @method     init

    @abstract   Initalises the internal variables, uu, ux and the hmac array.

*/
- init;

/*!
    @method     dealloc

    @abstract   Liberates the internal variables uu, ux, and the hmac array.

*/
- (void)dealloc;

/*!
    @method     description

    @abstract   Prints the description of the SHKeyDeriver object (uu, ux and hmac).

    @result     String with the description of the SHKeyDeriver object.

*/
- (NSString*)description;

/*!
    @method     deriveKeyWithPasswdHMac:salt:iterations:key:keyLength:

    @abstract   Derives a key from a password HMAC.

    @discussion Derives a key from the HMAC of the password, using the salt
				provided in in_salt, performing in_iterations.
				The length of the requested key is provided in out_keyLength.
				The key is provided in out_key.

    @param      in_passwdHMac HMAC of the text password.

    @param      in_salt Salt used to derive the key.

    @param      in_iterations Number of iterations to peform on the data
				to derive the key.

    @param      out_key Derived key.

    @param      out_keyLength Requested length for the key to be computed.

*/
- (void)deriveKeyWithPasswdHMac:(SHHMac*)in_passwdHMac
	salt:(NSData*)in_salt iterations:(ulong32)in_iterations
	key:(NSMutableData*)out_key keyLength:(ulong32)out_keyLength;
				
/*!
    @method     deriveKeyWithPasswd:in_passwd:salt:iterations:key:keyLength:

    @abstract   Derives a key from a text password.

    @discussion Derives a key from the the text password, the HMAC of the
				text password is computed and the 
				deriveKeyWithPasswdHMac:salt:iterations:key:keyLength:
				method is called.

    @param      in_passwd text password.

    @param      in_salt Salt used to derive the key.

    @param      in_iterations Number of iterations to peform on the data
				to derive the key.

    @param      out_key Derived key.

    @param      out_keyLength Requested length for the key to be computed.

*/
- (void)deriveKeyWithPasswd: (NSData*) in_passwd 
	salt:(NSData*)in_salt iterations:(ulong32)in_iterations 
	key:(NSMutableData*)out_key keyLength:(ulong32)out_keyLength;

@end
