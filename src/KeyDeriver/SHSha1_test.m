/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision: 71 $
 *
 *	$Author: jcarro $
 *
 *	$Date: 2008-03-30 03:24:17 +0200 (Sun, 30 Mar 2008) $
 *
 ****************************************************************************/
 
#import <Foundation/Foundation.h>

#import "SHSha1.h"

int main (int argc, const char * argv[]) {

	SHSha1 *sha1;
	NSData* testData;
	NSMutableData* hashValue;
	
	char* testString = "The quick brown fox jumps over the lazy dog";
	
	testData = [[NSData alloc] initWithBytes: (void*)testString length: strlen(testString)];

	sha1 = [[SHSha1 alloc] init];
	
	[sha1 hashWithData: testData];
	
	hashValue = [[NSMutableData alloc] init];
	
	[sha1 endWithHashValue: hashValue];
	
	uchar* hashPtr;
	
	hashPtr = (uchar*) [hashValue mutableBytes];
	
	NSMutableString *stringHash;
	
	stringHash = [[NSMutableString alloc] init];
	
	long i;
	for(i=0; i<20; i++) {
		[stringHash appendFormat: @"%02X", hashPtr[i]];
	}
	
	NSString *expectedHash = @"2FD4E1C67A2D28FCED849EE1BB76E7391B93EB12";
	if ([stringHash compare: expectedHash]) {
		NSLog(@"Failure expected sha1: %@", expectedHash);
		NSLog(@"and obtained sha1:     %@", stringHash);
		NSLog(@"are different."); 
	} else {
		NSLog(@"Successfull expected sha1: %@", expectedHash);
		NSLog(@"and obtained sha1:         %@", stringHash);
		NSLog(@"are identical."); 
	}
	
	return(0);
}