/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*****************************************************************************
 *	
 *	Acknowledgements:
 *
 *	The code in this file is based in the code published by Brian Gladman
 *	in his file encryption example with the following copyright notice.
 * 
 * --------------------------------------------------------------------------
 * Copyright (c) 2002, Dr Brian Gladman <                 >, Worcester, UK.
 * All rights reserved.
 *
 * LICENSE TERMS
 *
 * The free distribution and use of this software in both source and binary
 * form is allowed (with or without changes) provided that:
 *
 *  1. distributions of this source code include the above copyright
 *     notice, this list of conditions and the following disclaimer;
 *
 *  2. distributions in binary form include the above copyright
 *     notice, this list of conditions and the following disclaimer
 *     in the documentation and/or other associated materials;
 *
 *  3. the copyright holder's name is not used to endorse products
 *     built using this software without specific written permission.
 *
 * ALTERNATIVELY, provided that this notice is retained in full, this product
 * may be distributed under the terms of the GNU General Public License (GPL),
 * in which case the provisions of the GPL apply INSTEAD OF those given above.
 *
 * DISCLAIMER
 *
 * This software is provided 'as is' with no explicit or implied warranties
 * in respect of its properties, including, but not limited to, correctness
 * and/or fitness for purpose.
 * --------------------------------------------------------------------------
 *
 * Issue Date: 24/01/2003
 *
 * This is the header file for an implementation of a random data pool based on
 * the use of an external entropy function (inspired by Peter Gutmann's work).
 *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#import "SHRandomPool.h"
#import "SHSha1.h"

#define MIN_MIX_ITERATIONS      20    /* min initial pool mixing iterations   */

@implementation SHRandomPool

//
//	Initialises and calls reset.
//
- init {	
	[self reset];
	
	return( self );
}

//
// clear the buffers and the counter in the context
// then preload teh reandodm bubbfer
//
- (void) reset {
	memset(randomBuffer, 0, RANDOM_POOL_SIZE);
	memset(outputBuffer, 0, RANDOM_POOL_SIZE);
	bufferPosition = 0;

	/* initialise the random data pool                      */
	[self updatePool];

	/* mix the pool a minimum number of times               */
	ulong32 i;
	for(i = 0; i < MIN_MIX_ITERATIONS; i++) {
		[self mixBuffer: randomBuffer];
	}

	/* update the pool to prime the pool output buffer      */
	[self updatePool];
}

/*
 *	Fills the randomBuffer with random data obtained using the 
 *	arc4random function.
 *
 */
- (void) loadRandomBuffer {
	ulong32 randomData;
	
	ulong32 i = 0;
	while(i+sizeof(ulong32) < RANDOM_POOL_SIZE) {
		
		randomData = arc4random();
		memcpy((void*)randomBuffer+i, (void*)&randomData, sizeof(ulong32));
		i += sizeof(ulong32);
	}
	
	if((RANDOM_POOL_SIZE-i) > 0) {
		randomData = arc4random();
		memcpy((void*)randomBuffer+i, (void*)&randomData, RANDOM_POOL_SIZE-i);		
	}
}

/* mix a random data pool using the SHA1 compression function (as   */
/* suggested by Peter Gutmann in his paper on random pools)         */
- (void) mixBuffer: (uchar*) in_buffer {

	SHSha1* sha1 = [[SHSha1 init] alloc];
	void* hashPtr = [sha1 hash];		
	void* wordBufferPtr = [sha1 wordBuffer];

	void* bufferPtr = (void*) in_buffer;

	ulong32 length;
	ulong32 i;
	for(i = 0; i < RANDOM_POOL_SIZE; i += SHA1_DIGEST_SIZE) {
	
		/* copy digest size pool block into SHA1 hash block */
		if (i == 0) {
			memcpy(hashPtr, bufferPtr + RANDOM_POOL_SIZE - SHA1_DIGEST_SIZE, SHA1_DIGEST_SIZE);
		} else {
			memcpy(hashPtr, bufferPtr + i - SHA1_DIGEST_SIZE, SHA1_DIGEST_SIZE);
		}
    
		/* copy data from pool into the SHA1 data buffer    */
		length = RANDOM_POOL_SIZE - i;

		if (length > SHA1_BLOCK_SIZE) {
			memcpy( wordBufferPtr, bufferPtr + i, SHA1_BLOCK_SIZE);
		} else {
			memcpy( wordBufferPtr, bufferPtr + i, length);
		}

		if (length < SHA1_BLOCK_SIZE) {		
			memcpy( wordBufferPtr + length, bufferPtr, SHA1_BLOCK_SIZE - length);
		}

		/* compress using the SHA1 compression function     */
		[sha1 compile];

		/* put digest size block back into the random pool  */
		memcpy((void*) bufferPtr + i, hashPtr, SHA1_DIGEST_SIZE);
	}
	
	[sha1 release];
}

/* refresh the output buffer and update the random pool by adding   */
/* entropy and remixing                                             */
- (void) updatePool {
	
	/* transfer random pool data to the output buffer   */
	memcpy(outputBuffer, randomBuffer, RANDOM_POOL_SIZE);

	/* enter entropy data into the pool */
	ulong32 randomData;
	ulong32 bytesToCopy;
	ulong32 i = 0;
	while(i < RANDOM_POOL_SIZE) {

		randomData = arc4random();	

		bytesToCopy = RANDOM_POOL_SIZE-i;	
		if ( bytesToCopy > 4 ) {
			bytesToCopy = 4;
		}
		
		memcpy((void*)randomBuffer+i, (void*) &randomData, bytesToCopy);
		
		i += bytesToCopy;
	}

	/* invert and xor the original pool data into the pool  */
	for(i = 0; i < RANDOM_POOL_SIZE; i++) {
		randomBuffer[i] ^= ~outputBuffer[i];
	}

	/* mix the pool and the output buffer   */
	[self mixBuffer: randomBuffer];
	[self mixBuffer: outputBuffer];
}

/* provide random bytes from the random data pool   */
- (void)randomData:(NSMutableData*)out_randomData length:(ulong32)in_length {

	//
	//	Set the size of the output data.
	//
	[out_randomData setLength: in_length];
	
	uchar* bufferPtr = (uchar*) [out_randomData mutableBytes];
	ulong32 outBytesToCopy = in_length;
	
	ulong32 bytesToCopy;
	while(outBytesToCopy) {
	
		/* transfer 'data_len' bytes (or the number of bytes remaining  */
        /* the pool output buffer if less) into the output*/
		bytesToCopy = RANDOM_POOL_SIZE - bufferPosition;
		if (outBytesToCopy < bytesToCopy) {
			bytesToCopy = outBytesToCopy;
		}
		
		memcpy(bufferPtr, outputBuffer+bufferPosition, bytesToCopy);

        bufferPtr += bytesToCopy;          /* update ouput buffer position pointer     */
        bufferPosition += bytesToCopy;     /* update pool output buffer pointer        */
        outBytesToCopy -= bytesToCopy;    /* update the remaining data count          */

        /* refresh the random pool if necessary */
        if(bufferPosition == RANDOM_POOL_SIZE) {
            [self updatePool]; 
			bufferPosition = 0;
        }
    }
}

@end