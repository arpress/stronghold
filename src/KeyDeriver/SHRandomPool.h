/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*****************************************************************************
 *	
 *	Acknowledgements:
 *
 *	The code in this file is based in the code published by Brian Gladman
 *	in his file encryption example with the following copyright notice.
 * 
 * --------------------------------------------------------------------------
 * Copyright (c) 2002, Dr Brian Gladman <                 >, Worcester, UK.
 * All rights reserved.
 *
 * LICENSE TERMS
 *
 * The free distribution and use of this software in both source and binary
 * form is allowed (with or without changes) provided that:
 *
 *  1. distributions of this source code include the above copyright
 *     notice, this list of conditions and the following disclaimer;
 *
 *  2. distributions in binary form include the above copyright
 *     notice, this list of conditions and the following disclaimer
 *     in the documentation and/or other associated materials;
 *
 *  3. the copyright holder's name is not used to endorse products
 *     built using this software without specific written permission.
 *
 * ALTERNATIVELY, provided that this notice is retained in full, this product
 * may be distributed under the terms of the GNU General Public License (GPL),
 * in which case the provisions of the GPL apply INSTEAD OF those given above.
 *
 * DISCLAIMER
 *
 * This software is provided 'as is' with no explicit or implied warranties
 * in respect of its properties, including, but not limited to, correctness
 * and/or fitness for purpose.
 * --------------------------------------------------------------------------
 *
 * Issue Date: 24/01/2003
 *
 * This is the header file for an implementation of a random data pool based on
 * the use of an external entropy function (inspired by Peter Gutmann's work).
 *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#include "typedef.h"
#include "SHSha1.h"

#define RANDOM_POOL_LEN    256    /* minimum random pool size             */

/* ensure that pool length is a multiple of the SHA1 digest size        */
#define RANDOM_POOL_SIZE  (SHA1_DIGEST_SIZE * (1 + (RANDOM_POOL_LEN - 1) / SHA1_DIGEST_SIZE))

/*!
    @class		SHRandomPool
  
	@abstract	Implementation of a radom data pool.
    
	@discussion	The random data pool is based on the use of an external entropy
				function (inspired by Peter Gutmann's work).
*/
@interface SHRandomPool : NSObject {

	/*!
		@var		randomBuffer
		
		@abstract	Internal variable, it holds random data as returned by the arc4random
					system function, and then mixed using the SHA-1 hash function.
		
		@discussion	The randomBuffer is filled automatically when it gets empty.
	*/
	uchar randomBuffer[RANDOM_POOL_SIZE];

	/*!
		@var		outputBuffer
		
		@abstract	Contains the random data copied from the randomBuffer that is ready to use
					by the consumer objects.

		@discussion	The outputBuffer is filled automatically when it gets empty, then the randomBuffer
					is copied to the outputBuffer and the randomBuffer is filled again.
	*/
 	uchar outputBuffer[RANDOM_POOL_SIZE];	
	
	/*!
		@var		bufferPosition
		
		@abstract	Position of the first unused byte in the outputBuffer array.
	*/
	ulong32	bufferPosition;
}

/*!
    @method     init

    @abstract   Itinialises the SHRandomPool calling the reset method.
*/
- init;

/*!
    @method     reset

    @abstract   Resets the randomBuffer and the outputBuffer and then updates both
				buffers with random data.

    @discussion Clears the randomBuffer and the outputBuffer and then calls updatePool
				to loads the randomBuffer with random data from the arc4random system
				function. Then calls updatePool and mixBuffer methods to load and mix
				both buffers.

*/
- (void)reset;

/*!
    @method     loadRandomBuffer

    @abstract   Loads the randomBuffer with random data from the data returned from the 
				arc4random system function.
				
*/
- (void)loadRandomBuffer;

/*!
    @method     mixBuffer:

    @abstract   Mixes the buffer in_buffer calling several times the SHA-1 function
				on the buffer data.

    @param      in_buffer buffer to mix using the SHA-1 function.

*/
- (void)mixBuffer:(uchar*)in_buffer;

/*!
    @method     updatePool

    @abstract   Reloads the outputBuffer from the randomBuffer data and then reloads
				the randomBuffer.
				
*/
- (void)updatePool;

/*!
    @method     randomData:length:

    @abstract   Returns in_length random bytes from the ouputBuffer.

    @discussion Returns in_length random bytes from the ouputBuffer, if the
				outputBuffer gets exhausted then the ouputBuffer is reloaded
				as needed calling the updatePool method.

    @param      out_randomData Random data returned by the function.

    @param      in_length Number of random bytes to return in the out_randomData parameter.

*/
- (void)randomData:(NSMutableData*)out_randomData length:(ulong32)in_length;

@end