/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#import <Cocoa/Cocoa.h>

#import "SHRandomPool.h"
#import "SHHmac.h"
#import "SHKeyDeriver.h"
#import "SHBlowfishCypher.h"

/*!
    @class       SHCypher
	
    @superclass  NSObject
	
    @abstract    Object used to encrypt/decrypt the data using the blofish algorithm
				 and the HMAC-SHA1 algorithm to derive the password.
	
    @discussion  The key is derived using the HMAC-SHA1 from the text password, the
				 blowfish algorithm is used to encrypt/decrypt the data.
*/
@interface SHCypher : NSObject {

	/*!
		@var		randomPool
		
		@abstract	Random pool used to obtain random data as needed by the cypher.
	*/
	SHRandomPool* randomPool;
	
	/*!
		@var		keyDeriver
		
		@abstract	Used to derive the key from the password.
	*/
	SHKeyDeriver* keyDeriver;
	
	/*!
		@var		hmac
		
		@abstract	HMAC-SHA1 context used to compute HMAC-SHA! values as needed.
	*/
	SHHMac*	hmac;
	
	/*!
		@var		blowfishCypher
		
		@abstract	Used to encrypt/decrypt the data using the blowfish algorithm.
	*/
	SHBlowfishCypher* blowfishCypher;
}

/*!
    @method     init

    @abstract   Initialises the SHCypher object allocating and intialising all the attributes.

    @discussion Inialiases and allocates the following attributes:
					<ul>
						<li>randomPool</li>
						<li>keyDeriver</li>
						<li>hmac</li>
						<li>blowfishCypher</li>
					</ul>

*/
- init;

/*!
    @method     dealloc

    @abstract   Releases the attributes allocated by the <i>init</i> method.

*/
- (void)dealloc;

/*!
    @method     encryptData:withPassword:salt:initVector:authCode:

    @abstract   Encrypts the data using the password.

    @discussion Encrypts the <i>inout_data</i> and returns and authentification code
				to verify the password before the decryption.
				The encryption is performed using the salt <i>out_salt</i> and the 
				initial vector <i>out_initVector</i>, that are returned to decrypt later
				the	data.

    @param      inout_data Data to encrypt.

    @param      in_password Input password.

    @param      out_salt Salt used for the encryption, it is computed automatically
				by the SHCypher object and it is returned to be used in the decryption
				process.

    @param      out_initVector Initial vector used for the encryption, it is computed
				automatically by the SHCypher object and it is returned to be used in
				the decryption process.

    @param      out_authCode Authentification code computed and returned by the SHCypher.
				It can be used to verify that the password is correct before decrypting
				the data.

*/
- (void)encryptData:(NSMutableData*)inout_data withPassword:(NSData*)in_password salt:(NSMutableData*)out_salt 
	initVector:(NSMutableData*)out_initVector authCode:(NSMutableData*)out_authCode;
			
/*!
    @method     decryptData:withPassword:withSalt:withInitVector:withAuthCode:

    @abstract   Decrypts the data using the password.

    @discussion Decrypts the <i>inout_data</i> and returns if the password was valid ot not before
				the decryption is performed.
				The encryption is performed using the salt <i>in_salt</i> and the 
				initial vector <i>in_initVector</i> and authenification code <i>n_autCode</i>

    @param      inout_data Data to decrypt.

    @param      in_password Input password.

    @param      in_salt Salt to be used for the decryption, it should be the value returned by the
				encryptData:withPassword:salt:initVector:authCode: method.

    @param      in_initVector Initial vector used for the decryption, it should be the value returned by the
				encryptData:withPassword:salt:initVector:authCode: method.

    @param      in_authCode Authentification code used to verify that the password is valid before decrypting
				the data.

    @result     Returns YES if the password is valid and the data has been decrypted, else NO is returned and 
				the data is not decrypted.

*/
- (BOOL)decryptData:(NSMutableData*)inout_data withPassword:(NSData*)in_password withSalt:(NSData*)in_salt
	withInitVector:(NSData*)in_initVector withAuthCode:(NSData*)in_authCode;

@end
