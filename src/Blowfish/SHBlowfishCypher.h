/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*****************************************************************************
 *	
 *	Acknowledgements:
 *
 *	The code in this file is based in the _THE BLOWFISH ENCRYPTION ALGORITHM_
 *	by Bruce Schneier 
 *
 *	Revised code--3/20/94
 *
 *	Converted to C++ class 5/96, Jim Conger
 *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision: 71 $
 *
 *	$Author: jcarro $
 *
 *	$Date: 2008-03-30 03:24:17 +0200 (Sun, 30 Mar 2008) $
 *
 ****************************************************************************/
 
#import "../typedef.h"

#define BF_SUBKEYS			18
#define BF_SBOXES			4
#define BF_SBOXES_ENTRIES	256
#define BF_MAX_PASSWD		56  // 448bits

union dword {
	ulong32 dword;
	uchar byte[4];
};
typedef union dword dword;

/*!
    @class       SHBlowfishCypher 
    
	@superclass  NSObject
	
    @abstract    Class to encrypt data using the blowfish encryption algorithm.
*/
@interface SHBlowfishCypher : NSObject {

	/*!
		@var		pa
		
		@abstract	Internal <i>P</i> array of 32 bit integers used for the encryption.
	*/	
	ulong32 pa[BF_SUBKEYS];

	/*!
		@var		sb
		
		@abstract	Internal array of <i>S-Boxes</i>  of 32 bit integers used for the encryption.
	*/
	ulong32 sb[BF_SBOXES][BF_SBOXES_ENTRIES];
}

/*!
    @method     initWithKey:

    @abstract   Intialises the encryption context with the provided key.

    @discussion If the encryption key is longer than 448 bits (56 bytes) an exception is raised.

    @param      in_key Input key, if it is greater than 448 bits (56 bytes) an exception is raised.

*/
- initWithKey:(NSData*)in_key;

/*!
    @method     reset

    @abstract   Resets the internal state of the encryption context, copying the default values for the internal
				<i>pa</i> and <i>sb</i> attribute arrays.

*/
- (void)reset;

/*!
    @method     F:

    @abstract   Computes the trabsformation of the input value <i>x</i> using the current values of the
				internal <i>sb</i> attribute array.

    @param      x Input value to transform.

    @result     Result of the transformation <i>x</i> using the current value of the internal <i>sb</i>
				attribute array.

*/
- (dword)F:(dword)x;

/*!
    @method     encryptDwordPair:

    @abstract   Encrypts the 64 bit block represented by the array <i>inout_dwordPair</i> using the
				current values of the internal arribute array <i>pa</i>.
				The function is for internal use of the blowfish encryption algorithm.				

    @param			

*/
- (void)encryptDwordPair:(dword*)inout_dwordPair;

/*!
    @method     encryptBlock:

    @abstract   Encrypts the 64 bit block in an byte-endian independent mode.

    @discussion The function calls the encryptDwordPair: method but before and after 
				the <i>inout_dwordPair</i> is converted from and to big-endian.
				The function is for internal use of the blowfish encryption algorithm.				

    @param      inout_dwordPair Array of 2 dword values (64 bits in total) to be encrypted.

*/
- (void)encryptBlock:(dword*)inout_dwordPair;

/*!
    @method     encryptData:withInitVector:

    @abstract   Encrypts/decrypts the <i>inout_textData</i> using the initial vector <i>in_initVector</i>.

    @discussion The blowfish encryption algorith uses an OFB so the input and output has the same number
				of bytes.
				The working of the algorythm in OFB mode allows to use the same function to encrypt and decrypt
				the data

    @param      inout_textData Data to encrypt/decrypt.

    @param      in_initVector Initial vector to use for the decryption.

*/
- (void)encryptData:(NSMutableData*)inout_textData withInitVector:(NSData*)in_initVector;

@end