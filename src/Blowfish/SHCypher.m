/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#import "SHCypher.h"

#define SH_KEY_ITERATIONS 1000

@implementation SHCypher

- init {
	[super init];
	
	randomPool = [[SHRandomPool alloc] init];
	hmac = [[SHHMac alloc] init];
	keyDeriver = [[SHKeyDeriver alloc] init];
	blowfishCypher = [[SHBlowfishCypher alloc] init];
	
	return (self);
}

- (void) dealloc {
	[randomPool release];
	[hmac release];
	[keyDeriver release];
	[blowfishCypher release];
	
	[super dealloc];
}

- (void) encryptData: (NSMutableData*) inout_data withPassword: (NSData*) in_password salt: (NSMutableData*) out_salt 
	initVector: (NSMutableData*) out_initVector authCode: (NSMutableData*) out_authCode {
	
	// Obtain some random data to be the salt
	[randomPool randomData: out_salt length: 28];
	
	// Ontain some random data to be the initial vector
	[randomPool randomData: out_initVector length: 8];	
	
	// Derive the key with the salt
	NSMutableData* derivedKey = [[NSMutableData alloc] init];
	[keyDeriver deriveKeyWithPasswd: in_password salt: out_salt iterations: SH_KEY_ITERATIONS key: derivedKey keyLength: 112];
	
	NSLog(@"encrypData:\n\
		in_password %@\n\
		out_salt %@\n\
		derivedKey %@", in_password, out_salt, derivedKey);
	
	// Initialise the cypher
	NSRange blowfishKeyRange;
	blowfishKeyRange.location = 0;
	blowfishKeyRange.length = 56;
	[blowfishCypher initWithKey: [derivedKey subdataWithRange: blowfishKeyRange]];
	
	// Initialise the authentification hmac
	NSRange authKeyRange;	
	authKeyRange.location = 56;
	authKeyRange.length = 56;
	[hmac reset];
	[hmac addKey: [derivedKey subdataWithRange: authKeyRange]];
	
	// Cypher the data with the blowfish.
	[blowfishCypher encryptData: inout_data withInitVector: out_initVector];
	
	// Add the cypher data to the hmac.
	[hmac addData: inout_data];
	
	// Get the authentification
	[hmac endWithMac: out_authCode];	
	
	// Release the derived key.
	[derivedKey release];
}

- (BOOL) decryptData: (NSMutableData*) inout_data withPassword: (NSData*) in_password withSalt: (NSData*) in_salt
	withInitVector: (NSData*) in_initVector withAuthCode: (NSData*) in_authCode {

	// Derive the key with the salt
	NSMutableData* derivedKey = [[NSMutableData alloc] init];
	[keyDeriver deriveKeyWithPasswd: in_password salt: in_salt iterations: SH_KEY_ITERATIONS key: derivedKey keyLength: 112];

	NSMutableData* authCode = [[NSMutableData alloc] init];
		
	// Initialise the authentification hmac
	NSRange authKeyRange;		
	authKeyRange.location = 56;
	authKeyRange.length = 56;

	[hmac reset];
	[hmac addKey: [derivedKey subdataWithRange: authKeyRange]];
	
	// Add the cypher data to the hmac.
	[hmac addData: inout_data];
	
	// Get the authentification
	[hmac endWithMac: authCode];	
	
	// The 
	BOOL result = NO;
	
	if ([in_authCode isEqual: authCode]) {
		
		// Initialise the cypher
		NSRange blowfishKeyRange = {0, 56};
		[blowfishCypher initWithKey: [derivedKey subdataWithRange: blowfishKeyRange]];

		// Cypher the data with the blowfish.
		[blowfishCypher encryptData: inout_data withInitVector: in_initVector];
		
		result = YES;
	}
	
	// Release the derived key.
	[authCode release];
	[derivedKey release];
	
	return (result);
}

@end
