/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 ************************/
//
//		$Revision$
//
//		$Author$
//
//		$Date$
//
/****************************************************************************/

#import "SHGroupName.h"

@implementation SHGroupName

- init {
	return([self initWithName: @"" identifier: [NSNumber numberWithUnsignedLongLong: 0]]);
}

- initWithName: (NSString*) in_name {
	return([self initWithName: in_name identifier: [NSNumber numberWithUnsignedLongLong: 0]]);
}

- initWithName: (NSString*) in_name identifier: (NSNumber*) in_identifier {

	[super init];
	
	[self setName: in_name];
	[self setIdentifier: in_identifier];
	
	return(self);
}

- (void) dealloc {

	[name release];
	[identifier release];	

	[super dealloc];
}

- (NSString*) name {

	return (name);

}

- (void) setName: (NSString*) in_name {

	[in_name retain];
	[name release];
	name = in_name;

}

- (NSNumber*) identifier {

	return (identifier);

}

- (void) setIdentifier: (NSNumber*) in_identifier {

	[in_identifier retain];
	[identifier release];
	identifier = in_identifier;

}

- (void) storeIn: (SHSqlDb*) in_database {
	
	// If the identifier is not 0 we update the record in the database,
	// else we insert the new record.
	SHSqlQuery *sqlQuery;
	
	if([identifier boolValue]) {

		sqlQuery = [in_database query: @"UPDATE groupTable SET name = ?1 WHERE (identifier==?2)"];
		[sqlQuery bindIndex: 1 withString: name];
		[sqlQuery bindIndex: 2 withUnsignedLong: identifier];	
		
		// Execute the query.
		[sqlQuery execute];

	} else {

		sqlQuery = [in_database query: @"INSERT INTO groupTable (name) VALUES (?1)"];
		[sqlQuery bindIndex: 1 withString: name];

		// Execute the query.
		[sqlQuery execute];
				
		[self setIdentifier: [in_database lastInsertRowId]];

	}	

}

- (void) removeFrom: (SHSqlDb*) in_database {
	
	// If the identifier is not 0 we update the record in the database,
	// else we insert the new record.
	SHSqlQuery *sqlQuery;
	
	if([identifier boolValue]) {

		sqlQuery = [in_database query: @"DELETE FROM groupTable WHERE (identifier==?1)"];
		[sqlQuery bindIndex: 1 withUnsignedLong: identifier];	
		
		// Execute the query.
		[sqlQuery execute];
		
		// Delete the record from the 
		sqlQuery = [in_database query: @"DELETE FROM groupRecordCrossTable WHERE (groupId==?1)"];
		[sqlQuery bindIndex: 1 withUnsignedLong: identifier];	

		// Execute the query.
		[sqlQuery execute];

	}	
}

- (void) loadFromQuery: (SHSqlQuery*) in_query {
	//
	// Get the identifier from the query.
	//
	[self setIdentifier: [in_query columnAsUnsignedLong: 0]];
	
	//
	// Get the name from the query.
	//
	[self setName: [in_query columnAsString: 1]];
	
}

- (BOOL) readFrom: (SHSqlDb*) in_database withId: (NSNumber*) in_identifier {
	NSString *queryString;
		
	queryString = [NSString stringWithFormat: @"SELECT identifier,name FROM groupTable WHERE (identifier==%d)",
		in_identifier];
	
	SHSqlQuery* query;
	query = [in_database query: queryString];
	
	BOOL foundRecord;
	foundRecord = [query stepAndFinalize];
	
	if(foundRecord) {

		[self loadFromQuery: query];
		[query finish];

	}
	
	return(foundRecord);
}

@end
