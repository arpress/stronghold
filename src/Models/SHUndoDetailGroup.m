//
//  SHUndoDetailGroup.m
//  Stronghold
//
//  Created by Javier Carro on 10/05/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "SHUndoDetailGroup.h"


@implementation SHUndoDetailGroup

- initWithGroupId:(NSNumber*)in_groupId recordId:(NSNumber*)in_recordId 
	groupRow:(slong32)in_groupRow recordRow:(slong32)in_recordRow {
	
	[super init];
	
	[in_groupId retain];
	groupId = in_groupId;
	
	[in_recordId retain];
	recordId = in_recordId;
	
	recordRow = in_recordRow;
	groupRow = in_groupRow;
	
	return(self);
}

- (void)dealloc {
	[groupId release];
	[recordId release];
	
	[super dealloc];
}

- (ulong32)groupRow {
	return(groupRow);
}

- (ulong32)recordRow {
	return(recordRow);
}
- (void)insertIntoDatabase:(SHSqlDb*)in_database {

	NSString* queryString = [NSString 
		stringWithFormat: @"INSERT INTO groupRecordCrossTable (recordId,groupId) VALUES (%d,%d)", 
		[recordId longValue], [groupId longValue]];

	SHSqlQuery* sqlQuery = [in_database query: queryString];
	[sqlQuery execute];
}

- (void)deleteFromDatabase:(SHSqlDb*)in_database {

	NSString* queryString = [NSString 
		stringWithFormat: @"DELETE FROM groupRecordCrossTable WHERE (recordId==%d AND groupId==%d)", 
		[recordId longValue], [groupId longValue]];

	SHSqlQuery* sqlQuery = [in_database query: queryString];
	[sqlQuery execute];

}

@end
