//
//  SHUndoGroupRecord.m
//  Stronghold
//
//  Created by Javier Carro on 08/05/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "SHUndoGroupRecord.h"

@implementation SHUndoGroupRecord

- initWithIdentifier:(NSNumber*)in_identifier recordType:(ulong32)in_recordType groupRow:(slong32)in_groupRow
	recordRow:(slong32)in_recordRow database:(SHSqlDb*)in_database {
	
	[super init];

	[in_identifier retain];
	identifier	= in_identifier;
	
	recordType	= in_recordType;
	
	recordRow	= in_recordRow;
	groupRow	= in_groupRow;
	
	NSString* tableName;
	NSString* crossTableQuery;
	
	switch(recordType) {
		case SH_UNDO_RECORD_ITEM:
			tableName = @"recordTable";
			crossTableQuery = @"SELECT groupId FROM groupRecordCrossTable WHERE (recordId == %d)";
		break;
		
		case SH_UNDO_GROUP_ITEM:
			tableName = @"groupTable";			
			crossTableQuery = @"SELECT recordId FROM groupRecordCrossTable WHERE (groupId == %d)";
		break;
		
		default:
			[NSException raise: SHGenericException format: @"Unknown undo item type %d", recordType];
		break;
	}
	
	NSString *queryString;		
	queryString = [NSString stringWithFormat: @"SELECT * FROM %@ WHERE (identifier==%d)", tableName, [identifier longLongValue]];
	
	SHSqlQuery* query;
	query = [in_database query: queryString];
	
	bool foundRecord;
	foundRecord = [query stepAndFinalize];
	
	if(!foundRecord) {
		[NSException raise: SHGenericException format: @"No record found in table %@ with identifier %d", tableName, [identifier longLongValue]];
	}
	
	ulong32 fieldIndex;
	ulong32 fieldType;
	numFields = [query columnCount];
	
	fieldNameArray	= [[NSMutableArray alloc] initWithCapacity: numFields];
	fieldTypeArray	= [[NSMutableArray alloc] initWithCapacity: numFields];
	fieldValueArray	= [[NSMutableArray alloc] initWithCapacity: numFields];

	for( fieldIndex=0; fieldIndex < numFields; fieldIndex++) {
	
		[fieldNameArray addObject: [query columnName: fieldIndex]];
		
		fieldType = [query columnType: fieldIndex];		
		[fieldTypeArray addObject: [NSNumber numberWithUnsignedInt: fieldType]];
		
		switch(fieldType) {
		
			case SQLITE_INTEGER: 
				[fieldValueArray addObject: [query columnAsLongLong: fieldIndex]];
			break;
			
			case SQLITE_FLOAT:
				[fieldValueArray addObject: [query columnAsDouble: fieldIndex]];				
			break;
			
			case SQLITE3_TEXT:
				[fieldValueArray addObject: [query columnAsString: fieldIndex]];				
			break;
			
			case SQLITE_BLOB:
				[fieldValueArray addObject: [query columnAsBlob: fieldIndex]];				
			break;
			
			case SQLITE_NULL:
				[fieldValueArray addObject: [NSNull null]];				

			break;

			default:
				[NSException raise: SHGenericException format: @"Unknown sqlite type %d", fieldType];
			break;
		}
	
	}
	
	[query finish];
	
	//
	//	Save the list of all the cross identifiers (groupId or recordId) associated to the
	//	current identifier.
	//
	crossIdentifierArray = [[NSMutableArray alloc] init];
	
	queryString = [NSString stringWithFormat: crossTableQuery, [in_identifier longLongValue]];
	query = [in_database query: queryString];
	
	foundRecord = [query stepAndFinalize];
	
	while(foundRecord) {

		[crossIdentifierArray addObject: [query columnAsLongLong: 0]];
		foundRecord = [query stepAndFinalize];		
	}

	return(self);
}

- (void)dealloc {

	[identifier release];
	
	[fieldNameArray release];
	[fieldTypeArray release];	
	[fieldValueArray release];
	
	[crossIdentifierArray release];
	
	[super dealloc];
}

- (NSNumber*)identifier {
	return(identifier);
}

- (ulong32)groupRow {
	return(groupRow);
}

- (ulong32)recordRow {
	return(recordRow);
}

- (void)insertIntoDatabase:(SHSqlDb*)in_database {

	NSString* tableName;
	NSString* crossTableCleanQuery;
	NSString* crossTableInsertQuery;

	switch(recordType) {
		case SH_UNDO_RECORD_ITEM:
			tableName = @"recordTable";
			crossTableCleanQuery = @"DELETE FROM groupRecordCrossTable WHERE (recordId == %d)";
			crossTableInsertQuery = @"INSERT OR REPLACE INTO groupRecordCrossTable (recordId, groupId) VALUES (?1, ?2)";
		break;
		
		case SH_UNDO_GROUP_ITEM:
			tableName = @"groupTable";			
			crossTableCleanQuery = @"DELETE FROM groupRecordCrossTable WHERE (groupId == %d)";
			crossTableInsertQuery = @"INSERT OR REPLACE INTO groupRecordCrossTable (groupId, recordId) VALUES (?1, ?2)";
		break;
		
		default:
			[NSException raise: SHGenericException format: @"Unknown undo item type %d", recordType];
		break;
	}
	
	NSMutableString *fieldNameString	= [[NSMutableString alloc] init];
	NSMutableString *fieldValueString	= [[NSMutableString alloc] init];
	
	ulong32 fieldIndex;
	
	for( fieldIndex=0; fieldIndex < numFields-1; fieldIndex++) {
		
		[fieldNameString appendFormat: @"%@, ",		[fieldNameArray objectAtIndex: fieldIndex]]; 
		[fieldValueString appendFormat: @"?%d, ",	(fieldIndex+1)]; 		
	}

	[fieldNameString appendFormat: @"%@",		[fieldNameArray objectAtIndex: fieldIndex]]; 
	[fieldValueString appendFormat: @"?%d",		(fieldIndex+1)]; 		

	NSString *queryString = [NSString stringWithFormat: @"INSERT OR REPLACE INTO %@ (%@) VALUES (%@)",
		tableName, fieldNameString, fieldValueString, [identifier unsignedLongLongValue]];
	
	SHSqlQuery *sqlQuery;		
	sqlQuery = [in_database query: queryString];
	
	ulong32 fieldType;
	for( fieldIndex=0; fieldIndex < numFields; fieldIndex++ ) {
		
		fieldType = [[fieldTypeArray objectAtIndex: fieldIndex] unsignedIntValue];
		
		switch(fieldType) {
		
			case SQLITE_INTEGER: 
				[sqlQuery bindIndex: fieldIndex+1 withLongLong: [fieldValueArray objectAtIndex: fieldIndex]];
			break;
			
			case SQLITE_FLOAT:
				[sqlQuery bindIndex: fieldIndex+1 withDouble: [fieldValueArray objectAtIndex: fieldIndex]];
			break;
			
			case SQLITE3_TEXT:
				[sqlQuery bindIndex: fieldIndex+1 withString: [fieldValueArray objectAtIndex: fieldIndex]];
			break;
			
			case SQLITE_BLOB:
				[sqlQuery bindIndex: fieldIndex+1 withBlob: [fieldValueArray objectAtIndex: fieldIndex]];
			break;

			case SQLITE_NULL:
				[sqlQuery bindIndexWithNull: fieldIndex+1];
			break;

			default:
				[NSException raise: SHGenericException format: @"Unknown sqlite type %d", fieldType];
			break;
		}	
	}
	
	// Execute the query.
	[sqlQuery execute];
	
	[fieldNameString release];
	[fieldValueString release];
	
	//
	//	Clean the groupRecordCrossTable and regenerate it from the crossIdentifierArray
	//
	queryString = [NSString stringWithFormat: crossTableCleanQuery, [identifier longLongValue]];
	sqlQuery = [in_database query: queryString];	
	[sqlQuery execute];
	
	
	sqlQuery = [in_database query: crossTableInsertQuery];	

	NSNumber* crossIdentifier;
	ulong32 crossItemIndex;
	ulong32	numCrossItems = [crossIdentifierArray count];
	for(crossItemIndex=0; crossItemIndex<numCrossItems; crossItemIndex++) {
		
		crossIdentifier = [crossIdentifierArray objectAtIndex: crossItemIndex];
		[sqlQuery bindIndex: 1 withLongLong: identifier];
		[sqlQuery bindIndex: 2 withLongLong: crossIdentifier];
		[sqlQuery stepAndReset];
	}
}

- (void)deleteFromDatabase:(SHSqlDb*)in_database {
	
	NSString* tableName;
	NSString* crossTableCleanQuery;
	
	switch(recordType) {
		case SH_UNDO_RECORD_ITEM:
			tableName = @"recordTable";
			crossTableCleanQuery = @"DELETE FROM groupRecordCrossTable WHERE (recordId == %d)";
		break;
		
		case SH_UNDO_GROUP_ITEM:
			tableName = @"groupTable";			
			crossTableCleanQuery = @"DELETE FROM groupRecordCrossTable WHERE (groupId == %d)";
		break;
		
		default:
			[NSException raise: SHGenericException format: @"Unknown undo item type %d", recordType];
		break;
	}

	NSString* queryString = [NSString stringWithFormat: @"DELETE FROM %@ WHERE (identifier == %d)", tableName, [identifier longLongValue]]; 
	
	SHSqlQuery *sqlQuery;		
	sqlQuery = [in_database query: queryString];

	// Execute the query.
	[sqlQuery execute];
	
	queryString = [NSString stringWithFormat: crossTableCleanQuery, [identifier longLongValue]];
	sqlQuery = [in_database query: queryString];

	// Execute the query.
	[sqlQuery execute];
}

@end
