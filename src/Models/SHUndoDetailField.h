//
//  SHUndoDetailField.h
//  Stronghold
//
//  Created by Javier Carro on 11/05/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "typedef.h"
#import "SHRecordField.h"

@interface SHUndoDetailField : NSObject {

	NSUInteger currentFieldIndex;
	
	SHRecordField* previousField;
	
	ulong32 recordRow;
	
	ulong32 groupRow;
}

- initWithCurrentFieldIndex:(NSUInteger)in_currentFieldIndex previousField:(SHRecordField*)in_previousField
	groupRow:(slong32)in_groupRow recordRow:(slong32)in_recordRow;

- (void)dealloc;

- (NSUInteger)currentFieldIndex;

- (void)setCurrentFieldIndex:(NSUInteger)in_currentFieldIndex;

- (SHRecordField*)previousField;

- (void)setPreviousField:(SHRecordField*)in_previousField;

- (ulong32)groupRow;

- (ulong32)recordRow;

@end
