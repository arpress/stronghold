//
//  SHUndoDetailField.m
//  Stronghold
//
//  Created by Javier Carro on 11/05/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "SHUndoDetailField.h"


@implementation SHUndoDetailField

- initWithCurrentFieldIndex:(NSUInteger)in_currentFieldIndex previousField:(SHRecordField*)in_previousField
	groupRow:(slong32)in_groupRow recordRow:(slong32)in_recordRow {
	
	[super init];
	
	currentFieldIndex = in_currentFieldIndex;
	[self setPreviousField: in_previousField];
	
	recordRow = in_recordRow;
	groupRow = in_groupRow;
	
	return(self);
}

- (void)dealloc {
	[previousField release];
	
	[super dealloc];
}

- (NSUInteger)currentFieldIndex {
	return(currentFieldIndex);
}

- (void)setCurrentFieldIndex:(NSUInteger)in_currentFieldIndex {
	currentFieldIndex = in_currentFieldIndex;
}

- (SHRecordField*)previousField {
	return(previousField);
}

- (void)setPreviousField: (SHRecordField*)in_previousField {
	
	[in_previousField retain];
	previousField = in_previousField;
}

- (ulong32)groupRow {
	return(groupRow);
}

- (ulong32)recordRow {
	return(recordRow);
}

@end
