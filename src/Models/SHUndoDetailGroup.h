//
//  SHUndoDetailGroup.h
//  Stronghold
//
//  Created by Javier Carro on 10/05/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "typedef.h"
#import "SHSqlDb.h"

@interface SHUndoDetailGroup : NSObject {
	
	NSNumber* groupId;

	NSNumber* recordId;
	
	ulong32 recordRow;
	
	ulong32 groupRow;
}

- initWithGroupId:(NSNumber*)in_groupId recordId:(NSNumber*)in_recordId 
	groupRow:(slong32)in_groupRow recordRow:(slong32)in_recordRow;

- (void)dealloc;

- (ulong32)groupRow;

- (ulong32)recordRow;

- (void)insertIntoDatabase:(SHSqlDb*)in_database;

- (void)deleteFromDatabase:(SHSqlDb*)in_database;

@end
