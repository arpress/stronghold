/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 ************************/
//
//		$Revision$
//
//		$Author$
//
//		$Date$
//
/****************************************************************************/

#import <Cocoa/Cocoa.h>
#import "SHSqlQuery.h"

/*!
    @class       SHSqlDb 

    @superclass  NSObject

	@coclass	 SHSqliteQuery

    @abstract    Encapsulates the sqlite database object.

    @discussion  It is a thin objective-C layer on the top of the sqlite library.
*/
@interface SHSqlDb : NSObject {
	
	/*!
		@var	dbHandler
		
		@abstract	Pointer to the sqlite3 underlying database handler.
	*/
	sqlite3* dbHandler;
}

/*!
    @method     init

    @abstract   Initialised an allocated SHSqlDb object.

    @discussion Initialise <i>dbHandler</i> to NULL.
	
	@return		Initialized SHSqlDb object.
*/
- init;

/*!
    @method     dealloc

    @abstract	Deallocates the memory occupied by the receiver.

    @discussion If the <i>dbHandler</i> attribute is not NULL the sqlite database associated
				with it is closed (see @link close close@/link).

*/
- (void)dealloc;

/*!
    @method     open:

    @abstract   Opens the database contained in the file <i>in_dbFilename</i>.

    @discussion Calls the sqlite library to open the database contained in the file pointed
				by <i>in_dbFilename</i>.
				No check is made to verify if the file exists or if its really a sqlite file.
				
				If the opening of the file is succesfull the required tables are created (if they do not exist) calling the 
				following methods:
				
				<ul>
					<li> @link initRecordTable initRecordTable@/link

					<li> @link initGroupTable initGroupTable@/link

					<li> @link initGroupRecordCrossTable initGroupRecordCrossTable@/link

					<li> @link initAuxTable initAuxTable@/link
				</ul>
				
				and the resulting sqlite3* dbHandler is stored in the <i>dbHandler</i> attribute.
				
				If the opening of the file failes the @link close@/link mehtod is called for sanity and an
				exception is thrown.
	
	@param		in_dbFilename Path of the sqilte database file to open.
	
	@trhows

*/
- (void)open:(NSString*)in_dbFilename;

/*!
    @method     close

    @abstract   Closes the sqlite database identified by <i>dbHandler</i>.

    @discussion If the <i>dbHandler</i> attribute is not NULL the <i>sqlite_close</i> function is called to close the sqlite
				database.
				
				If an error occurs (result code from sqlite_close different from SQLITE_OK or SQLITE_BUSY) then an exception
				is thrown.

	@throws

*/
- (void)close;

/*!
    @method     initRecordTable

    @abstract   Creates the <i>recordTable</i> table in the sqlite databse if it does not already exists.

    @discussion The <i>recordTable</i> has the following structure:

				<table>
				
				<tr>
				<th>Column</th>
				<th>Name</th>
				<th>Type</th>
				</tr>			
				
				<tr>
				<td>1</td>
				<td><b>identifier</b></td>
				<td>INTEGER PRIMARY KEY AUTOINCREMENT</td>
				</tr>

				<td>2</td>
				<td><b>name</b></td>
				<td>TEXT</td>
				</tr>


				<td>3</td>
				<td><b>contents</b></td>
				<td>BLOB</td>
				</tr>

				<td>4</td>
				<td><b>salt</b></td>
				<td>BLOB</td>
				</tr>

				<td>5</td>
				<td><b>initVector</b></td>
				<td>BLOB</td>
				</tr>

				<td>6</td>
				<td><b>authCode</b></td>
				<td>BLOB</td>
				</tr>

				</table>
	
	@throws

*/
- (void)initRecordTable;

/*!
    @method     initGroupTable

    @abstract   Creates the <i>groupTable</i> table in the sqlite databse if it does not already exists.

    @discussion The <i>groupTable</i> has the following structure:

				<table>
				
				<tr>
				<th>Column</th>
				<th>Name</th>
				<th>Type</th>
				</tr>			
				
				<tr>
				<td>1</td>
				<td><b>identifier</b></td>
				<td>INTEGER PRIMARY KEY AUTOINCREMENT</td>
				</tr>

				<td>2</td>
				<td><b>name</b></td>
				<td>TEXT</td>
				</tr>

				</table>
	
	@throws

*/
- (void)initGroupTable;

/*!
    @method     initGroupRecordCrossTable

    @abstract   Creates the <i>groupRecordCrossTable</i> table in the sqlite databse if it does not already exists.

    @discussion The <i>groupRecordCrossTable</i> has the following structure:

				<table>
				
				<tr>
				<th>Column</th>
				<th>Name</th>
				<th>Type</th>
				</tr>			
				
				<tr>
				<td>1</td>
				<td><b>groupId</b></td>
				<td>INTEGER</td>
				</tr>

				<td>2</td>
				<td><b>recordId</b></td>
				<td>INTEGER</td>
				</tr>

				</table>
	
	@throws

*/
- (void)initGroupRecordCrossTable;

/*!
    @method     initAuxTable

    @abstract   Creates the <i>auxTable</i> table in the sqlite databse if it does not already exists.

    @discussion The <i>auxTable</i> has the following structure:

				<table>
				
				<tr>
				<th>Column</th>
				<th>Name</th>
				<th>Type</th>
				</tr>			
				
				<tr>
				<td>1</td>
				<td><b>name</b></td>
				<td>TEXT UNIQUE PRIMARY KEY</td>
				</tr>

				<td>2</td>
				<td><b>contents</b></td>
				<td>BLOB</td>
				</tr>

				<td>3</td>
				<td><b>salt</b></td>
				<td>BLOB</td>
				</tr>

				<td>4</td>
				<td><b>initVector</b></td>
				<td>BLOB</td>
				</tr>

				<td>5</td>
				<td><b>authCode</b></td>
				<td>BLOB</td>
				</tr>

				</table>
	
	@throws

*/
- (void)initAuxTable;

/*!
    @method     createTempRecordTable

    @abstract   Creates an auxiliary <i>tempRecordTable</i> with an structure identical to the <i>recordTable</i>.

    @discussion Creates a table <i>tempRecordTable</i> that is used to store the records from <i>recordTable</i> crypted with
				a new password.
				
				In case the re-crypting of the original records fails then the table can be deleted and the original <i>recordTable</i>
				is not damaged.
				
				When the rec-rypting of the <i>cordTable</i> is finished the <i>tempRecordTable</i> is renamed to <i>recordTable</i>
				and the original <i>recordTable</i> is deleted.
				
	@trhows
				
	@seealso	deleteTempRecordTable
*/
- (void)createTempRecordTable;

/*!
    @method     deleteTempRecordTable

    @abstract   Deletes the auxiliary <i>tempRecordTable</i>.

    @discussion Deletes the table <i>tempRecordTable</i> that is used to store the records from <i>recordTable</i> crypted with
				a new password.
				
	@throws
	
	@seealso	createTempRecordTable
*/
- (void)deleteTempRecordTable;

/*!
    @method     query:

    @abstract   Compiles an the SQL query in <i>n_query</i>.
	
    @discussion The SQL query is compiled into an @link SHSqlQuery SHSqlQuery@/link.

	@param		in_query
				SQL query to be compiled.
				
    @result     Compiled SHSqlQuery.
				
	@throws		TODO.
	
	@seealso	SHSqlQuery.
*/
- (SHSqlQuery*)query:(NSString*)in_query;

/*!
    @method     lastInsertRowId

    @abstract   Returns the id of the last inserted row.

    @result     Identifier of the last inserted row as an NSNumber (from an unsigned long long).
	
	@throws
*/
- (NSNumber*)lastInsertRowId;

@end
