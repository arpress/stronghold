/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Foobar is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Foobar is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 ************************/
//
//		$Revision$
//
//		$Author$
//
//		$Date$
//
/****************************************************************************/

#import <Cocoa/Cocoa.h>
#import "SHSqlDb.h"

@interface TemplateName : NSObject {
 NSString*			name;
 NSNumber*			identifier;
 NSMutableArray*	fieldArray;
}

- (NSString*) name;
- (void) setName: (NSString*) in_name;

- (NSNumber*) identifier;
- (void) setIdentifier: (NSNumber*) in_identifier;

- (NSMutableArray*) fieldArray;
- (void) fieldArray: (NSMutableArray*) in_fieldArray;

- (void) storeIn: (SHSqlDb*) in_database;
- (void) readFrom: (SHSqlDb*) in_database withId: (ulong32) in_identifier;

@end
