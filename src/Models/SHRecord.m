/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 ************************/
//
//		$Revision$
//
//		$Author$
//
//		$Date$
//
/****************************************************************************/

#import "SHRecord.h"
#import "SHRecordField.h"
#import "SHSqlQuery.h"

@implementation SHRecord

- init {
	[super init];
	
	// Initialise the identifier to 0 to indicate that the record is not in the database.
	identifier = [[NSNumber alloc] initWithUnsignedLong: 0];
	
	// Initialise the name attribute.
	name = nil;
	
	// Initialise the fields to empty.
	fieldArray    = [[NSMutableArray alloc] init];
	
	cypher = [[SHCypher alloc] init];
	
	return(self);
}

- (void) dealloc {

	[identifier release];	
	[name release];
	[fieldArray release];
	[cypher release];
		
	[super dealloc];
}

- (NSNumber*) identifier {
	return(identifier);
}

- (void) setIdentifier: (NSNumber*) in_identifier {
	[in_identifier retain];
	[identifier release];
	identifier = in_identifier;
}

- (NSString*) name {
	return(name);
}

- (void) setName: (NSString*) in_name {
	[in_name retain];
	[name release];
	name = in_name;
}

- (NSMutableArray*) fieldArray {
	return(fieldArray);
}

- (void) setFieldArray: (NSMutableArray*) in_fieldArray {
	[in_fieldArray retain];
	[fieldArray release];
	fieldArray = in_fieldArray;
}

- (void) setFieldArrayWithData: (NSData*) in_fieldData {
	
	//
	// Get the contents of the stream as a pointer and
	// its length.
	//
	const char* fieldData;
	NSUInteger fieldDataLength;
	
	fieldDataLength = [in_fieldData length];
	fieldData = (const char*) [in_fieldData bytes];
	
	//
	// Remove all the fields in the array before adding the new fields.
	//
	[fieldArray removeAllObjects];

	//
	// Creates the diferent fields and we add them to the fieldArray.
	//	
	NSString*		fieldName;
	NSString*		fieldValue;
	SHRecordField*	recordField;

	NSUInteger offset = 0;
	while (offset < fieldDataLength) {
		fieldName = [NSString stringWithUTF8String: (fieldData + offset)];
		offset += strlen(fieldData+offset) + 1;
		
		fieldValue = [NSString stringWithUTF8String: (fieldData + offset)];
		offset += strlen(fieldData+offset) + 1;
		
		recordField = [[SHRecordField alloc] initWithName: fieldName Value: fieldValue];
		[fieldArray addObject: recordField];
		[recordField release];
	}
}

- (NSMutableData*) fieldStream {
	// Create a byte stream to hold the representation of the record data.
	NSMutableData *fieldStream = [[NSMutableData alloc] init];
	[fieldStream autorelease];
	
	NSEnumerator *arrayEnumerator = [fieldArray objectEnumerator];
	id fieldObject;

	const char* utfString;
	while (fieldObject = [arrayEnumerator nextObject]) {
		// Append the field name string including the null terminating character.
		utfString = [[fieldObject name] UTF8String];
		
		if(utfString) {
			[fieldStream appendBytes:utfString length:strlen(utfString)+1];
		}

		// Append the field name string including the null terminating character.
		utfString = [[fieldObject value] UTF8String];
		
		if(utfString) {
			[fieldStream appendBytes:utfString length:strlen(utfString)+1];
		}
	}
	
	return (fieldStream);
}

- (void)storeIn:(SHSqlDb*)in_database table:(NSString*)in_table withPasswordHash:(NSData*)in_passwordHash {

	NSMutableData* fieldStream;
	fieldStream = [self fieldStream];
	
	NSMutableData* salt			= [[NSMutableData alloc] init];
	NSMutableData* initVector	= [[NSMutableData alloc] init];
	NSMutableData* authCode		= [[NSMutableData alloc] init];

	[cypher encryptData: fieldStream withPassword: in_passwordHash salt: salt 
		initVector: initVector authCode: authCode];
	
	// If the identifier is not 0 we update the record in the database,
	// else we insert the new record.

	NSString *queryString = [NSString stringWithFormat: @"INSERT OR REPLACE INTO %@ (identifier, name, \
		contents, salt, initVector, authCode) VALUES (?1, ?2, ?3, ?4, ?5, ?6)", in_table];

	SHSqlQuery *sqlQuery;		
	sqlQuery = [in_database query: queryString];
			
	[sqlQuery bindIndex: 1 withUnsignedLong: identifier];				
	[sqlQuery bindIndex: 2 withString: name];
	[sqlQuery bindIndex: 3 withBlob: fieldStream];
	[sqlQuery bindIndex: 4 withBlob: salt];
	[sqlQuery bindIndex: 5 withBlob: initVector];
	[sqlQuery bindIndex: 6 withBlob: authCode];

	// Execute the query.
	[sqlQuery execute];
	
	[salt release];
	[initVector release];
	[authCode release];
}

- (void)storeInFields:(SHSqlDb*)in_database table:(NSString*)in_table withPasswordHash:(NSData*)in_passwordHash {
	NSMutableData* fieldStream;
	fieldStream = [self fieldStream];
	
	NSMutableData* salt			= [[NSMutableData alloc] init];
	NSMutableData* initVector	= [[NSMutableData alloc] init];
	NSMutableData* authCode		= [[NSMutableData alloc] init];

	[cypher encryptData: fieldStream withPassword: in_passwordHash salt: salt 
		initVector: initVector authCode: authCode];
	
	// If the identifier is not 0 we update the record in the database,
	// else we insert the new record.

	NSString *queryString = [NSString stringWithFormat: @"UPDATE %@ SET \
		contents=?2, salt=?3, initVector=?4, authCode=?5 WHERE (identifier==?1)", in_table];

	SHSqlQuery *sqlQuery;		
	sqlQuery = [in_database query: queryString];
			
	[sqlQuery bindIndex: 1 withUnsignedLong: identifier];				
	[sqlQuery bindIndex: 2 withBlob: fieldStream];
	[sqlQuery bindIndex: 3 withBlob: salt];
	[sqlQuery bindIndex: 4 withBlob: initVector];
	[sqlQuery bindIndex: 5 withBlob: authCode];

	// Execute the query.
	[sqlQuery execute];
	
	[salt release];
	[initVector release];
	[authCode release];
}

- (BOOL)readFrom:(SHSqlDb*)in_database withIdentifier:(NSNumber*)in_identifier withPasswordHash:(NSData*)in_passwordHash {

	NSString* queryString = [NSString stringWithFormat: @"SELECT identifier, name ,contents, salt, \
		initVector, authCode FROM recordTable WHERE (identifier==%d)", [in_identifier unsignedIntValue]];
	
	SHSqlQuery* query = [in_database query: queryString];
	
	BOOL foundRecord = [query stepAndFinalize];
	
	if(foundRecord) {	

		[self loadFromQuery: query withPasswordHash: in_passwordHash];
		[query finish];
		
	}
	
	return(foundRecord);	
}

- (void)loadFromQuery:(SHSqlQuery*)in_query withPasswordHash:(NSData*)in_passwordHash {

	NSMutableData *fieldArrayData;
	NSData* salt;
	NSData* initVector;
	NSData* authCode;

	//
	// Get the identifier from the query.
	//
	[self setIdentifier: [in_query columnAsUnsignedLong: 0]];
	[self setName: [in_query columnAsString: 1]];
	
	fieldArrayData = [[NSMutableData alloc] init];
	[fieldArrayData setData: [in_query columnAsBlob: 2]];
	
	// Check that there is any data to decrypt.
	if( [fieldArrayData length] > 0 ) {

		salt		= [in_query columnAsBlob: 3];
		initVector	= [in_query columnAsBlob: 4];
		authCode	= [in_query columnAsBlob: 5];
	
		[cypher decryptData: fieldArrayData withPassword: in_passwordHash withSalt: salt
			withInitVector: initVector withAuthCode: authCode];
	}
	
	// If the fieldArrayData is valid
	[self setFieldArrayWithData: fieldArrayData];
	
	[fieldArrayData release];
}

@end
