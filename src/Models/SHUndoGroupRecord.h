//
//  SHUndoGroupRecord.h
//  Stronghold
//
//  Created by Javier Carro on 08/05/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SHSqlDb.h"

#define SH_UNDO_RECORD_ITEM		1
#define SH_UNDO_GROUP_ITEM		2

@interface SHUndoGroupRecord : NSObject {
	/*!
		@var		identifier

		@abstract	Identifier <i>rowId</i> of the record in the database.
	*/
	NSNumber*	identifier;
	
	/*!
		@var		recordType

		@abstract	Indicates if the record is a group or a record.
	*/
	ulong32			recordType;
	
	/*!
		@var		numFields

		@abstract	Number of fields of the record.
	*/
	ulong32			numFields;
	
	/*!
		@var		groupRow

		@abstract	Index of the element in the controller's array.
	*/
	ulong32			groupRow;

	/*!
		@var		recordRow

		@abstract	Index of the element in the controller's array.
	*/
	ulong32			recordRow;
		
	/*!
		@var		fieldNameArray

		@abstract	Array containing the name of the fields.
	*/
	NSMutableArray* fieldNameArray;

	/*!
		@var		fieldTypeArray

		@abstract	Array containing the type of the fields.
	*/
	NSMutableArray* fieldTypeArray;

	/*!
		@var		fieldValueArray

		@abstract	Array containing the values of the fields.
	*/
	NSMutableArray* fieldValueArray;
		
	/*!
		@var		crossIdentifierArray

		@abstract	Array of groupId or recordId to regenerate the groupRecordCrossTable.
	*/
	NSMutableArray* crossIdentifierArray;		
}

- initWithIdentifier:(NSNumber*)in_identifier recordType:(ulong32)in_recordType groupRow:(slong32)in_groupRow
	recordRow:(slong32)in_recordRow database:(SHSqlDb*)in_database;

- (void)dealloc;

- (NSNumber*)identifier;

- (ulong32)groupRow;

- (ulong32)recordRow;

- (void)insertIntoDatabase:(SHSqlDb*)in_database;

- (void)deleteFromDatabase:(SHSqlDb*)in_database;

@end
