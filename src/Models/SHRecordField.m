/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 ************************/
//
//		$Revision$
//
//		$Author$
//
//		$Date$
//
/****************************************************************************/

#import "SHRecordField.h"

@implementation SHRecordField

- initWithName: (NSString*) in_name Value: (NSString*) in_value {

	[super init];
	
	[self setName: in_name];
	[self setValue: in_value];
	
	return(self);	
}

- initWithRecordField:(SHRecordField*)in_recordField {
	[super init];
	
	name = [[NSString alloc] initWithString: [in_recordField name]];
	
	value = [[NSString alloc] initWithString: [in_recordField value]];

	return(self);
}

- (void) dealloc {

	[name release];
	[value release];
	
	[super dealloc];
}

- (NSString*) name {

	return(name);

}

- (void) setName: (NSString*) in_name {

	[in_name retain];
	[name release];
	name = in_name;

}

- (NSString*) value {

	return(value);

}

- (void) setValue: (NSString*) in_value {

	[in_value retain];
	[value release];
	value = in_value;

}

@end
