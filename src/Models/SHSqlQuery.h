/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 ************************/
//
//		$Revision$
//
//		$Author$
//
//		$Date$
//
/****************************************************************************/

#import <Cocoa/Cocoa.h>
#import <sqlite3.h>
#import "typedef.h"

/*!
    @class       SHSqlQuery

    @abstract    aaaaaaaaaaaaaaaaa

    @discussion  bbbbbbbbbbb
*/
@interface SHSqlQuery : NSObject {
	
	/*!
		@var		dbHandle
		
		@abstract	Pointer to the sqlite3 underlying database handler.
	*/
	sqlite3*		dbHandle;

	/*!
		@var		statement
		
		@abstract	Pointer to the sqlite compiled statement.
	*/
	sqlite3_stmt*	statement;
}

/*!
    @method     

    @abstract   Initializes an allocated SHSqlQuery object.

    @discussion The SHSqlQuery is just a convenient wrapper around the sqlite statements
				but does not compile it.

    @param      in_statement
				Sqlite already compiled statement.

    @param      in_dbHandle
				Pointer to the sql database.

*/
- initWithStatement:(sqlite3_stmt*)in_statement withDbHandle:(sqlite3*)in_dbHandle;

/*!
    @method     

    @abstract   Initializes an allocated SHSqlQuery object.

    @discussion The SHSqlQuery is just a convenient wrapper around the sqlite statements
				but does not compile it.

*/
- (void)dealloc;
				
/*!
    @method     stepAndFinalize

    @abstract   Step through the next result of the sql statement.

    @discussion If there is another statement the results are laoded and can be accessed
				using the <i>columnAs*</i> methods and the return is YES.
				
				If there are no more results the statement is finalized and the method returns false.

	@throws		TBW
	
    @result     YES if results are avaliable and can be accessed using the <i>columnAs*</i> methods.
	
				NO if no more results are available and the statement is <i>finalized</i>.
*/
- (BOOL)stepAndFinalize;

/*!
    @method     stepAndReset

    @abstract   Step through the next result of the sql statement.

    @discussion If there is another statement the results are laoded and can be accessed
				using the <i>columnAs*</i> methods and the return is YES.
				
				If there are no more results the statement is finalized<i>reset</i> and the method returns false.

	@throws		TBW
	
    @result     YES if results are avaliable and can be accessed using the <i>columnAs*</i> methods.
	
				NO if no more results are available and the statement is <i>reset</i>.
*/
- (BOOL)stepAndReset;

/*!
    @method     execute

    @abstract   <#(brief description)#>

    @discussion <#(comprehensive description)#>

*/
- (void)execute;

/*!
    @method     reset

    @abstract   Reset the statement.

    @discussion The method is just a convenient wrapper around the <i>reset</i> function
				of sqlite.
	
	@throws		TBW
*/
- (void)reset;

/*!
    @method     finish

    @abstract   Finish the statement.

    @discussion The method is just a convenient wrapper around the <i>finish</i> function
				of sqlite.
	
	@throws		TBW
*/
- (void)finish; 

/*!
    @method     bindIndex:withString:

    @abstract   Links the string to the parameter index in the sql statement.

    @discussion Links a the string to the parameter with index <i>index</i> in
				the sqlite statement.
				
				It is a covenience wrapper for the <i>sqlite3_bind_text</i> 
				from the sqlite API.

    @param      in_index
				Position of the parameter in the sql statement.

    @param      in_string
				String to bind to the parameter in position
				<i>in_index</i> in the sql statement.

	@throws
*/
- (void)bindIndex:(ulong32)in_index withString:(NSString*)in_string;

/*!
    @method     bindIndex:withBlob:

    @abstract   Links the blob (byte stream) to the parameter index in the sql statement.

    @discussion Links a the blob (byte stream) to the parameter with index <i>index</i>
				in the sqlite statement.
				
				It is a covenience wrapper for the <i>sqlite3_bind_blob</i> 
				from the sqlite API.

    @param      in_index
				Position of the parameter in the sql statement.

    @param      in_blob
				Blob (byte stream) to bind to the parameter in position
				<i>in_index</i> in the sql statement.

	@throws
*/
- (void)bindIndex:(ulong32)in_index withBlob:(NSData*)in_blob;

/*!
    @method     bindIndex:withUnsignedLong:

    @abstract   Links the unsigned long to the parameter index in the sql statement.

    @discussion Links a the unsigned long to the parameter with index <i>index</i>
				in the sqlite statement.
				
				It is a covenience wrapper for the <i>sqlite3_bind_int</i> 
				from the sqlite API.

    @param      in_index
				Position of the parameter in the sql statement.

    @param      in_number
				Unsigned long to bind to the parameter in position
				<i>in_index</i> in the sql statement.
	
	@throws
*/
- (void)bindIndex:(ulong32)in_index withUnsignedLong:(NSNumber*)in_number;

/*!
    @method     bindIndex:withUnsignedLong:

    @abstract   Links the unsigned long to the parameter index in the sql statement.

    @discussion Links a the unsigned long to the parameter with index <i>index</i>
				in the sqlite statement.
				
				It is a covenience wrapper for the <i>sqlite3_bind_int</i> 
				from the sqlite API.

    @param      in_index
				Position of the parameter in the sql statement.

    @param      in_number
				Unsigned long to bind to the parameter in position
				<i>in_index</i> in the sql statement.
	
	@throws
*/
- (void)bindIndex:(ulong32)in_index withLongLong:(NSNumber*)in_number;

/*!
    @method     bindIndex:withDouble:

    @abstract   Links the double to the parameter index in the sql statement.

    @discussion Links a the double to the parameter with index <i>index</i>
				in the sqlite statement.
				
				It is a covenience wrapper for the <i>sqlite3_bind_double</i> 
				from the sqlite API.

    @param      in_index
				Position of the parameter in the sql statement.

    @param      in_number
				Double to bind to the parameter in position
				<i>in_index</i> in the sql statement.
	
	@throws
*/
- (void)bindIndex:(ulong32)in_index withDouble:(NSNumber*)in_number;

/*!
    @method     bindIndexWithNull

    @abstract   Links a null e to the parameter index in the sql statement.

    @discussion Links a null to the parameter with index <i>index</i>
				in the sqlite statement.
				
				It is a covenience wrapper for the <i>sqlite3_bind_null</i> 
				from the sqlite API.

    @param      in_index
				Position of the parameter in the sql statement.
	
	@throws
*/
- (void)bindIndexWithNull:(ulong32)in_index;
	
/*!
    @method     columnAsDouble:

    @abstract   Returns the specified column of the result, interpreted as a double.

    @discussion Reads the column <i>in_column</i> from the result, interpreting it as
				a double.
				
				It is a covenience wrapper for the <i>sqlite3_column_double</i> 
				from the sqlite API.

	@throws

    @param      in_column
				Index of the column to read.

    @result     Value of the column <i>in_column</i> read as a double.

*/
- (NSNumber*)columnAsDouble:(int)in_column;

/*!
    @method     columnAsUnsignedLong:

    @abstract   Returns the specified column of the result, interpreted as an unsigned long.

    @discussion Reads the column <i>in_column</i> from the result, interpreting it as
				an unsigned long.
				
				It is a covenience wrapper for the <i>sqlite3_column_int</i> 
				from the sqlite API.

	@throws

    @param      in_column
				Index of the column to read.

    @result     Value of the column <i>in_column</i> read as an unsigned long.

*/
- (NSNumber*)columnAsUnsignedLong:(int)in_column;

/*!
    @method     columnAsUnsignedLongLong:

    @abstract   Returns the specified column of the result, interpreted as an unsigned long long.

    @discussion Reads the column <i>in_column</i> from the result, interpreting it as
				an unsigned long long.
				
				It is a covenience wrapper for the <i>sqlite3_column_int64</i> 
				from the sqlite API.

	@throws

    @param      in_column
				Index of the column to read.

    @result     Value of the column <i>in_column</i> read as an unsigned long long.

*/
- (NSNumber*)columnAsUnsignedLongLong:(int)in_column;

/*!
    @method     columnAsLongLong:

    @abstract   Returns the specified column of the result, interpreted as a long long.

    @discussion Reads the column <i>in_column</i> from the result, interpreting it as
				an long long.
				
				It is a covenience wrapper for the <i>sqlite3_column_int64</i> 
				from the sqlite API.

	@throws

    @param      in_column
				Index of the column to read.

    @result     Value of the column <i>in_column</i> read as an long long.

*/
- (NSNumber*)columnAsLongLong:(int)in_column;

/*!
    @method     columnAsString:

    @abstract   Returns the specified column of the result, interpreted as a string.

    @discussion Reads the column <i>in_column</i> from the result, interpreting it as
				a string.
				
				It is a covenience wrapper for the <i>sqlite3_column_text</i> 
				from the sqlite API.

	@throws

    @param      in_column
				Index of the column to read.

    @result     Value of the column <i>in_column</i> read as a string.

*/
- (NSString*)columnAsString:(int)in_column;

/*!
    @method     columnAsBlob:

    @abstract   Returns the specified column of the result, interpreted as a blob (byte stream).

    @discussion Reads the column <i>in_column</i> from the result, interpreting it as
				a blob (byte stream).
				
				It is a covenience wrapper for the <i>sqlite3_column_blob</i> 
				from the sqlite API.

	@throws

    @param      in_column
				Index of the column to read.

    @result     Value of the column <i>in_column</i> read as a blob (byte stream).

*/
- (NSData*)columnAsBlob:(int)in_column;

/*!
    @method     columnCount

    @abstract   Returns the number of columns in the query.

    @result     Number of columns in the query.

*/
- (ulong32)columnCount;

/*!
    @method     columnType:

    @abstract   Returns the type of the column with index <i>in_column</i>.

    @discussion <#(comprehensive description)#>

    @param      in_column Index of the column for which the type is requested.

    @result     Returns the type of the column (see sqlite documentation for the types).

*/
- (ulong32)columnType:(ulong32)in_column;

/*!
    @method     columnName:

    @abstract   Returns the name of the column with index <i>in_column</i>.

    @param      in_column Index of the column for which the name is requested.

    @result     Returns the name of the column.

*/
- (NSString*)columnName:(ulong32)in_column;

@end
