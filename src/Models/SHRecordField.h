/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 ************************/
//
//		$Revision$
//
//		$Author$
//
//		$Date$
//
/****************************************************************************/

#import <Cocoa/Cocoa.h>

/*!
    @class       SHRecordField 

    @superclass  NSObject 

    @abstract    <#(brief description)#>

    @discussion  <#(comprehensive description)#>
*/
@interface SHRecordField : NSObject {

	NSString*	name;

	NSString*	value;
}

/*!
    @method     initWithName:Value:

    @abstract   <#(brief description)#>

    @discussion <#(comprehensive description)#>

    @param      <#in_name (description)#>

    @param      <#in_value (description)#>

*/
- initWithName:(NSString*)in_name Value:(NSString*)in_value;

- initWithRecordField:(SHRecordField*)in_recordField;

/*!
    @method     dealloc

    @abstract   <#(brief description)#>

    @discussion <#(comprehensive description)#>

*/
- (void)dealloc;

/*!
    @method     name

    @abstract   <#(brief description)#>

    @discussion <#(comprehensive description)#>

    @result     <#(description)#>

*/
- (NSString*)name;

/*!
    @method     setName:

    @abstract   <#(brief description)#>

    @discussion <#(comprehensive description)#>

    @param      <#in_name (description)#>

*/
- (void)setName:(NSString*)in_name;

/*!
    @method     value

    @abstract   <#(brief description)#>

    @discussion <#(comprehensive description)#>

    @result     <#(description)#>

*/
- (NSString*)value;

/*!
    @method     setValue:

    @abstract   <#(brief description)#>

    @discussion <#(comprehensive description)#>

    @param      <#in_value (description)#>

*/
- (void)setValue:(NSString*)in_value;

@end
