/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 ************************/
//
//		$Revision$
//
//		$Author$
//
//		$Date$
//
/****************************************************************************/

#import <Cocoa/Cocoa.h>
#import "SHSqlDb.h"

/*!
    @class			SHRecordName 

    @superclass		NSObject

    @abstract		Holds the identifier and name of a record from the database.

    @discussion		Holds the identifier and name of a record from the database.
					This class is used by the SHRecordTableController to hold an array of records that belong to a certain 
					group without loading the record fields that are crypted.
					
	@helps			SHRecordTableController to build the array of records that belong to a certain group.
*/
@interface SHRecordName : NSObject {
 
	/*!
		@var		identifier

		@abstract	Identifier <i>rowId</i> of the record in the database.
	*/
	NSNumber*	identifier;
	
	/*!
		@var		name
		
		@abstract	NSString containing the name of the record.
	*/
	NSString*	name;
}

/*!
    @method     init

    @abstract   Inialise an allocated SHRecordName object.

    @discussion Calls initWithName: @"" identifier: 0.
	
	@return		Initialized SHRecordName object.
*/
- init;

/*!
    @method     initWithName:
	
    @abstract   Inialise an allocated SHRecordName object.

    @discussion Calls initWithName: in_name identifier: 0.
	
	@param		in_name
				NSString with the name used to intialise <i>name</i>.
		
	@return		Initialized SHRecordName object.
*/
- initWithName:(NSString*)in_name;

/*!
    @method     initWithName:identifier:
  
	@abstract	Initializes an allocated SHRecordName object.

    @discussion Allocates and intialises the following attributes:
				
				<ul>

				<li>identifier: Initialiazed with <i>in_identifier</i>.
				
				<li>name:	Initialized to <i>in_name</i>.

				</ul>

	@param		in_name
				NSString with the name used to intialise <i>name</i>.
	
	@param		in_identifier
				NSNumber with the name used to intialise <i>identifier</i>.
	
	@return		Initialized SHRecordName object.
*/
- initWithName:(NSString*)in_name identifier:(NSNumber*)in_identifier;

/*!
    @method		dealloc
	
    @abstract   Deallocates the memory occupied by the receiver.

    @discussion Deallocates the following attributes:
				
				<ul>
				<li>identifier.
				
				<li>name.
				</ul>
*/
- (void)dealloc;

/*!
    @method		identifier

    @abstract   Returns the value of the <i>identifier</i> attribute.

	@return		NSNumber with the value of the <i>identifier</i> attribute.
*/
- (NSNumber*)identifier;

/*!
   
	@method    setIdentifier: 
    
	@abstract   Sets the value of the <i>identifier</i> attribute.
    
	@param		in_identifier New value for the <i>identifier</i> attribute.
*/
- (void)setIdentifier:(NSNumber*)in_identifier;

/*!
    @method     name

    @abstract   Returns the value of the <i>name</i> attribute.

	@return		NSString with the value of the <i>name</i> attribute.
*/
- (NSString*)name;

/*!
	@method		setName:
    
	@abstract   Sets the value of the <i>name</i> attribute.
    
	@param		in_name New value for the <i>name</i>  attribute.
*/
- (void)setName:(NSString*)in_name;

/*!
    @method     removeFrom:
	
    @abstract   Removes the record from the database.

    @discussion Removes the record with identifier <i>identifier</i> from the 
				database <i>in_database</i>, and removes all the rows in the
				<i>groupRecordCrossTable</i> so the record is removed from all
				the groups.

*/
- (void)removeFrom:(SHSqlDb*)in_database;

/*!
    @method		storeIn:

    @abstract	Stores the record in the database.

    @discussion If the <i>identifier</i> attribute is 0 the record is inserted in the database
				else it is just updated.
	
	@param		in_database
				Database where the record will be stored.
				
	@throws		

*/
- (void)storeIn:(SHSqlDb*)in_database;

/*!
    @method     readFrom:withIdentifier:
    
	@abstract   Loads a SHRecordName with <i>identifier</i> from the database.
	
    @discussion The function creates and statement, executes it and calls the @link loadFromQuery: loadFromQuery: @/link 
				method to load the results.
				This subroutine prepares a sql query and exeutes it, then calls 
				@link loadFromQuery:in_query: loadFromQuery:in_query: @/link
				to load the record data from the query results.
				    
	@param		in_database
				Handler of the database from where the record should be loaded.

	@param		in_identifier
				Indentifier of the record to load from the database <i>in_database</i>.
	
	@result     Returns YES if the record was succesfully loaded from the database, else it returns NO.
*/
- (BOOL)readFrom:(SHSqlDb*)in_database withIdentifier:(NSNumber*)in_identifier;

/*!
    @method     loadFromQuery:

    @abstract   Loads the SHRecordName attributes from the query <i>in_query</i>.

    @discussion This is an utility function to load all the attributes of the SHRecordName from a sqlite query.
				The columns of the sqilte query should be (in the same order):
				
				1	Record identifier <i>identifier</i>.
				
				2	Record name <i>name</i>.
					
				This method is used by the @link readFrom:withIdentifier: @/link
				method to load the fields from the sqilte query.
				It can be used also to load several SHRecordName instances or the same one that is reused 
				several times when a sqlite query returns more than one result.
	
	@param		in_query
				Sqlite query that has been successfully excetuted and for which the results are the ones specified in the
				<i>discussion</i> topic (in the same order).
				
	@throws		TBD
*/
- (void)loadFromQuery:(SHSqlQuery*) in_query;

@end