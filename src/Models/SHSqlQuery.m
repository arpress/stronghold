/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 ************************/
//
//		$Revision$
//
//		$Author$
//
//		$Date$
//
/****************************************************************************/

#import "SHSqlQuery.h"

@implementation SHSqlQuery

- initWithStatement:(sqlite3_stmt*)in_statement withDbHandle:(sqlite3*)in_dbHandle {
	dbHandle = in_dbHandle;
	statement = in_statement;
	
	// Check the parameters and launch an exception if appropiate
	return [self init]; 
}

- (void)dealloc {
	// Check if the statement should be liberated.
	if(statement) {
		[self finish];
	}
	
	[super dealloc];
}

- (BOOL) stepAndFinalize {

	int sqlResult;
	sqlResult = sqlite3_step(statement);
	
	// If the statement can not be used anymore then
	// it should be liberated.
	BOOL result;
	
	switch( sqlResult ) {

		case SQLITE_ROW :  {

			result = YES;

		} break;
	
		case SQLITE_DONE : {

			[self finish]; 
			result = NO;

		} break;
		
		default : {

			NSString* sqlError = [NSString stringWithUTF8String: sqlite3_errmsg(dbHandle)];

			[NSException raise: SHGenericException format: sqlError];

			[self finish];

		}
	}
	
	return(result);
}

- (BOOL) stepAndReset {

	int sqlResult;
	sqlResult = sqlite3_step(statement);
	
	// If the statement can not be used anymore then
	// it should be liberated.
	BOOL result;
	
	switch( sqlResult ) {

		case SQLITE_ROW :  {

			result = YES;

		} break;
	
		case SQLITE_DONE : {

			[self reset]; 
			result = NO;

		} break;
		
		default : {

			NSString* sqlError = [NSString stringWithUTF8String: sqlite3_errmsg(dbHandle)];
			[NSException raise: SHGenericException format: sqlError];

		}
	}
	
	return(result);
}

- (void) execute {

	BOOL moreRecords = [self stepAndFinalize];
	
	if( moreRecords ) {
		[self finish];
		
		[NSException raise: SHGenericException format: @"Statement execution provided did not finish in the first iteration"];
	}
	
	statement = nil;	
}

- (void)reset {

	int sqlResult;	
	sqlResult = sqlite3_reset(statement);
	
	if( sqlResult != SQLITE_OK ) {
	
		NSString* sqlError = [NSString stringWithUTF8String: sqlite3_errmsg(dbHandle)];
		[NSException raise: SHGenericException format: sqlError];
	
	}
	
	sqlResult = sqlite3_clear_bindings(statement);
	if( sqlResult != SQLITE_OK ) {
	
		NSString* sqlError = [NSString stringWithUTF8String: sqlite3_errmsg(dbHandle)];
		[NSException raise: SHGenericException format: sqlError];
	
	}

}

- (void)finish {

	int sqlResult;	
	sqlResult = sqlite3_finalize(statement);
	
	if( sqlResult != SQLITE_OK ) {
	
		NSString* sqlError = [NSString stringWithUTF8String: sqlite3_errmsg(dbHandle)];
		[NSException raise: SHGenericException format: sqlError];
	
	}
	
	statement = nil;
}

- (void) bindIndex: (ulong32) in_index withString: (NSString*) in_string {
	
	int sqlResult;
	const char* textPtr;
	
	textPtr = [in_string UTF8String];
	
	sqlResult = sqlite3_bind_text(statement, in_index, textPtr, strlen(textPtr), SQLITE_TRANSIENT);
	
	if( sqlResult != SQLITE_OK ) {
	
		NSString* sqlError = [NSString stringWithUTF8String: sqlite3_errmsg(dbHandle)];
		[NSException raise: SHGenericException format: sqlError];
	
	}
}

- (void) bindIndex: (ulong32) in_index withBlob: (NSData*) in_blob {

	int sqlResult;
	const void* blobPtr;
	NSUInteger blobLength;
	
	blobPtr = [in_blob bytes];
	blobLength = [in_blob length];
	
	sqlResult = sqlite3_bind_blob(statement, in_index, blobPtr, blobLength , SQLITE_TRANSIENT);
	
	if( sqlResult != SQLITE_OK ) {
	
		NSString* sqlError = [NSString stringWithUTF8String: sqlite3_errmsg(dbHandle)];
		[NSException raise: SHGenericException format: sqlError];
	
	}
}

- (void) bindIndex: (ulong32) in_index withUnsignedLong: (NSNumber*) in_number {
	int sqlResult;
	
	sqlResult = sqlite3_bind_int(statement, in_index, [in_number unsignedIntValue]);
	
	if( sqlResult != SQLITE_OK ) {
	
		NSString* sqlError = [NSString stringWithUTF8String: sqlite3_errmsg(dbHandle)];
		[NSException raise: SHGenericException format: sqlError];
	
	}
}

- (void)bindIndex:(ulong32)in_index withLongLong:(NSNumber*)in_number {
	int sqlResult;
	
	sqlResult = sqlite3_bind_int64(statement, in_index, [in_number longLongValue]);
	
	if( sqlResult != SQLITE_OK ) {
	
		NSString* sqlError = [NSString stringWithUTF8String: sqlite3_errmsg(dbHandle)];
		[NSException raise: SHGenericException format: sqlError];
	
	}
}

- (void)bindIndex:(ulong32)in_index withDouble:(NSNumber*)in_number {
	int sqlResult;
	
	sqlResult = sqlite3_bind_double(statement, in_index, [in_number doubleValue]);
	
	if( sqlResult != SQLITE_OK ) {
	
		NSString* sqlError = [NSString stringWithUTF8String: sqlite3_errmsg(dbHandle)];
		[NSException raise: SHGenericException format: sqlError];
	
	}
}

- (void)bindIndexWithNull:(ulong32)in_index {
	int sqlResult;
	
	sqlResult = sqlite3_bind_null(statement, in_index);
	
	if( sqlResult != SQLITE_OK ) {
	
		NSString* sqlError = [NSString stringWithUTF8String: sqlite3_errmsg(dbHandle)];
		[NSException raise: SHGenericException format: sqlError];
	}
}

- (NSNumber*) columnAsDouble: (int) in_column {
	double columnResult;
	
	columnResult = sqlite3_column_double(statement, in_column);
	
	NSNumber* resultValue = [NSNumber numberWithDouble: columnResult];
	
	return(resultValue);
}

- (NSNumber*) columnAsUnsignedLongLong: (int) in_column {
	ulong64 columnResult;
	
	columnResult = sqlite3_column_int64(statement, in_column);

	NSNumber* resultValue = [NSNumber numberWithUnsignedLongLong: columnResult];
	
	return(resultValue);
}

- (NSNumber*) columnAsLongLong: (int) in_column {
	slong64 columnResult;
	
	columnResult = sqlite3_column_int64(statement, in_column);

	NSNumber* resultValue = [NSNumber numberWithLongLong: columnResult];
	
	return(resultValue);
}

- (NSNumber*) columnAsUnsignedLong: (int) in_column {
	ulong32 columnResult;
	
	columnResult = sqlite3_column_int64(statement, in_column);

	NSNumber* resultValue = [NSNumber numberWithUnsignedLong: columnResult];
	
	return(resultValue);
}

- (NSString*) columnAsString: (int) in_column {
	const char* columnResult;
	
	columnResult = (const char*) sqlite3_column_text(statement, in_column);
	
	NSString* stringResult = [NSString stringWithUTF8String: columnResult];
	
	return(stringResult);
}

- (NSData*) columnAsBlob: (int) in_column {
	const char* columnResult;
	NSUInteger	columnResultLength;
	
	columnResult = (const char*) sqlite3_column_blob(statement, in_column);
	columnResultLength = (NSUInteger) sqlite3_column_bytes(statement, in_column);
	
	NSData* blobResult = [NSData dataWithBytes: columnResult length:columnResultLength];
	
	return(blobResult);
}

- (ulong32)columnCount {

	ulong32 columnCount = sqlite3_column_count(statement);
	
	return(columnCount);
}

- (ulong32)columnType:(ulong32)in_column {

	ulong32 columnType = sqlite3_column_type(statement, in_column);
	
	return(columnType);
}

- (NSString*)columnName:(ulong32)in_column {

	const char* columnName = sqlite3_column_name(statement, in_column);
	
	NSString *columnNameString = [NSString stringWithCString: columnName];
	
	return(columnNameString);
}

@end
