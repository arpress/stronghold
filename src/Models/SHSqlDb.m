/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 ************************/
//
//		$Revision$
//
//		$Author$
//
//		$Date$
//
/****************************************************************************/

#import "SHSqlDb.h"
#import "SHSqlQuery.h"

@implementation SHSqlDb

- init {
	dbHandler = NULL;
	return(self);
}

- (void) dealloc {
	if(dbHandler) {
		[self close];
	}
	
	[super dealloc];
}

- (void)open:(NSString*)in_dbFilename {
	int sqlResult;
	
	const char* dbPath = [in_dbFilename fileSystemRepresentation];
	sqlResult = sqlite3_open( dbPath, &dbHandler );
	
	if(sqlResult != SQLITE_OK) {
	
		NSString* sqlError = [NSString stringWithUTF8String: sqlite3_errmsg(dbHandler)];
		[NSException raise: SHGenericException format: sqlError];
	
	}
}

- (void) close {
	int sqlResult;
	
	sqlResult = sqlite3_close(dbHandler);
	
	if(sqlResult != SQLITE_OK) {

		NSString* sqlError = [NSString stringWithUTF8String: sqlite3_errmsg(dbHandler)];
		[NSException raise: SHGenericException format: sqlError];
		
	}

	dbHandler = NULL;
}

- (void) initRecordTable {

	SHSqlQuery* query;
	
	query = [self query: @"CREATE TABLE IF NOT EXISTS recordTable (identifier INTEGER PRIMARY KEY AUTOINCREMENT, \
		name TEXT, contents BLOB, salt BLOB, initVector BLOB, authCode BLOB)"];

	[query execute];
}

- (void) initGroupTable {

	SHSqlQuery* query;
	
	query = [self query: @"CREATE TABLE IF NOT EXISTS groupTable (identifier INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)"];

	[query execute];	
}

- (void) initGroupRecordCrossTable {

	SHSqlQuery* query;
	
	query = [self query: @"CREATE TABLE IF NOT EXISTS groupRecordCrossTable (groupId INTEGER, recordId INTEGER)"];
	
	[query execute];
}

- (void) initAuxTable {

	SHSqlQuery* query;
	
	query = [self query: @"CREATE TABLE IF NOT EXISTS auxTable (name TEXT UNIQUE PRIMARY KEY, contents BLOB, \
		salt BLOB, initVector BLOB, authCode BLOB)"];

	[query execute];
}

- (void) createTempRecordTable {

	SHSqlQuery* query;
	
	query = [self query: @"CREATE TABLE IF NOT EXISTS tempRecordTable (identifier INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, contents BLOB, \
		salt BLOB, initVector BLOB, authCode BLOB)"];

	[query execute];
}

- (void) deleteTempRecordTable {

	SHSqlQuery* query;

	query = [self query: @"DROP TABLE IF EXISTS tempRecordTable"];

	[query execute];
}

- (SHSqlQuery*) query: (NSString*) in_query {

	int sqlResult;
	SHSqlQuery *query = nil;
	
	// Create the statement.
	sqlite3_stmt *statement;
	const char* queryString = [in_query UTF8String];
	sqlResult = sqlite3_prepare_v2(dbHandler, queryString, -1, &statement, NULL);
	
	if(sqlResult == SQLITE_OK) {
		query = [[SHSqlQuery alloc] initWithStatement:statement withDbHandle: dbHandler];
		[query autorelease];
	} else {		
		
		NSString* sqlError = [NSString stringWithUTF8String: sqlite3_errmsg(dbHandler)];
		[NSException raise: SHGenericException format: sqlError];

	}
	
	return(query);	
}

- (NSNumber*) lastInsertRowId {
	ulong64 lastRowId;
	NSNumber* rowId;
	
	if (dbHandler) {
		lastRowId = sqlite3_last_insert_rowid(dbHandler);
		rowId = [NSNumber numberWithUnsignedLongLong: lastRowId];		
	} else {

		NSString* sqlError = [NSString stringWithUTF8String: sqlite3_errmsg(dbHandler)];
		[NSException raise: SHGenericException format: sqlError];

	}	

	return(rowId);
}

@end
