/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/
 
/*******************		FILE IDENTIFICATION		 ************************/
//
//		$Revision$
//
//		$Author$
//
//		$Date$
//
/****************************************************************************/

#import <Cocoa/Cocoa.h>

#import "typedef.h"
#import "SHSqlDb.h"
#import "SHCypher.h"

/*!
    @class			SHRecord
	
	@abstract		Object used to represent the records in the database.
	
	@discussion		SHRecord objects have are described by their identifier,
					the name, the record fields and a cypher helper that is used
					to crypt and decrypt the record fields.
	
	@updated
*/

@interface SHRecord : NSObject {

	/*!
		@var		identifier

		@abstract	Identifier <i>rowId</i> of the record in the database.
	*/
	NSNumber*	identifier;

	/*!
		@var		name
		
		@abstract	NSString containing the name of the record.
	*/
	NSString*	name;

	/*!
		@var		fieldArray
		
		@abstract	Contents of the record, it is an array of
					field names and field values.
	*/
	NSMutableArray*	fieldArray;

	/*!
		@var		cypher
		
		@abstract	Helper object to crypt and decrypt the record fields. 
	*/	
	SHCypher* cypher;
}

/*!
    @method     init
  
	@abstract   Initializes an allocated SHRecord object.

    @discussion Allocates and intialises the following attributes:
				
				<ul>
				<li>identifier: Initialiazed with 0.
				
				<li>name:	Initialized to nil.
				
				<li>fieldArray: Initialized to an empty array.
				
				<li>cypher: Initialized with the default SHCypher init.
				</ul>
	
	@return		Initialized SHRecord object.
*/
- init;

/*!
    @method		dealloc
	
    @abstract   Deallocates the memory occupied by the receiver.

    @discussion Deallocate the following attributes:
				
				<ul>
				<li>identifier.
				
				<li>name.
				
				<li>fieldArray.
				
				<li>cypher.
				</ul>
*/
- (void) dealloc;

/*!
    @method		identifier

    @abstract   Returns the value of the <i>identifier</i> attribute.

	@return		NSNumber with the value of the <i>identifier</i> attribute.
*/
- (NSNumber*)identifier;

/*!
   
	@method     setIdentifier: 
    
	@abstract   Sets the value of the <i>identifier</i> attribute.
    
	@param		in_identifier New value for the <i>identifier</i> attribute.
*/
- (void)setIdentifier:(NSNumber*)in_identifier;

/*!
    @method     name

    @abstract   Returns the value of the <i>name</i> attribute.

	@return		NSString with the value of the <i>name</i> attribute.
*/
- (NSString*)name;

/*!
	@method		setName:
    
	@abstract   Sets the value of the <i>name</i> attribute.
    
	@param		in_name New value for the <i>name</i>  attribute.
*/
- (void)setName: (NSString*) in_name;

/*!
    @method		fieldArray

    @abstract   Returns the value of the <i>fieldArray</i> attribute.

	@return		NSMutableArray with the value of the <i>fieldArray</i> attribute.
*/
- (NSMutableArray*)fieldArray;

/*!
	@method		setFieldArray:
    
	@abstract   Sets the value of the <i>fieldArray</i> attribute.
    
	@param		in_fieldArray New value for the <i>fieldArray</i> attribute.
*/
- (void)setFieldArray:(NSMutableArray*)in_fieldArray;

/*!
    @method     fieldStream

    @abstract   Creates a byte stream joining all the names and values in the 
				fields array.

    @discussion For each field the title and the name are appended in this
				order to the byte stream. The strings are stored with their
				terminating null character.

	@return		NSMutableData containing the resulting byte stream.
*/
- (NSMutableData*)fieldStream;

/*!
    @method     storeIn:withPasswordHash:
    
	@abstract   Stores the record crypted in the database.
    
	@discussion The record is stored in the database using its identifier, in
				the <i>recordTable</i>.
				
				If the identifier is 0 the record is inserted in the database,
				and in case it is different from 0 ,the record is updated in the
				database in the column <i>identifier</i>.
				
				The name is stored in clear text in the database in the column 
				<i>name</i>.
				
				The field array is first converted to a byte stream using the method
				@link fieldStream @/link and then it is crypted using the password 
				from the parameter <i>in_passwordHash</i>.
				To crypt the byte stream with automatically generated <i>salt</i> and
				<i>initialVector</i> that are stored in the columns <i>salt</i> and 
				<i>initialVector</i>.
	
    @param      in_database
				Database where the record should be stored.
		
    @param      in_passwordHash 
				Password to be used to crypt the recordFields.
				It is the hash value computed using SHA-1 of the original text
				password provided by the user and should always have a length of 20 bytes
				(160 bits).
*/
- (void)storeIn:(SHSqlDb*)in_database table:(NSString*)in_table withPasswordHash:(NSData*)in_passwordHash;

- (void)storeInFields:(SHSqlDb*)in_database table:(NSString*)in_table withPasswordHash:(NSData*)in_passwordHash;

/*!
    @method     readFrom:withIdentifier:withPasswordHash:
    
	@abstract   Loads a record with <i>identifier</i> from the database.
	
    @discussion The function creates and statement, executes it and calls the <i>loadFrom</i> method to load the
				results.
				This subroutine prepares a sql query and exeutes it, then calls 
				@link readFrom:withIdentifier:withPasswordHash: readFrom:withIdentifier:withPasswordHash: @/link
				to load the record
				data from the query results, decrypting the record fields.
				    
	@param		in_database
				Handler of the database from where the record should be loaded.

	@param		in_identifier
				Indentifier of the record to load from the database <i>in_database</i>.

	@param		in_passwordHash
				Password to be used to crypt the recordFields.
				It is the hash value computed using SHA-1 of the original text
				password provided by the user and should always have a length of 20 bytes
				(160 bits).
	
	@result     Returns YES if the record was succesfully loaded from the database, else it returns NO.
*/
- (BOOL)readFrom:(SHSqlDb*)in_database withIdentifier:(NSNumber*)in_identifier withPasswordHash:(NSData*)in_passwordHash;

/*!
    @method     loadFromQuery:withPasswordHash:
	
    @abstract   Loads the record attribute from the query <i>in_query</i>.

    @discussion This is an utility function to load all the attributes of the SHRecord from a sqlite query.
				The columns of the sqilte query should be (in the same order):
				
				1	Record identifier <i>identifier</i>.
				
				2	Record name <i>name</i>.
				
				3	Encrypted record fields byte stream <i>fieldDataArray</i>.
				
				4	Salt value to be used for uncrypting the record fields <i>salt</i>.
				
				5	Initial vector to be used for uncrypting the record fields <i>initVector</i>.
				
				6	Authentification code to verify that the passsword derived key is valid before 
					decrypting the record fields <i>authCode</i>.
					
				This method is used by the @link readFrom:withIdentifier:withPasswordHash: readFrom:withIdentifier:withPasswordHash: @/link
				method to load the fields from the sqilte query.
				It can be used also to load several SHRecord instances or the same one that is reused several times when a sqlite query
				returns more than one result.
	
	@param		in_query
				Sqlite query that has been successfully excetuted and for which the results are the ones specified in the
				<i>discussion</i> topic (in the same order).

	@param		in_passwordHash
				Password to be used to decrypt the recordFields.
				It is the hash value computed using SHA-1 of the original text
				password provided by the user and should always have a length of 20 bytes
				(160 bits).	
				
	@throws		TBD
*/
- (void)loadFromQuery:(SHSqlQuery*)in_query withPasswordHash:(NSData*)in_passwordHash;

@end
