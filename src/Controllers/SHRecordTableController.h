/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#import <Cocoa/Cocoa.h>
#import "SHSqlDb.h"
#import "SHRecord.h"
#import "SHUndoGroupRecord.h"
#import "SHSqlDbController.h"
#import "SHRecordDetailFieldController.h"
#import "SHRecordDetailGroupController.h"

@interface SHRecordTableController : NSObject {
	
	/*!
		@var		currentRecordFields
	
		@abstract	Currently selected record fields decrypted, if a record is selected.
		
		@discussion If a record is selected and the application is <i>unlocked</i> (so the
					password has been introduced by the user) then the record is loaded from
					the database and the record fields are decrypted.
					In other case the recordFields are reset to empty.
	*/	
	SHRecord* currentRecordFields;

	/*!
		@var		isRecordSelected
	
		@abstract	
	*/	
	BOOL isRecordSelected;

	/*!
		@var		recordNameArray
	
		@abstract	Pointer to the record name array of the currently selected group.
	*/	
	NSMutableArray* recordNameArray;
	
	/*
	
	*/
	slong32 currentGroupId;
	
	/*!
		@var		recordTableView
	
		@abstract	Outlet to the NSTableView used to display the record names.
	*/	
	IBOutlet NSTableView* recordTableView;
	
	/*!
		@var		addRecordButton
	
		@abstract	Outlet to the NSButton used to add the new records.
		
		@discussion	This outlet is used to enable/disable de add button when appropiate.
					If a group is selected the button will be enabled in other case disabled.
	*/	
	IBOutlet NSButton* addRecordButton;
		
	/*!
		@var		addFieldsButton
	
		@abstract	Outlet to the NSButton used to add the new records.
		
		@discussion	This outlet is used to enable/disable de add button when appropiate.
					If a record is selected and the application is unlocked the button 
					will be enabled in other case disabled.
	*/	
	IBOutlet NSButton* addFieldsButton;

	IBOutlet NSButton* editFieldsButton;

	/*!
		@var		appController
	
		@abstract
	*/	
	IBOutlet SHSqlDbController* sqlDbController;
	
	/*!
		@var		recordDetailFieldController
	
		@abstract
	*/	
	IBOutlet SHRecordDetailFieldController* recordDetailFieldController;

	/*!
		@var		recordDetailGroupController
	
		@abstract
	*/	
	IBOutlet SHRecordDetailGroupController*	recordDetailGroupController;
}

/*!
	@methodgroup Initialization
*/

/*!
    @method     init

    @abstract   Initializes an allocated SHRecordTableController object.

    @discussion Calls the super init and returns the intialized object.
*/
- init;

/*!
    @method     dealloc

    @abstract   Deallocates the memory occupied by the receiver.

    @discussion If the <i>recordNameArray</i> attribute is allocated then it is released.
				The <i>currentRecordFields</i> is released.
*/
- (void)dealloc;

/*!
	@methodgroup Database Management
*/

/*!
    @method     loadFromSqlDbInGroup:

    @abstract   Reloads the recordTableView.

    @discussion Loads the records corresponding to the group <i>in_group</i> in the internal array and
				calls the reloadData method for the table. 
*/
- (void)loadFromSqlDbInGroup:(NSNumber*)in_group;

/*!
    @method     saveRecordFields

    @abstract   Saves the <i>currentRecordFields</i> in the database.

    @discussion Saves the <i>currentRecordFields</i> if a record is selected and
				the application is <i>unlocked</i> (so the password has been introduced 
				by the user) then the <i>currentRecordFields</i> are updated in the database
*/
- (void)saveRecordFields;

/*!
    @method     updateRecordFields

    @abstract   <#(brief description)#>

    @discussion <#(comprehensive description)#>

*/
- (void)updateRecordFields;

/*!
    @method     selectRowIndex:

    @abstract   Selects the row <i>in_rowIndex</i> in the tableView associated to the controller.

*/
- (void)selectRowIndex:(ulong32)in_rowIndex;

/*!
    @method     selectedRow:

    @abstract   Returns the current selected row in the tableView associated to the controller.

*/
- (ulong32)selectedRow;

- (void)updateAddRecordButton;

/*!
	@methodgroup Tableview Data Source
*/

/*!
    @method     numberOfRowsInTableView:

    @abstract   Returns the number of records in the table.

    @discussion This is a method called by the <i><recordTableView</i>
				as this controller acts as a dellegate for its NSTableView.
				The number of records returned is the number of elements in
				<i>recordNameArray</i>.

    @param      in_tableView NSTableView that calls the method.

    @result     NSInteger containing the number of rows in the table.

*/
- (NSInteger)numberOfRowsInTableView:(NSTableView*)in_tableView;

/*!
    @method     tableView:objectValueForTableColumn:row:

    @abstract   Returns the valus for the row <i>in_row</i> and column <i>in_column</i>.

    @discussion This function is called by <i>recordTableView</i> to refresh the cell 
				in <i>in_row</i>, <i>in_column</i>.
				
				Returns the <i>name</i> of the SHRecordName object at the index <i>in_row</i>
				of the <i>recordNameArray</i> attribute.

    @param      in_tableView NSTableView that calls the method.

    @param      in_column Object represeing the column for which the value is requested.

    @param      in_row Index of the row for which the valie is requested.
	
	@result     NSString with the value to be displayed in the cell <i>in_row</i>, <i>in_column</i>.

*/
- (id)tableView:(NSTableView*)in_tableView objectValueForTableColumn:(NSTableColumn*)in_column row:(NSInteger)in_row;

/*!
    @method     tableView:setObjectValue:forTableColumn:row:

    @abstract   Updates the value of the record field for hte row <i>in_row</i> and column <i>in_column</i>.

    @discussion This function is called by <i>recordTableView</i> to update the cell 
				in <i>in_row</i>, <i>in_column</i>.
				
				Updates the <i>name</i> of the SHRecordName object
				at the index <i>in_row</i> of the <i>recordNameArray</i> attribute.

    @param      in_tableView NSTableView that calls the method.
	
    @param      in_value NSString with the value that should be updated.
	
    @param      in_column Object represeing the column for which the value is requested.

    @param      in_row Index of the row for which the valie is requested.
*/
- (void)tableView:(NSTableView*)in_tableView setObjectValue:(id)anObject forTableColumn:(NSTableColumn*)in_column row:(NSInteger)in_row;

/*!
	@methodgroup TableView Delegate
*/

/*!
    @method     tableViewSelectionDidChange:

    @abstract   Manage the change in the selection of the recordTableView.

    @discussion First he recordFields are saved using the <i>saveRecordFields</i> method,
				then if there is a record selected and the application is <i>unlocked</i>
				(so the	password has been introduced by the user) the record fields are 
				loaded and decrypted for the new selected record, else the record fields
				are left empty.

    @param      in_notification Notification object that contains the NSTableView
				that has launched the notification.
*/
- (void)tableViewSelectionDidChange:(NSNotification*)in_notification;

/*!
	@methodgroup GUI Actions
*/

/*!
    @method     add:

    @abstract   Adds a new SHRecordName object to the <i>recordTableView</i>.

    @discussion Allocates and intialises a new SHRecordName with <i>name</i> "Untitled" 
				and the record fields are left empty, then the <i>recordTableView</i> 
				is reloaded.

    @param      in_sender GUI object invoking the action.
*/
- (IBAction)add:(id)in_sender;

/*!
    @method     delete:

    @abstract   Deletes the SHRecordName selected in the <i>recordTableView</i>.

    @discussion The SHRecordField corresponding to the currently selected row in the 
				<i>recordTableView</i> is removed from the <i>recordNameArray</i> and the 
				<i>fieldTableView</i> is reloaded.
				
				The removed record is also deleted from the database.

    @param      in_sender GUI object invoking the action.
*/
- (IBAction)delete:(id)in_sender;

- (void)deleteRecord;

- (void)deleteSheetDidEnd:(NSWindow*)in_sheet returnCode:(int)in_returnCode contextInfo:(void*)in_contextInfo;

- (void)undoAdd:(SHUndoGroupRecord*)in_undoRecord;

- (void)undoDelete:(SHUndoGroupRecord*)in_undoRecord;

- (void)undoEdit:(SHUndoGroupRecord*)in_undoRecord;

@end
