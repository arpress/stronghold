/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision: 71 $
 *
 *	$Author: jcarro $
 *
 *	$Date: 2008-03-30 03:24:17 +0200 (Sun, 30 Mar 2008) $
 *
 ****************************************************************************/

#import "Message/NSMailDelivery.h"
#import "SHExceptionController.h"

@implementation SHExceptionController

- (id)init {

	[super init];
		
	// Set the exception handler and mask.
	[[NSExceptionHandler defaultExceptionHandler] setDelegate: self];
	[[NSExceptionHandler defaultExceptionHandler] setExceptionHandlingMask: NSLogAndHandleEveryExceptionMask];
	
	return(self);
}

- (void)dealloc {

	[[NSExceptionHandler defaultExceptionHandler] setDelegate: nil];
	
	[super dealloc];
}

- (BOOL)shouldDisplayException:(NSException*)in_exception {
	
//	NSString* name = [in_exception name];
//	NSString* reason = [in_exception reason];
	
	// Compare the name and the reason with the names and reasons of the exceptions we do not
	// want to fordward.
//	if( ) {
//		return(NO);
//	}
	
	return(YES);
}

- (BOOL)exceptionHandler:(NSExceptionHandler*)in_sender shouldLogException:(NSException*)in_exception mask:(ulong32)in_mask {

	return(YES);
}

- (BOOL)exceptionHandler:(NSExceptionHandler*)in_sender shouldHandleException:(NSException*)in_exception mask:(ulong32)in_mask {

	// if the exception isn't filtered by shouldDisplayException, display the information to the user
	if ([self shouldDisplayException: in_exception]) {
	
		[self displayExceptionPanelWithException: in_exception];
	}
    
    return(YES);
}

- (void)initPanelFromBundle {

    if ( !exceptionWindow ) {
	
		BOOL nibDidLoad = [NSBundle loadNibNamed: @"ExceptionPanel" owner: self];
		if( !nibDidLoad ) {
			
			[NSException raise: SHGenericException format: @"ExceptionPanel nib failed to load"];
		}
	}
}

- (void)displayExceptionPanelWithException:(NSException*)in_exception {

	[self initPanelFromBundle];
	
	NSProcessInfo* processInfo = [NSProcessInfo processInfo];
	
	NSString* stackTrace = [[in_exception userInfo] objectForKey: NSStackTraceKey];
	
	if( stackTrace ) {
		
		NSMutableArray *args = [ NSMutableArray array ];
				
		[args addObject: @"-p"];
		[args addObject: [[NSNumber numberWithInt: [processInfo processIdentifier]] stringValue]];
		
		NSScanner* scanner = [NSScanner scannerWithString: stackTrace];
		NSString* output;
		
		NSCharacterSet* whitespacesSet = [NSCharacterSet whitespaceCharacterSet];
		
		while( [scanner scanUpToCharactersFromSet: whitespacesSet intoString: &output]) {

			[args addObject: output];
		}
		
		
		NSPipe *atosOutputPipe = [[NSPipe alloc] init];
		NSTask *atosTask = [[NSTask alloc] init];
		
		[atosTask setLaunchPath: @"/usr/bin/atos"];
		[atosTask setArguments: args];
		[atosTask setStandardOutput: atosOutputPipe];
		
		[atosTask launch];
		[atosTask waitUntilExit];
		
		int atosStatus = [atosTask terminationStatus];
		
		if( atosStatus == 0 ) {
			
			NSData* atosOutputData = [[atosOutputPipe fileHandleForReading] availableData];
			
			NSMutableString* textViewString = [[exceptionDescTextView textStorage] mutableString];

			NSString* atosOutputString = [[NSString alloc] initWithData: atosOutputData encoding: NSUTF8StringEncoding];			
			
			[textViewString setString: @""];
			
			[textViewString appendFormat: @"System: %@ %@\n\nMemory: %.2f mb\n\n",
				[processInfo operatingSystemName], [processInfo operatingSystemVersionString],
				[processInfo physicalMemory] / (1024.0*1024.0)];
							
			[textViewString appendFormat: @"Exception: %@\n\nReason: %@\n\nStack trace:\n\n%@",
				[in_exception name], [in_exception reason], atosOutputString];
				
			[atosOutputString release];
		}
		
		[atosTask release];
		[atosOutputPipe release];
	}

	[exceptionWindow center];
    [exceptionWindow makeKeyAndOrderFront:nil];
	
	[NSApp runModalForWindow: exceptionWindow];
}

- (IBAction)emailException:(id)in_sender {

	NSString* problemDescString		= [problemDescTextView string];
	NSString* exceptionDescString	= [exceptionDescTextView string];
	
	NSString* messageText = [NSString 
		stringWithFormat: @"Problem description:\n\n%@\n -------------- \n\nException description:\n\n%@\n -------------- \n", 
		problemDescString, exceptionDescString];
		
	[[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString: [NSString stringWithFormat: @"mailto:%@?SUBJECT=%@&BODY=%@", 
		@"jcarro@users.sourceforge.net", @"Stronghold%20Exception%20Report", 
		[messageText stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding] ] ] ];
	
	
	[self endPanel: self];
}

- (IBAction)endPanel:(id)in_sender {
	[exceptionWindow close];
	
	[NSApp stopModal];
	[NSApp terminate: self];
}

@end
