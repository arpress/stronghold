/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#import <Cocoa/Cocoa.h>
#import "SHSqlDbController.h"
#import "SHGroupName.h"
#import "SHBlowfishCypher.h"
#import "SHKeyDeriver.h"
#import "SHHmac.h"
#import "SHUndoDetailGroup.h"


@class SHGroupTableController;

@interface SHRecordDetailGroupController : NSObject {
			
	/*!
		@var
	
		@abstract	Identifier of the record for which the group are displayed.
	*/	
	NSNumber* recordIdentifier;
	
	/*!
		@var
	
		@abstract	Array of the group identifiers selected in the record.
	*/	
	NSMutableSet* groupIdSet;
	
	/*!
		@var
	
		@abstract	Outlet to the NSTableView used to display the groups for
					which the record belongs to.
	*/	
	IBOutlet NSTableView* groupTableView;

	/*!
		@var
	
		@abstract	
	*/
	IBOutlet SHSqlDbController* sqlDbController;
		
	/*!
		@var
	
		@abstract	
	*/
	IBOutlet SHGroupTableController* groupTableController;
}

/*!
	@methodgroup Initialization
*/

/*!
    @method     init

    @abstract   Initializes an allocated SHRecordDetailGroupController object.

    @discussion Calls the super init and returns the intialized object.
*/
- init;

/*!
    @method     dealloc

    @abstract   Deallocates the memory occupied by the receiver.

    @discussion If the <i>groupIdSet</i> attribute is allocated then it is released.
*/
- (void)dealloc;

/*!
	@methodgroup Accessors
*/

/*!
    @method     recordIdentifier

    @abstract   Returns the value of the <i>recordIdentifier</i> attribute.

    @result     NSNumber corresponding tot he <i>recordIdentifier</i>.
*/
- (NSNumber*)recordIdentifier;

/*!
    @method     setRecordIdentifier:

    @abstract   Sets the value of the <i>recordIndentifier</i> attribute.

    @param      in_recordIdentifier  New value for the <i>recordIndentifier</i> attribute.
*/
- (void)setRecordIdentifier:(NSNumber*)in_recordIdentifier;

/*!
	@methodgroup Group management
*/

/*!
    @method     updateGroupTableView

    @abstract   Refresh the table of groups of the record when the record list changes.
    @discussion The delegate method @link tableView:objectValueForTableColumn:row: tableView:objectValueForTableColumn:row:@/link
				access the array of groupName objects from <i>groupTableController</i> so reloading the data will refresh
				the list of groups in the SHRecordDetailGroupController.

*/
- (void)updateGroupTableView;

/*!
	@methodgroup Database Management
*/

/*!
    @method     loadFromSqlDb

    @abstract   Reloads the <i>groupIdSet</i> from the database.

    @discussion Loads the groups for which the record <i><recordIdentifier/i> in the 
				internal set and calls the reloadData method for the table. 
*/
- (void)loadFromSqlDb;

- (void)updateLockStatus;

/*!
	@methodgroup Tableview Data Source
*/

/*!
    @method     numberOfRowsInTableView:

    @abstract   Returns the number of rfields in the table.

    @discussion This is a method called by the <i><groupTableView</i>
				as this controller acts as a dellegate for its NSTableView.

    @param      in_tableView NSTableView that calls the method.

    @result     NSInteger containing the number of rows in the table.

*/
- (NSInteger)numberOfRowsInTableView:(NSTableView*)in_tableView;

/*!
    @method     tableView:objectValueForTableColumn:row:

    @abstract   Returns the valus for the row <i>in_row</i> and column <i>in_column</i>.

    @discussion This function is called by <i>groupTableView</i> to refresh the cell 
				in <i>in_row</i>, <i>in_column</i>.
				
				Returns the <i>YES</i> if the <i>groupId</i> of the group at index <i>in_row</i>
				is in the <i>groupIdSet</i> else it returns <i>NO</i>.

    @param      in_tableView NSTableView that calls the method.

    @param      in_column Object representing the column for which the value is requested.

    @param      in_row Index of the row for which the value is requested.
	
	@result     NSNumber build from the boolean indicating if the group is in the <i>groupIdSet</i>.

*/
- (id)tableView:(NSTableView*)pTableView objectValueForTableColumn:(NSTableColumn*)in_column row:(NSInteger)in_row;

/*!
    @method     tableView:setObjectValue:forTableColumn:row:

    @abstract   Updates the value of the group for the row <i>in_row</i> and column <i>in_column</i>.

    @discussion This function is called by <i>groupTableView</i> to update the cell 
				in <i>in_row</i>, <i>in_column</i>.
				
				If the checkbox for the element in row <i>in_row</i> is selected then the corresponding
				<i>groupId</i> is added to the <i>groupIdSet</i> else it is removed.
				
    @param      in_tableView NSTableView that calls the method.
	
    @param      in_value NSNumber corresponding to a boolean indicating if the <i>groupId</i> corresponding to the
				element in <i>in_row</i>.

    @param      in_column Object represeing the column for which the value is requested.

    @param      in_row Index of the row for which the value should be updated.
*/
- (void)tableView:(NSTableView*)pTableView setObjectValue:(id)pNewValue forTableColumn:(NSTableColumn*)pTableColumn row:(NSInteger)in_row; 

/*!
	@methodgroup Tableview Delegate
*/

/*!
    @method     tableView:willDisplayCell:forTableColumn:row:

    @abstract   Updates the cell corresponding to <i>in_row</i>, <i>in_column</i> before it is displayed.

    @discussion The checkbox is not an standard cell that can be updated with an string. Before the cell
				is displayed the <i>groupTableView</i> calls this method to update the checkbox, depending 
				wether the identifier corresponding to the element in <i>in_row</i>, <i>in_column</i> is in
				the <i>groupIdSet</i> or not.

    @param      in_tableView NSTableView that calls the method.
	
    @param      inout_cell NSButtonCell to update.

    @param      in_column Object represeing the column for which the value is requested.

    @param      in_row Index of the row for which the value should be updated.
*/
- (void)tableView:(NSTableView*)pTableView willDisplayCell:(id)inout_cell forTableColumn:(NSTableColumn*)in_column row:(NSInteger)in_row;

- (void)undoAdd:(SHUndoDetailGroup*)in_undoDetailGroup;

- (void)undoDelete:(SHUndoDetailGroup*)in_undoDetailGroup;

@end
