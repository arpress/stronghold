/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#import "SHIntialPasswordSheetController.h"

@implementation SHIntialPasswordSheetController

- (void)controlTextDidChange:(NSNotification*)in_notification {
	
	NSSecureTextField* inputField;	
	inputField = [in_notification object];
	
	if (inputField == passwordField) {
		[verifyField setStringValue: @""];
		[setPasswordButton setEnabled: NO];

		NSString* password = [passwordField stringValue];
		
		if ([password length] > 7) {
			[verifyField setEnabled: YES];
		} else {
			[verifyField setEnabled: NO];
		}
		
	} else {
		NSString* password = [passwordField stringValue];
		NSString* verifyPassword = [verifyField stringValue];
		
		if ([password isEqual: verifyPassword]) {
			[setPasswordButton setEnabled: YES];
		} else {
			[setPasswordButton setEnabled: NO];
		}
	}

}

- (IBAction)endSheet:(id)in_sender {

	// Hide the password sheet.
	[sheet orderOut: in_sender];
		
	if (in_sender == setPasswordButton) {

		//	Set the password to the redord detail field controller.
		NSString *passwordString;
		NSData*	passwordHash;
		passwordString = [passwordField stringValue];
		
		passwordHash = [sqlDbController computeHashFromString: passwordString];
		[sqlDbController storePasswordHash: passwordHash];
		
		[sqlDbController updatePasswordHash: passwordHash];

		//	Return the code to the NSApp.
		[NSApp endSheet: sheet returnCode: 1];

	} else {

		//	Return the code to the NSApp.
		[NSApp endSheet: sheet];
				
		[NSApp terminate: self];
	}
	
	// Clear the secure text fields.
	[passwordField setStringValue: @""];
	[verifyField setStringValue: @""];
	
}

@end
