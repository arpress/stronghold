/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#import "SHPasswordSheetController.h"
#import "SHAppController.h"
#import "NSWindow+SHGuiEffects.h"

@implementation SHPasswordSheetController

- (void)controlTextDidChange:(NSNotification*)in_notification {
	
	NSString* passwordString = [passwordField stringValue];
		
	if ([passwordString length] > 7) {

		[okButton setEnabled: YES];

	} else {

		[okButton setEnabled: NO];

	}
}

- (IBAction)endSheet:(id)in_sender {
		
	if (in_sender == okButton) {

		//	Set the password to the redord detail field controller.
		NSString* passwordString = [passwordField stringValue];
		NSMutableData* passwordHash = [sqlDbController computeHashFromString: passwordString];

		BOOL isValidPassword = [sqlDbController isValidPasswordHash: passwordHash];
		
		//
		//	If the password is not valid exit without closing the sheet.
		//
		if( !isValidPassword ) {
			[passwordField setStringValue: @""];
			
			[sheet shake];
			
			return;
		}
			
		[sqlDbController updatePasswordHash: passwordHash];

		//	Return the code to the NSApp.
		[NSApp endSheet: sheet];

	} else {

		//	Return the code to the NSApp.
		[NSApp endSheet: sheet];
	}
	
	// Clear the secure text fields.
	[passwordField setStringValue: @""];

	// Hide the password sheet.
	[sheet orderOut: in_sender];
}

@end
