/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#import <Cocoa/Cocoa.h>
#import "SHSqlDb.h"
#import "SHCypher.h"

@interface SHSqlDbController : NSObject {

	SHSqlDb* database;
	
	SHCypher* cypher;

	NSMutableData* passwordHash;

	BOOL isLocked;
}

- init;

- (void)release;

- (BOOL)isLocked;

- (BOOL)isUnlocked;

- (SHSqlDb*)database;

- (void)openDatabase;

- (void)closeDatabase;

- (void) initTables;

- (NSData*)passwordHash;

- (NSMutableData*)computeHashFromString:(NSString*)in_passwordString;

- (void)storePasswordHash:(NSData*)in_passwordHash;

- (void)updatePasswordHash:(NSData*)in_passwordHash;

- (BOOL)checkPasswordExists;

- (BOOL)isValidPasswordHash:(NSData*)in_passwordHash;

- (void)encryptRecordsUsingPasswordHash:(NSData*)in_oldPasswordHash withPasswordHash:(NSData*)in_newPasswordHash
	progressIndicator:(NSProgressIndicator*)in_progressIndicator;
	
- (void)swapRecordTables;


@end
