/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#import <Cocoa/Cocoa.h>
#import "RBSplitView.h"
#import "SHSqlDbController.h"
#import "SHRecordTableController.h"
#import "SHExceptionController.h"
#import "SS_PrefsController.h"

@interface SHAppController : NSObject {
		
	IBOutlet RBSplitSubview* groupSubview;

	IBOutlet NSButton* groupAddButton;

	IBOutlet RBSplitSubview* recordSubview;

	IBOutlet NSButton* recordAddButton;

	IBOutlet RBSplitSubview* fieldSubview;

	IBOutlet NSButton* fieldAddButton;

	IBOutlet NSButton* fieldEditButton;
	
	IBOutlet NSBox*	   fieldButtonBox;
	
	IBOutlet NSWindow* mainWindow;

	IBOutlet NSWindow* aboutWindow;

	IBOutlet NSWindow* initialPasswordSheet;

	IBOutlet NSWindow* passwordSheet;

	IBOutlet NSWindow* changePasswordSheet;

	IBOutlet NSButton* lockToggleButton;

	IBOutlet NSMenuItem* lockMenuItem;

	IBOutlet NSMenuItem* dockLockMenuItem;
	
	SS_PrefsController *prefController;
	
	IBOutlet SHSqlDbController* sqlDbController;
		
	IBOutlet SHRecordTableController* recordTableController;
	
	IBOutlet SHGroupTableController* groupTableController;
	
	IBOutlet SHExceptionController* exceptionController;
}

- init;

- (void)dealloc;
	
- (void)awakeFromNib;

- (void)initDefaultPreferences;

- (void)applicationWillTerminate:(NSNotification*)in_notification;

- (BOOL)applicationShouldHandleReopen:(NSApplication*)in_application hasVisibleWindows:(BOOL)in_hasVisibleWindows;

- (void)selectGroupRow:(ulong32)in_groupRow recordRow:(ulong32)in_recordRow;

- (ulong32)selectedGroupRow;

- (ulong32)selectedRecordRow;

//
//
//

- (IBAction)expandCollapseGroupSubview:(id)in_sender;

//
//	Action raised the lock button is clicked
//

- (IBAction)toggleLock:(id)in_sender;

- (void) updateToggleLock;

- (IBAction)showPreferences:(id)in_sender;

//
//
//

- (IBAction)showChangePasswordSheet:(id)in_sender;

//
//
//

// Application delegate

- (void)applicationDidFinishLaunching:(NSNotification*)in_notification;

- (IBAction)showChangePasswordSheet:(id)in_sender;

- (void)passwordSheetDidEnd:(NSWindow*)in_sheet returnCode:(int)in_returnCode contextInfo:(void*)in_contextInfo;

- (IBAction)showAboutPanel:(id)in_sender;


//- (NSError*)application:(NSApplication*)application willPresentError:(NSError*)error;

- (IBAction)sendFeedback:(id)in_sender;

@end
