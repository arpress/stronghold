/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#import "SHRecordTableController.h"
#import "SHRecordName.h"
#import "SHAppController.h"

@implementation SHRecordTableController

- init {
	[super init];
	
	currentRecordFields = [[SHRecord alloc] init];
	recordNameArray = [[NSMutableArray alloc] init];
	
	isRecordSelected = NO;
	
	return (self);
}

- (void)dealloc {

	[currentRecordFields release];
	[recordNameArray release];

	[super dealloc];
}

- (void)loadFromSqlDbInGroup:(NSNumber*)in_group {

	// Deleselect all the records.
	// This should be down at the beginning so the
	// record information (fields and groups selected) for
	// the record are stored in the database.
	[recordTableView deselectAll: self];

	// Remove all the records in the array
	[recordNameArray removeAllObjects];

	currentGroupId = [in_group longValue];

	NSString *queryString;
	
	// We have not selected any group we have nothing to do.
	if(currentGroupId == 0) {
		queryString = nil;
	}
	
	// If we have selected to display ALL the records
	if(currentGroupId == -2) {		
		queryString = [NSString stringWithFormat: @"SELECT identifier,name FROM recordTable"];
	}

	// If we have selected to display only the UGROUPED records
	if(currentGroupId == -1) {		
		queryString = [NSString stringWithFormat: @"SELECT identifier,name FROM recordTable WHERE \
			(identifier NOT IN (SELECT recordId FROM groupRecordCrossTable))"];
	}
	
	// If we have choosen to display the records in one group
	if(currentGroupId > 0) {
		queryString = [NSString stringWithFormat: @"SELECT identifier,name FROM recordTable WHERE \
			(identifier IN (SELECT recordId FROM groupRecordCrossTable WHERE (groupId==%d)))", currentGroupId];	
	}
	
	if (currentGroupId < -2) {
		[NSException raise: SHGenericException format: @"Invalid group identifier: %d", currentGroupId];
	}
	
	// If we have selcted a group then
	if (queryString) {
	
		SHSqlDb* database = [sqlDbController database];
		
		if(!database) {
			[NSException raise: SHGenericException format: @"Database handler is nil"];
		}
		
		SHSqlQuery* query = [database query: queryString];
	
		SHRecordName* recordName;
	
		BOOL foundRecord = [query stepAndFinalize];
		
		while(foundRecord) {
			recordName = [[SHRecordName alloc] init];
			
			[recordName loadFromQuery: query];
			
			[recordNameArray addObject: recordName];
			
			[recordName release];
			
			foundRecord = [query stepAndFinalize];
		}		
	}
	
	[self updateAddRecordButton];
	[recordTableView reloadData];
}

- (void)saveRecordFields {

	if(isRecordSelected && [sqlDbController isUnlocked]) {

		SHSqlDb* database = [sqlDbController database];
		
		if(!database) {
			[NSException raise: SHGenericException format: @"Database handler is nil"];
		}
		
		NSData* passwordHash = [sqlDbController passwordHash];		
		
		[currentRecordFields storeInFields: database table: @"recordTable" withPasswordHash: passwordHash];
	}
}

- (void)updateRecordFields {
	
	NSInteger selectedRow = [recordTableView selectedRow];

	if( selectedRow >= 0 ) {
	
		// Set the flag to indicate that a record is now selected.
		isRecordSelected = YES;		

		SHRecordName* recordName = [recordNameArray objectAtIndex: selectedRow];

		[recordDetailGroupController setRecordIdentifier: [recordName identifier]];
		
		// If the appplication is not locked then load and decrypt the fieldArray for the selected
		// record.
		if( [sqlDbController isUnlocked] ) {

			SHSqlDb* database = [sqlDbController database];
			if(!database) {
				[NSException raise: SHGenericException format: @"Database handler is nil"];
			}
			
			NSData* passwordHash = [sqlDbController passwordHash];
			[currentRecordFields readFrom: database withIdentifier: [recordName identifier]
				withPasswordHash: passwordHash];
				
			[recordDetailFieldController setFieldArray: [currentRecordFields fieldArray]];
			
			[editFieldsButton setEnabled: YES];
			
			if ( [editFieldsButton state] == NSOnState ) {
				[addFieldsButton setEnabled: YES];			
			} else {
				[addFieldsButton setEnabled: NO];			
			}
			
		} else {
		
			// TODO Reset the record fields			
			[recordDetailFieldController setFieldArray: nil];	

			[editFieldsButton setEnabled: NO];
			[addFieldsButton setEnabled: NO];						
		}
	} else {
	
		// No record has been selected.
		isRecordSelected = NO;	

		// TODO Reset ther record fields	
		[recordDetailFieldController setFieldArray: nil];	
		[recordDetailGroupController setRecordIdentifier: nil];
		
		[addFieldsButton setEnabled: NO];			
		[editFieldsButton setEnabled: NO];				
	}	
}

// Methods to operate as the data source of the groupTable n the GUI.

- (NSInteger)numberOfRowsInTableView:(NSTableView*)in_tableView {

	return([recordNameArray count]);

}

- (id)tableView:(NSTableView*)in_tableView objectValueForTableColumn:(NSTableColumn*)in_column row:(NSInteger)in_row {

	SHRecordName* recordName = [recordNameArray objectAtIndex: in_row];

	NSString* value = [recordName valueForKey: [in_column identifier]];
	
	return(value);
}

- (void)tableView:(NSTableView*)in_tableView setObjectValue:(id)in_value forTableColumn:(NSTableColumn*)in_column row:(NSInteger)in_row {

	SHRecordName* recordName = [recordNameArray objectAtIndex: in_row];

	id currentValue = [recordName valueForKey: [in_column identifier]];
	
	if( ![currentValue isEqual: in_value] ) {

		SHSqlDb* database = [sqlDbController database];
		
		if( !database ) {
			[NSException raise: SHGenericException format: @"Database handler is nil"];	
		}
		
		
		ulong32 groupRow	= [[NSApp delegate] selectedGroupRow];

		SHUndoGroupRecord* undoRecord = [[SHUndoGroupRecord alloc] initWithIdentifier: [recordName identifier]
			recordType: SH_UNDO_RECORD_ITEM groupRow: groupRow recordRow:in_row database: database];

		[recordName setValue: in_value forKey: [in_column identifier]];
		[recordName storeIn: database];
		

		NSUndoManager* undoManager = [recordTableView undoManager];
		
		[undoManager registerUndoWithTarget:self selector:@selector(undoEdit:) object: undoRecord];
		[undoManager setActionName: @"Edit Record"];
		
		[undoRecord release];
	}
}

// Methods corresponding to the delegate of the NSTableView.
- (void)tableViewSelectionDidChange:(NSNotification*)in_notification {

	if( [sqlDbController isUnlocked] ) {

		[self saveRecordFields];
	}

	[self updateRecordFields];
}

- (void)selectRowIndex:(ulong32)in_rowIndex {
	[recordTableView deselectAll: self];
	[recordTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:in_rowIndex] byExtendingSelection:NO];
}

- (ulong32)selectedRow {
	ulong32 selectedRow = [recordTableView selectedRow];
	
	return(selectedRow);
}

- (void)updateAddRecordButton {

	if( (currentGroupId != 0) && [sqlDbController isUnlocked] ) {
			
		[addRecordButton setEnabled: YES];
	
	} else {
	
		[addRecordButton setEnabled: NO];
	
	}
	
	[recordDetailGroupController updateLockStatus];
}

// Methods for the GUI management.

- (IBAction)add:(id)in_sender {

	SHSqlDb* database = [sqlDbController database];	
	if( !database ) {
		[NSException raise: SHGenericException format: @"Database handler is nil"];	
	}
	
	SHRecordName* newRecordName = [[SHRecordName alloc] initWithName: @"Untitled"];
	
	[newRecordName storeIn: database];
	
	[recordNameArray addObject: newRecordName];
		
	// Add the record to the current group, if the selected group is not "all" or "ungrouped".
	
	if(currentGroupId > 1) {
		NSString* queryString;
		queryString = [NSString stringWithFormat: @"INSERT INTO groupRecordCrossTable (recordId,groupId) VALUES (%d,%d)", 
					  [[newRecordName identifier] longValue], currentGroupId];
					
		SHSqlQuery* sqlQuery = [database query: queryString];

		[sqlQuery execute];
	}
	
	[recordTableView reloadData];
	NSInteger lastRowIndex = [recordTableView numberOfRows] - 1;

	ulong32 groupRow	= [[NSApp delegate] selectedGroupRow];
	SHUndoGroupRecord* undoRecord = [[SHUndoGroupRecord alloc] initWithIdentifier: [newRecordName identifier]
		recordType: SH_UNDO_RECORD_ITEM groupRow: groupRow recordRow:lastRowIndex database: database];
	
	NSUndoManager* undoManager = [recordTableView undoManager];
	[undoManager registerUndoWithTarget:self selector:@selector(undoAdd:) object: undoRecord];	
	[undoManager setActionName: @"Add Record"];
	
	[undoRecord release];
	
	//
	[newRecordName release];
	
	// Select the last inserted row	
	[recordTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:lastRowIndex] byExtendingSelection:NO];
	[recordTableView editColumn:0 row:lastRowIndex withEvent:nil select:YES];
	
	
}

- (IBAction)delete:(id)in_sender {

	NSInteger selectedRow = [recordTableView selectedRow];

	if( selectedRow >= 0 && [sqlDbController isUnlocked] ) {
	
		// Request the user confirmation to delete the record if the
		// preferences are configured to do so.
		NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];		
		if([userDefaults boolForKey: @"waningBeforeDelete"]) {

			SHRecordName* recordName = [recordNameArray objectAtIndex: selectedRow];

			NSString* alertMessage = [NSString stringWithFormat: @"Are you sure you want to delete the record \"%@\"?",
										[recordName name]];
									
			
			NSAlert* alertPanel = [NSAlert alertWithMessageText: alertMessage
									defaultButton: @"Delete" alternateButton: @"Cancel" otherButton: nil
									informativeTextWithFormat: @""];

			[alertPanel setShowsHelp: YES];

			[alertPanel beginSheetModalForWindow: [NSApp mainWindow] modalDelegate:self
				didEndSelector: @selector(deleteSheetDidEnd:returnCode:contextInfo:) contextInfo: nil];			
		} else {		
			[self deleteRecord];
		}

	} else {
		
		// Send a beep because trying to delete a record when no record is selected.
		NSBeep();
	}
}

- (void)deleteSheetDidEnd:(NSWindow*)in_sheet returnCode:(int)in_returnCode contextInfo:(void*)in_contextInfo {
	
	if(in_returnCode == NSAlertDefaultReturn) {
		[self deleteRecord];
	}
}

- (void)deleteRecord {

	NSInteger selectedRow = [recordTableView selectedRow];

	SHRecordName* recordName = [recordNameArray objectAtIndex: selectedRow];

	SHSqlDb* database = [sqlDbController database];
	
	if(!database) {
		[NSException raise: SHGenericException format: @"Database handler is nil"];	
	}
		
	//
	//	Get the current values of the record for a future undo
	//
	ulong32 groupRow	= [[NSApp delegate] selectedGroupRow];
	SHUndoGroupRecord* undoRecord = [[SHUndoGroupRecord alloc] initWithIdentifier: [recordName identifier]
		recordType: SH_UNDO_RECORD_ITEM groupRow: groupRow recordRow:selectedRow database: database];

	NSUndoManager* undoManager = [recordTableView undoManager];
	[undoManager registerUndoWithTarget:self selector:@selector(undoDelete:) object: undoRecord];
	[undoManager setActionName: @"Delete Record"];
	
	//
	//	Now we remove the record.
	//
	[recordName removeFrom: database];
	[recordNameArray removeObjectAtIndex: selectedRow];
	
	// Get the remaining records in the list.
	ulong32 numRecords = [recordNameArray count];
	
	// If there are more records in the list, then we have to apply the selection to
	// another record.
	// If there are no more records in the list then nullify the selections in the 
	// detail and detailGroup tables.
	if( numRecords ) {
	
		// If we have deleted the last rcord in the list then
		// select the new last record.
		// Else we keep the same selection index, but we have to reload the detailGroup
		// table and the detail table (if the password is loaded).
		if( selectedRow >= numRecords ) {
						
			[recordTableView selectRowIndexes: [NSIndexSet indexSetWithIndex: (numRecords-1)]
				byExtendingSelection: NO];
				
		} else {
			recordName = [recordNameArray objectAtIndex: selectedRow];					

			[recordDetailGroupController setRecordIdentifier: [recordName identifier]];

			
			// If the appplication is not locked then load and decrypt the fieldArray for the selected
			// record.
			if( [sqlDbController isUnlocked] ) {
		
				NSData* passwordHash = [sqlDbController passwordHash];
				[currentRecordFields readFrom: database withIdentifier: [recordName identifier]	
					withPasswordHash: passwordHash];
					
				[recordDetailFieldController setFieldArray: [currentRecordFields fieldArray]];	
			} else {
				// TODO Reset ther record fields
				[recordDetailFieldController setFieldArray: nil];	
				[recordDetailGroupController setRecordIdentifier: nil];
			}
		}
	
		// A record is now serlected.
		isRecordSelected = YES;		
	
	} else {
		// TODO Reset ther record fields
		[recordDetailFieldController setFieldArray: nil];	
		[recordDetailGroupController setRecordIdentifier: nil];
		
		// No record is now selected.
		isRecordSelected = NO;		
	}
	
	[recordTableView reloadData];	
}

- (void)undoAdd:(SHUndoGroupRecord*)in_undoRecord {
	SHSqlDb* database = [sqlDbController database];

	if( !database ) {
		[NSException raise: SHGenericException format: @"Database handler is nil"];	
	}

	[in_undoRecord deleteFromDatabase: database];

	NSUndoManager* undoManager = [recordTableView undoManager];
	
	[undoManager registerUndoWithTarget:self selector:@selector(undoDelete:) object: in_undoRecord];

	[[NSApp delegate] selectGroupRow: [in_undoRecord groupRow] recordRow: [in_undoRecord recordRow]];
	
	[recordTableView reloadData];
}

- (void)undoDelete:(SHUndoGroupRecord*)in_undoRecord {
	SHSqlDb* database = [sqlDbController database];

	if( !database ) {
		[NSException raise: SHGenericException format: @"Database handler is nil"];	
	}

	[in_undoRecord insertIntoDatabase: database];

	NSUndoManager* undoManager = [recordTableView undoManager];
	
	[undoManager registerUndoWithTarget:self selector:@selector(undoAdd:) object: in_undoRecord];

	[[NSApp delegate] selectGroupRow: [in_undoRecord groupRow] recordRow: [in_undoRecord recordRow]];
	
	[recordTableView reloadData];
}

- (void)undoEdit:(SHUndoGroupRecord*)in_undoRecord {
	SHSqlDb* database = [sqlDbController database];

	if( !database ) {
		[NSException raise: SHGenericException format: @"Database handler is nil"];	
	}

	SHUndoGroupRecord* undoRecord = [[SHUndoGroupRecord alloc] initWithIdentifier: [in_undoRecord identifier]
		recordType: SH_UNDO_RECORD_ITEM groupRow: [in_undoRecord groupRow] recordRow: [in_undoRecord recordRow]
		database: database];

	[in_undoRecord insertIntoDatabase: database];

	NSUndoManager* undoManager = [recordTableView undoManager];
	
	[undoManager registerUndoWithTarget:self selector:@selector(undoEdit:) object: undoRecord];	
	[undoRecord release];

	[[NSApp delegate] selectGroupRow: [in_undoRecord groupRow] recordRow: [in_undoRecord recordRow]];
	
	[recordTableView reloadData];
}

@end
