/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/
 
#import <Cocoa/Cocoa.h>

#import "SHUndoDetailField.h"
#import "SHSqlDbController.h"

/*!
    @class       SHRecordDetailFieldController 

    @abstract    Controller the table of record fields.

    @discussion  Controller for the record fields.
				 contains an array of the record fields and an
				 outlet to the NSTableView to display the fields.
				 
				 The field array is loaded when a record is selected,
				 when the record selection changes the record fields are
				 stored in the database.
*/
@interface SHRecordDetailFieldController : NSObject {

	/*!
		@var		fieldArray
		
		@abstract	Pointer to the field array in the record being displayed.
	*/
	NSMutableArray* fieldArray;
	
	/*!
		@var		fieldTableView
		
		@abstract	Outlet to the NSTableView used to display the record fields.
	*/
	IBOutlet NSTableView* fieldTableView;

	IBOutlet NSButton*	addFieldsButton;	
	
	IBOutlet NSButton*	editFieldsButton;
	
	/*!
		@var		appController
	
		@abstract
	*/	
	IBOutlet SHSqlDbController* sqlDbController;

}

/*!
	@methodgroup Initialization
*/

/*!
    @method     init

    @abstract   Initializes an allocated SHRecordDetailFieldController object.

    @discussion Calls the super init and returns the intialized object.
*/
- init;

/*!
    @method     dealloc

    @abstract   Deallocates the memory occupied by the receiver.

    @discussion If the <i>fieldArray</i> attribute is allocated then it is released.
*/
- (void)dealloc;

/*!
	@methodgroup Accessors
*/

/*!
    @method     fieldArray

    @abstract   Returns the value of the <i>fieldArray</i> attribute.

    @result		NSMutableArray containing the array of fields for the record.
*/
- (NSMutableArray*)fieldArray;

/*!
    @method     setFieldArray:

    @abstract   Sets the value of the <i>setFieldArray</i> attribute.

    @param      in_fieldArray  New value for the <i>setFieldArray</i> attribute.

*/
- (void)setFieldArray:(NSMutableArray*)in_fieldArray;

/*!
	@methodgroup Tableview Delegation
*/

/*!
    @method     numberOfRowsInTableView:

    @abstract   Returns the number of rfields in the table.

    @discussion This is a method called by the <i><fieldTableView</i>
				as this controller acts as a dellegate for its NSTableView.

    @param      in_tableView NSTableView that calls the method.

    @result     NSInteger containing the number of rows in the table.

*/
- (NSInteger)numberOfRowsInTableView:(NSTableView*)in_tableView;

/*!
    @method     tableView:objectValueForTableColumn:row:

    @abstract   Returns the valus for the row <i>in_row</i> and column <i>in_column</i>.

    @discussion This function is called by <i>fieldTableView</i> to refresh the cell 
				in <i>in_row</i>, <i>in_column</i>.
				
				Returns the <i>name</i> or the <i>value</i> of the SHRecordField object
				at the index <i>in_row</i> of the <i>fieldArray</i> attribute.

    @param      in_tableView NSTableView that calls the method.

    @param      in_column Object represeing the column for which the value is requested.

    @param      in_row Index of the row for which the valie is requested.
	
	@result     NSString with the value to be displayed in the cell <i>in_row</i>, <i>in_column</i>.

*/
- (id)tableView:(NSTableView*)in_tableView objectValueForTableColumn:(NSTableColumn*)in_column row:(NSInteger)in_row;

/*!
    @method     tableView:setObjectValue:forTableColumn:row:

    @abstract   Updates the value of the record field for hte row <i>in_row</i> and column <i>in_column</i>.

    @discussion This function is called by <i>fieldTableView</i> to update the cell 
				in <i>in_row</i>, <i>in_column</i>.
				
				Updates the <i>name</i> or the <i>value</i> of the SHRecordField object
				at the index <i>in_row</i> of the <i>fieldArray</i> attribute.

    @param      in_tableView NSTableView that calls the method.
	
    @param      in_value NSString with the value that should be updated. Depending of 
				the value of <i>in_column</i> the nale or the value of the SHRecordField
				will be updated.

    @param      in_column Object represeing the column for which the value is requested.

    @param      in_row Index of the row for which the valie is requested.
*/
- (void)tableView:(NSTableView*)in_tableView setObjectValue:(id)in_value forTableColumn:(NSTableColumn*)in_column row:(NSInteger)in_row;

/*
*/
- (BOOL)tableView:(NSTableView*)in_tableView shouldEditTableColumn:(NSTableColumn*)in_column row:(NSInteger)in_row;

/*!
	@methodgroup GUI Actions
*/

/*!
    @method     add:

    @abstract   Adds a new SHRecordField object to the <i>fieldArray</i>.

    @discussion Allocates and intialises a new SHRecordField with <i>name</i> "Unnamed" 
				and <i>value</i> "Empty field" then appends this new object to the 
				<i>fieldArray</i>, then the <i>fieldTableView</i> is reloaded.

    @param      in_sender GUI object invoking the action.
*/
- (IBAction)add:(id)in_sender;

/*!
    @method     delete:

    @abstract   Deletes the SHRecordField selected in the <i>fieldTableView</i>.

    @discussion The SHRecordField corresponding to the currently selected row in the 
				<i>fieldTableView</i> is removed from the <i>fieldArray</i> and the 
				<i>fieldTableView</i> is reloaded.

    @param      in_sender GUI object invoking the action.
*/
- (IBAction)delete:(id)in_sender;

/*!
    @method     toggleEdit:

    @abstract   

    @discussion 

    @param      in_sender GUI object invoking the action.
*/
- (IBAction)toggleEdit:(id)in_sender;

- (void)deleteSheetDidEnd:(NSWindow*)in_sheet returnCode:(int)in_returnCode contextInfo:(void*)in_contextInfo;

- (void)deleteField;

@end
