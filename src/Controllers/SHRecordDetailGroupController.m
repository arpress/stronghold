/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#import <Foundation/NSRange.h>
#import "SHRecordDetailGroupController.h"
#import "SHGroupTableController.h"
#import "SHSqlQuery.h"
#import "SHAppController.h"

@implementation SHRecordDetailGroupController

- init {
	[super init];

	// Allocate and intialise the groupArray
	groupIdSet = [[NSMutableSet alloc] init];
					
	return (self);
}

- (void)dealloc {
	
	// Liberate the record identifier.
	[recordIdentifier release];
	
	// Liberate the array of groups.
	[groupIdSet release];
	groupIdSet = nil;
		
	[super dealloc];
}

- (NSNumber*)recordIdentifier {

	return(recordIdentifier);

}
  
- (void)setRecordIdentifier:(NSNumber*)in_recordIdentifier {
	
	[in_recordIdentifier retain];	
	[recordIdentifier release];
	recordIdentifier = in_recordIdentifier;
		
	[self loadFromSqlDb];		
}

- (void)updateGroupTableView {

	[groupTableView reloadData];

}

- (void)loadFromSqlDb {
	
	// Remove all the records in the array
	[groupIdSet removeAllObjects];
		
	// Check that there is record selected to load the groups.
	if(recordIdentifier) {

		SHSqlDb* database = [sqlDbController database];
		
		if(!database) {
			[NSException raise: SHGenericException format: @"Database handler is nil"];
		}

		NSString *queryString;

		queryString = [NSString stringWithFormat: @"SELECT groupId FROM groupRecordCrossTable WHERE (recordId==%d)", 
						[recordIdentifier longValue]];

		SHSqlQuery* query;
		query = [database query: queryString];
	
		BOOL foundRecord;
		NSNumber* groupId;	
		foundRecord = [query stepAndFinalize];
		
		while(foundRecord) {

			groupId = [query columnAsUnsignedLong: 0];						
			
			[groupIdSet addObject: groupId];
			
			foundRecord = [query stepAndFinalize];
		}
	}
	
	[groupTableView reloadData];
	[groupTableView deselectAll: self];	
}

- (void)updateLockStatus {

	if([sqlDbController isUnlocked]) {
		[groupTableView setEnabled: YES];
	} else {
		[groupTableView setEnabled: NO];
	}

}

// Methods to operate as the data source of the recordTableView in the GUI.
- (NSInteger)numberOfRowsInTableView:(NSTableView*)in_tableView {
	
	NSInteger numberOfRows = 0;

	if (recordIdentifier) {
		NSMutableArray* groupNameArray;
		groupNameArray = [groupTableController groupNameArray];
	
		numberOfRows = [groupNameArray count];
	}
	
	return(numberOfRows);
}

// The method does nothing, but avoid getting an error in the debugger.
- (id)tableView:(NSTableView*)pTableView objectValueForTableColumn:(NSTableColumn*)pTableColumn row:(NSInteger)pRowIndex {
	
	NSMutableArray* groupNameArray = [groupTableController groupNameArray];
	
	// Get the group name for the row.
	SHGroupName* groupName = [groupNameArray objectAtIndex: pRowIndex];
	NSNumber* groupIdentifier = [groupName identifier];
	
	// Verify if the record identifier is in the list of groups
	// for the selected record.
	BOOL isGroupSelected = [groupIdSet containsObject: groupIdentifier];
	
	return [NSNumber numberWithBool: isGroupSelected];
}

- (void)tableView:(NSTableView*)pTableView setObjectValue:(id)pNewValue forTableColumn:(NSTableColumn*)pTableColumn row:(NSInteger)pRowIndex {

	NSMutableArray* groupNameArray = [groupTableController groupNameArray];
	
	// Get the group name for the row.
	SHGroupName* groupName = [groupNameArray objectAtIndex: pRowIndex];
	NSNumber* groupIdentifier = [groupName identifier];

	// If the received value is true then we add the group identifier
	// to the set of groups to which the record belongs.
	// In other case we remove it (if it was in the set).
	ulong32 groupRow = [[NSApp delegate] selectedGroupRow];	
	ulong32 recordRow = [[NSApp delegate] selectedRecordRow];	

	SHUndoDetailGroup* undoDetailGroup = [[SHUndoDetailGroup alloc] 
		initWithGroupId: groupIdentifier recordId: recordIdentifier
		groupRow:groupRow recordRow:recordRow];

	NSUndoManager* undoManager = [groupTableView undoManager];			
	NSString *queryString;
	
	BOOL isGroupSelected = [pNewValue boolValue];
	
	if (isGroupSelected) {

		queryString = [NSString stringWithFormat: @"INSERT INTO groupRecordCrossTable (recordId,groupId) VALUES (%d,%d)", 
					[recordIdentifier longValue], [groupIdentifier longValue]];

		[undoManager registerUndoWithTarget:self selector:@selector(undoAdd:) object: undoDetailGroup];		
	} else {

		queryString = [NSString stringWithFormat: @"DELETE FROM groupRecordCrossTable WHERE (recordId==%d AND groupId==%d)", 
					[recordIdentifier longValue], [groupIdentifier longValue]];

		[undoManager registerUndoWithTarget:self selector:@selector(undoDelete:) object: undoDetailGroup];	
	}
	
	[undoManager setActionName: @"Modify Record"];
	[undoDetailGroup release];
	
	SHSqlDb* database = [sqlDbController database];
	if(!database) {
		[NSException raise: SHGenericException format: @"Database handler is nil"];
	}

	SHSqlQuery* sqlQuery = [database query: queryString];
	[sqlQuery execute];

	[[NSApp delegate] selectGroupRow: groupRow recordRow: recordRow];
	
}

- (void)tableView:(NSTableView*)pTableView willDisplayCell:(id)inout_cell forTableColumn:(NSTableColumn*)pTableCcolumn row:(NSInteger)pRowIndex {

	NSMutableArray* groupNameArray = [groupTableController groupNameArray];

	// Get the group name for the row.
	SHGroupName* groupName = [groupNameArray objectAtIndex: pRowIndex];
	NSString* name = [groupName name];
	
	NSButtonCell* checkButton = (NSButtonCell*)inout_cell;

	[checkButton setTitle: name];
}

- (void)undoAdd:(SHUndoDetailGroup*)in_undoDetailGroup {

	SHSqlDb* database = [sqlDbController database];
	if(!database) {
		[NSException raise: SHGenericException format: @"Database handler is nil"];
	}
	
	[in_undoDetailGroup deleteFromDatabase: database];
	
	NSUndoManager* undoManager = [groupTableView undoManager];	
	[undoManager registerUndoWithTarget:self selector:@selector(undoDelete:) object: in_undoDetailGroup];	

	[[NSApp delegate] selectGroupRow: [in_undoDetailGroup groupRow] recordRow: [in_undoDetailGroup recordRow]];	
}

- (void)undoDelete:(SHUndoDetailGroup*)in_undoDetailGroup {

	SHSqlDb* database = [sqlDbController database];
	if(!database) {
		[NSException raise: SHGenericException format: @"Database handler is nil"];
	}
	
	[in_undoDetailGroup insertIntoDatabase: database];
	
	NSUndoManager* undoManager = [groupTableView undoManager];	
	[undoManager registerUndoWithTarget:self selector:@selector(undoAdd:) object: in_undoDetailGroup];	

	[[NSApp delegate] selectGroupRow: [in_undoDetailGroup groupRow] recordRow: [in_undoDetailGroup recordRow]];	
}

@end
