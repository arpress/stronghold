/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision: 71 $
 *
 *	$Author: jcarro $
 *
 *	$Date: 2008-03-30 03:24:17 +0200 (Sun, 30 Mar 2008) $
 *
 ****************************************************************************/

#import <Cocoa/Cocoa.h>
#import <ExceptionHandling/NSExceptionHandler.h>

#import "typedef.h"

@interface SHExceptionController : NSObject {
	
	IBOutlet NSWindow* exceptionWindow;
	
	IBOutlet NSTextView* problemDescTextView;
	
	IBOutlet NSTextView* exceptionDescTextView;
}

- (id)init;

- (void)dealloc;

- (BOOL)shouldDisplayException:(NSException*)in_exception;

- (BOOL)exceptionHandler:(NSExceptionHandler*)in_sender shouldLogException:(NSException*)in_exception mask:(ulong32)in_mask;

- (BOOL)exceptionHandler:(NSExceptionHandler*)in_sender shouldHandleException:(NSException*)in_exception mask:(ulong32)in_mask;

- (void)initPanelFromBundle;

- (void)displayExceptionPanelWithException:(NSException*)in_exception;

- (IBAction)emailException:(id)in_sender;

- (IBAction)endPanel:(id)in_sender;

@end
