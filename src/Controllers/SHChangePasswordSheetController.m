/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#import "SHChangePasswordSheetController.h"
#import "SHAppController.h"
#import "NSWindow+SHGuiEffects.h"

@implementation SHChangePasswordSheetController

- (void)controlTextDidChange:(NSNotification*)in_notification {
	
	NSSecureTextField* inputField;

	inputField = [in_notification object];
	
	if( inputField == oldPasswordField ) {

		NSString* password = [oldPasswordField stringValue];
		
		if ([password length] > 7) {
			[newPasswordField setEnabled: YES];
		} else {
			[newPasswordField setEnabled: NO];
			[newPasswordField setStringValue: @""];

			[verifyField setEnabled: NO];
			[verifyField setStringValue: @""];
		}
	}
	
	if( inputField == newPasswordField ) {
	
		NSString* password = [newPasswordField stringValue];
		
		if ([password length] > 7) {
			[verifyField setEnabled: YES];
		} else {
			[verifyField setStringValue: @""];
			[verifyField setEnabled: NO];
		}
	
	}	

	if( inputField == verifyField ) {
	
		NSString* password = [newPasswordField stringValue];
		NSString* verifyPassword = [verifyField stringValue];
		
		if ([password isEqual: verifyPassword]) {
			[changePasswordButton setEnabled: YES];
		} else {
			[changePasswordButton setEnabled: NO];
		}
	}

}

- (IBAction)endSheet:(id)in_sender {
		
	if (in_sender == changePasswordButton) {

		//	Set the password to the redord detail field controller.
		NSString* oldPasswordString;
		NSMutableData* oldPasswordHash;

		oldPasswordString = [oldPasswordField stringValue];	
		oldPasswordHash = [sqlDbController computeHashFromString: oldPasswordString];
		
		BOOL isValidPassword = [sqlDbController isValidPasswordHash: oldPasswordHash];
		
		//
		//	If the password is not valid exit without closing the sheet.
		//
		if( !isValidPassword ) {
			[oldPasswordField setStringValue: @""];
			[newPasswordField setStringValue: @""];
			[verifyField setStringValue: @""];
			
			[sheet makeFirstResponder: oldPasswordField];
			
			[sheet shake];
			
			return;
		}

		NSString* newPasswordString;
		NSMutableData* newPasswordHash;

		newPasswordString = [newPasswordField stringValue];
		newPasswordHash = [sqlDbController computeHashFromString: newPasswordString];
				
		[progressIndicator setMinValue: 0.0];
		[progressIndicator setDoubleValue: 0.0];
		[progressIndicator setDoubleValue: 0.0];

		[progressLabel setHidden: NO];		
		[progressIndicator setHidden: NO];		
		[progressLabel displayIfNeeded];
		[progressIndicator displayIfNeeded];
		
		[sqlDbController encryptRecordsUsingPasswordHash: oldPasswordHash withPasswordHash: newPasswordHash 
			progressIndicator: progressIndicator];
			
		[sqlDbController updatePasswordHash: newPasswordHash];

		//	Return the code to the NSApp.
		[NSApp endSheet: sheet returnCode: 1];

		[progressLabel setHidden: YES];		
		[progressIndicator setHidden: YES];

	} else {

		//	Return the code to the NSApp.
		[NSApp endSheet: sheet returnCode: 0];
	}
	
	// Clear the secure text fields.
	[newPasswordField setEnabled: NO];	
	[verifyField setEnabled: NO];

	[oldPasswordField setStringValue: @""];
	[newPasswordField setStringValue: @""];
	[verifyField setStringValue: @""];

	// Hide the password sheet.
	[sheet orderOut: in_sender];
}

@end
