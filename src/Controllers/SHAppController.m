/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#import "SHAppController.h"
#import "SHRecordTableController.h"

#import "SHRecordTableController.h"

@implementation SHAppController

////////////////////// INITIALISATION //////////////////////////

- init {
	[super init];
	
	return(self);
}

- (void) dealloc {
	
	[prefController dealloc];
	
	[super dealloc];
}

////////////////////// ACESSORS //////////////////////////


////////////////////// APPLICATION MANAGEMENT //////////////////////////

- (void)awakeFromNib {

	[self initDefaultPreferences];
	
	[sqlDbController openDatabase];
	
	[sqlDbController initTables];
	
	[groupTableController loadFromSqlDb];
	
	// Makes the bottom border round and textured.
	[mainWindow setAutorecalculatesContentBorderThickness:YES forEdge:NSMinYEdge];
	[mainWindow setContentBorderThickness:26.0 forEdge:NSMinYEdge];	
}

- (void)initDefaultPreferences {

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    NSDictionary *appDefaults = [NSDictionary
        dictionaryWithObject:@"YES" forKey:@"waningBeforeDelete"];

    [defaults registerDefaults:appDefaults];
}

- (void)applicationDidFinishLaunching: (NSNotification*) in_notification {

	// If a password is not loaded then we have to request the user for it.
	if (![sqlDbController checkPasswordExists]) {
	
		[NSApp beginSheet: initialPasswordSheet modalForWindow: mainWindow
			modalDelegate: self
			didEndSelector: @selector(passwordSheetDidEnd:returnCode:contextInfo:)
			contextInfo: NULL];
	
	}

}

- (void)applicationWillTerminate:(NSNotification*) in_notification {
	
	[recordTableController saveRecordFields];
	
	[sqlDbController closeDatabase];
}

- (BOOL)applicationShouldHandleReopen:(NSApplication*)in_application hasVisibleWindows:(BOOL)in_hasVisibleWindows {
	
	if(!in_hasVisibleWindows) {
		
		[mainWindow orderFront: self];
	}
	
	return(NO);
}

////////////////////// SELECTION MANAGEMENT //////////////////////////

- (void)selectGroupRow:(ulong32)in_groupRow recordRow:(ulong32)in_recordRow {
	
	[groupTableController selectRowIndex: in_groupRow];

	[recordTableController selectRowIndex: in_recordRow];
}

- (ulong32) selectedGroupRow {
	ulong32 selectedRow = [groupTableController selectedRow];
	
	return(selectedRow);
}

- (ulong32) selectedRecordRow {
	ulong32 selectedRow = [recordTableController selectedRow];
	
	return(selectedRow);
}

////////////////////// mainVSplit MANAGEMENT //////////////////////////

- (void)splitView:(RBSplitView*)in_sender wasResizedFrom:(float)in_fromDimension to:(float)in_toDimension {

	[fieldSubview changeDimensionBy: (in_toDimension - in_fromDimension) mayCollapse: NO move: YES];

}

// Manage the modification of side of the panels to keep the "add" buttons in place with regard to the subviews of the
// main split view.
- (void) splitView: (RBSplitView*) in_sender changedFrameOfSubview: (RBSplitSubview*) in_subview 
	from: (NSRect) in_fromRect to: (NSRect) in_toRect {
		
	id buttonBox = nil;
			
	if (in_subview == recordSubview) {
		buttonBox = recordAddButton;
	}

	if (in_subview == fieldSubview) {
		buttonBox = fieldButtonBox;
	}
	
	if(buttonBox) {
		// Move the button to align with the second (or third) subview.
		NSRect prevButtonRect = [buttonBox frame];
		NSRect newButtonRect = prevButtonRect;
			
		NSView* view = [buttonBox superview];
		newButtonRect.origin.x = [in_sender convertPoint: in_toRect.origin toView: view].x;
		newButtonRect.origin.x += 4;
			
		[buttonBox setFrame: newButtonRect];
				
		// We ask the button's superview to redisplay just the smallest rect that covers both the old and the new position.
		[view setNeedsDisplayInRect:NSUnionRect(prevButtonRect,newButtonRect)];
		
	}
}

- (void) splitView: (RBSplitView*) in_sender didCollapse: (RBSplitSubview*) in_subview {
	
	if( in_subview == groupSubview ) {
		
		[groupAddButton setHidden: YES];
	}
}

- (void) splitView: (RBSplitView*) in_sender didExpand: (RBSplitSubview*) in_subview {
	
	if( in_subview == groupSubview ) {
		
		[groupAddButton setHidden: NO];
	}
}

- (IBAction) expandCollapseGroupSubview: (id) in_sender {

	BOOL isGroupSubviewCollapsed = [groupSubview isCollapsed];

	[groupSubview setCanCollapse: YES]; 
	
	if( isGroupSubviewCollapsed ) {

		[groupSubview expandWithAnimation];
	} else {

		[groupSubview collapseWithAnimation];

	}

	[groupSubview setCanCollapse: YES]; 
}

////////////////// UI PASSWORD MANAGEMENT /////////////////////

- (IBAction) toggleLock: in_sender {
		
	if ([sqlDbController isLocked]) {

		[NSApp beginSheet: passwordSheet modalForWindow: mainWindow
			modalDelegate: self	
			didEndSelector: @selector(passwordSheetDidEnd:returnCode:contextInfo:)
			contextInfo: NULL];
			
	} else {
		
		// Save ther record fields before locking the application
		[recordTableController saveRecordFields];		
		[sqlDbController updatePasswordHash: nil];
	}
	
	[self updateToggleLock];
}

- (void) updateToggleLock {

	//	
	//	Update the menu, the dock menu and the icon, to
	//	be coherent with the lock status of the application.
	//
	if( [sqlDbController isLocked] ) {

		[lockToggleButton setState: NSOffState]; 
		
		[lockMenuItem setTitle: @"Locked"];
		[lockMenuItem setImage: [NSImage imageNamed: @"NSLockLockedTemplate"]];

		[dockLockMenuItem setTitle: @"Locked"];
		[dockLockMenuItem setImage: [NSImage imageNamed: @"NSLockLockedTemplate"]];
		
		[fieldEditButton setState: NSOffState];
		
		[groupAddButton setEnabled: NO];
		
		[recordTableController updateAddRecordButton];
		
	} else {

		[lockToggleButton setState: NSOnState]; 

		[lockMenuItem setTitle: @"Unlocked"];
		[lockMenuItem setImage: [NSImage imageNamed: @"NSLockUnlockedTemplate"]];

		[dockLockMenuItem setTitle: @"Unlocked"];
		[dockLockMenuItem setImage: [NSImage imageNamed: @"NSLockUnlockedTemplate"]];
		
		[groupAddButton setEnabled: YES];

		[recordTableController updateAddRecordButton];
	}		
	
	// Update the record fields view
	[recordTableController updateRecordFields];
}

- (IBAction)showPreferences:(id)in_sender {

    if (!prefController) {
        // Determine path to the sample preference panes
//        NSString *pathToPanes = [[NSString stringWithFormat:@"%@/../Preference Panes/", [[NSBundle mainBundle] bundlePath]]
//            stringByStandardizingPath];
        
        prefController = [[SS_PrefsController alloc] init];
        
        // Set which panes are included, and their order.
		// TODO activate for debuggin configuration
//		[prefController setDebug:YES];
        [prefController setPanesOrder:[NSArray arrayWithObjects:@"General", @"Updating", nil]];
    }
    
    // Show the preferences window.
    [prefController showPreferencesWindow];
}

/////////////////// CHANGE PASSWORD ///////////////////////

- (IBAction)showChangePasswordSheet: (id) in_sender {
		
	[NSApp beginSheet: changePasswordSheet modalForWindow: mainWindow
		modalDelegate: self
		didEndSelector: @selector(passwordSheetDidEnd:returnCode:contextInfo:)
		contextInfo: NULL];
}

- (void)passwordSheetDidEnd:(NSWindow*)in_sheet returnCode:(int)in_returnCode contextInfo:(void*)in_contextInfo {

	[self updateToggleLock];

}

- (IBAction)sendFeedback:(id)in_sender {
		
	[[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString: [NSString stringWithFormat: @"mailto:%@?SUBJECT=%@", 
		@"jcarro@users.sourceforge.net", @"Stronghold%20Feedback"] ] ];
}

- (IBAction) showAboutPanel: (id) in_sender {
	
    if ( !aboutWindow ) {
	
		BOOL nibDidLoad = [NSBundle loadNibNamed:@"AboutPanel" owner:self];
		if( !nibDidLoad ) {
			
			[NSException raise: SHGenericException format: @"AboutPanel nib failed to load"];
			
		}
		
	}
    
	[aboutWindow center];
    [aboutWindow makeKeyAndOrderFront: nil];
}

@end
