/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/
 
#import <Cocoa/Cocoa.h>
#import "SHSqlDbController.h"
#import "SHRecordTableController.h"
#import "SHRecordDetailGroupController.h"

@interface SHGroupTableController : NSObject {
	
	/*!
		@var		
		
		@abstract	
	*/
	NSMutableArray* groupNameArray;
	
	// Outlets to GUI objects.
	
	/*!
		@var		
		
		@abstract	
	*/
	IBOutlet NSTableView* groupTableView;
	
	/*!
		@var		
		
		@abstract	
	*/
	IBOutlet SHSqlDbController* sqlDbController;
		
	/*!
		@var		
		
		@abstract	
	*/
	IBOutlet SHRecordTableController* recordTableController;

	/*!
		@var		
		
		@abstract	
	*/
	IBOutlet SHRecordDetailGroupController* recordDetailGroupController;
}

- init;

- (void)dealloc;

- (NSMutableArray*)groupNameArray;

- (void)loadFromSqlDb;

// Methods to operate as the data source of the groupTableView in the GUI.

- (NSInteger)numberOfRowsInTableView:(NSTableView*)in_tableView;

- (id)tableView:(NSTableView*)in_tableView objectValueForTableColumn:(NSTableColumn*)in_column row:(NSInteger)in_row;

- (void)tableView:(NSTableView*)in_tableView setObjectValue:(id)anObject forTableColumn:(NSTableColumn*)in_column row:(NSInteger)in_row;

// Methods corresponding to the delegate of the NSTableView.

- (void)tableViewSelectionDidChange:(NSNotification*)in_notification;

/*!
    @method     selectRowIndex:

    @abstract   Selects the row <i>in_rowIndex</i> in the tableView associated to the controller.

*/
- (void)selectRowIndex:(ulong32)in_rowIndex;

/*!
    @method     selectedRow:

    @abstract   Returns the current selected row in the tableView associated to the controller.

*/
- (ulong32)selectedRow;


// Actions to be used by the GUI elemments.

- (IBAction)add:(id)in_sender;

- (IBAction)delete:(id)in_sender;

- (void)deleteSheetDidEnd:(NSWindow*)in_sheet returnCode:(int)in_returnCode contextInfo:(void*)in_contextInfo;

- (void)deleteGroup;

- (void)undoAdd:(SHUndoGroupRecord*)in_undoRecord;

- (void)undoDelete:(SHUndoGroupRecord*)in_undoRecord;

- (void)undoEdit:(SHUndoGroupRecord*)in_undoRecord;

@end
