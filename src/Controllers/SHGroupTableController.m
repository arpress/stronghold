/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#import "SHGroupTableController.h"
#import "SHGroupName.h"
#import "SHAppController.h"


@implementation SHGroupTableController

- init {
	[super init];
	
	groupNameArray = [[NSMutableArray alloc] init];	
		
	return (self);
}

- (void)dealloc {

	[groupNameArray release];

	[super dealloc];
}

- (NSMutableArray*)groupNameArray {

	return(groupNameArray);

}

- (void)loadFromSqlDb {

	SHSqlDb* database = [sqlDbController database];
	if( !database ) {
		[NSException raise: SHGenericException format: @"Database handler is nil"];
	}
	
	NSString *queryString;
	queryString = [NSString stringWithFormat: @"SELECT identifier,name FROM groupTable"];
	
	SHSqlQuery* query;
	query = [database query: queryString];
	
	SHGroupName* groupName;
	[groupNameArray removeAllObjects];
		
	// Load all the records in the database
	BOOL foundRecord;
	foundRecord = [query stepAndFinalize];
	
	while(foundRecord) {
		groupName = [[SHGroupName alloc] init];
		
		[groupName loadFromQuery: query];
		
		[groupNameArray addObject: groupName];
		
		[groupName release];
		
		foundRecord = [query stepAndFinalize];
	}
		
	// Tell the table view that it should reload all the data because it has changed.
	[groupTableView reloadData];
}

// Methods to operate as the data source of the groupTable n the GUI.

- (NSInteger)numberOfRowsInTableView:(NSTableView*)in_tableView {
	
	return([groupNameArray count] + 2);

}

- (id)tableView:(NSTableView*)in_tableView objectValueForTableColumn:(NSTableColumn*)in_column row:(NSInteger)in_row {

	NSString* value;
		
	switch( in_row ) {

		case 0: {

			value = @"All";

		} break;
		
		case 1: {

			value = @"Unfiled";

		} break;
		
		default: {

			SHGroupName* groupName;
			groupName = [groupNameArray objectAtIndex: (in_row - 2)];
			value = [groupName valueForKey: [in_column identifier]];

		} break;
	}

	return(value);
}

- (void)tableView:(NSTableView*)in_tableView setObjectValue:(id)in_value forTableColumn:(NSTableColumn*)in_column row:(NSInteger)in_row {

	if( in_row > 1) {
		
		SHGroupName* groupName = [groupNameArray objectAtIndex: in_row - 2];

		id currentValue = [groupName valueForKey: [in_column identifier]];
		
		if( ![currentValue isEqual: in_value] ) {
			
			SHSqlDb* database = [sqlDbController database];
			if( !database ) {
				[NSException raise: SHGenericException format: @"Database handler is nil"];
			}	
			
			ulong32 recordRow	= [[NSApp delegate] selectedRecordRow];

			SHUndoGroupRecord* undoRecord = [[SHUndoGroupRecord alloc] initWithIdentifier: [groupName identifier]
				recordType: SH_UNDO_GROUP_ITEM groupRow: in_row recordRow:recordRow database: database];
			
			[groupName setValue: in_value forKey: [in_column identifier]];
			[groupName storeIn: database];

			// Update the table that displays the groups selected for the 
			// current record.
			[recordDetailGroupController updateGroupTableView];
			
			NSUndoManager* undoManager = [groupTableView undoManager];
		
			[undoManager registerUndoWithTarget:self selector:@selector(undoEdit:) object: undoRecord];
			[undoManager setActionName: @"Edit Group"];
		
			[undoRecord release];
		}
		
	} else {	
		NSBeep();		
	}
}

// Methods corresponding to the delegate of the NSTableView.

- (void)tableViewSelectionDidChange:(NSNotification*)in_notification {
	
	NSTableView* notificationTableView = [in_notification object];
	NSInteger selectedRow = [notificationTableView selectedRow];
	
	NSNumber* groupId;
	
	// We have not selected any entry
	switch (selectedRow) {
		
		// No row is selected
		case -1: {
			groupId = [[NSNumber alloc] initWithLong: 0];			
			[groupId autorelease];
		} break;

		// We have selected the ALL entry
		case 0: {
			groupId = [[NSNumber alloc] initWithLong: -2];			
			[groupId autorelease];
		} break;

		// We have selected the UNGROUPED entry.
		case 1: {
			groupId = [[NSNumber alloc] initWithLong: -1];			
			[groupId autorelease];
		} break;
			
		default: {
			SHGroupName* groupName;
			groupName = [groupNameArray objectAtIndex: (selectedRow - 2)];
			groupId = [groupName identifier];
		} break;
		
	}
	
	// Reload the recordTableController so the recordTableView is updated.
	[recordTableController loadFromSqlDbInGroup: groupId];	
}

- (void)selectRowIndex:(ulong32)in_rowIndex {
	[groupTableView deselectAll: self];
	[groupTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:in_rowIndex] byExtendingSelection:NO];
}

- (ulong32)selectedRow {
	ulong32 selectedRow = [groupTableView selectedRow];
	
	return(selectedRow);
}

// Methods for the GUI management.

- (IBAction)add:(id)in_sender {

	SHSqlDb* database = [sqlDbController database];
	if( !database ) {
		[NSException raise: SHGenericException format: @"Database handler is nil"];	
	}

	SHGroupName* newGroupName = [[SHGroupName alloc] initWithName: @"No name"];
	
	[newGroupName storeIn: database];
	
	[groupNameArray addObject: newGroupName];
					
	[groupTableView reloadData];
	
	// Select the last inserted row
	NSInteger lastRowIndex = [groupTableView numberOfRows] - 1;

	ulong32 recordRow	= [[NSApp delegate] selectedRecordRow];
	SHUndoGroupRecord* undoRecord = [[SHUndoGroupRecord alloc] initWithIdentifier: [newGroupName identifier]
		recordType: SH_UNDO_GROUP_ITEM groupRow: lastRowIndex recordRow:recordRow database: database];
	
	NSUndoManager* undoManager = [groupTableView undoManager];
	[undoManager registerUndoWithTarget:self selector:@selector(undoAdd:) object: undoRecord];	
	[undoManager setActionName: @"Add Group"];
	
	[undoRecord release];
	[newGroupName release];
	
	[groupTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:lastRowIndex] byExtendingSelection:NO];
	[groupTableView editColumn:0 row:lastRowIndex withEvent:nil select:YES];
}

- (IBAction)delete:(id)in_sender {
	
	NSInteger selectedRow = [groupTableView selectedRow];
	
	// If we have not selected any group or we have selected the "All" or "Ungrouped"
	// entries, send a beep because those entries can not be deleted.
	// Else we delete the group.
	if (selectedRow > 1 && [sqlDbController isUnlocked]) {

		// Request the user confirmation to delete the record if the
		// preferences are configured to do so.
		NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];		
		if([userDefaults boolForKey: @"waningBeforeDelete"]) {

			SHGroupName* groupName = [groupNameArray objectAtIndex: (selectedRow - 2)];

			NSString* alertMessage = [NSString stringWithFormat: @"Are you sure you want to delete the group \"%@\"?",
										[groupName name]];
									
			
			NSAlert* alertPanel = [NSAlert alertWithMessageText: alertMessage
									defaultButton: @"Delete" alternateButton: @"Cancel" otherButton: nil
									informativeTextWithFormat: @""];

			[alertPanel setShowsHelp: YES];

			[alertPanel beginSheetModalForWindow: [NSApp mainWindow] modalDelegate:self
				didEndSelector: @selector(deleteSheetDidEnd:returnCode:contextInfo:) contextInfo: nil];			
				
		} else {
			[self deleteGroup];
		}

	} else {
	
		NSBeep();
		
	}
}

- (void)deleteSheetDidEnd:(NSWindow*)in_sheet returnCode:(int)in_returnCode contextInfo:(void*)in_contextInfo {
	
	if(in_returnCode == NSAlertDefaultReturn) {
		[self deleteGroup];
	}
}

- (void)deleteGroup {

	NSInteger selectedRow = [groupTableView selectedRow];

	SHGroupName* groupName = [groupNameArray objectAtIndex: (selectedRow - 2)];

	SHSqlDb* database = [sqlDbController database];
	if( !database ) {
		[NSException raise: SHGenericException format: @"Database handler is nil"];
	}

	//
	//	Get the current values of the record for a future undo
	//
	ulong32 recordRow	= [[NSApp delegate] selectedRecordRow];
	SHUndoGroupRecord* undoRecord = [[SHUndoGroupRecord alloc] initWithIdentifier: [groupName identifier]
		recordType: SH_UNDO_GROUP_ITEM groupRow: selectedRow recordRow:recordRow database: database];

	NSUndoManager* undoManager = [groupTableView undoManager];
	[undoManager registerUndoWithTarget:self selector:@selector(undoDelete:) object: undoRecord];
	[undoManager setActionName: @"Delete Group"];

	[groupName removeFrom: database];
	
	[groupNameArray removeObjectAtIndex: (selectedRow - 2)];
	
	// Get the remaining records in the list.
	ulong32 numRecords = [groupNameArray count] + 2;

	if( selectedRow >= numRecords ) {
	
		[groupTableView selectRowIndexes: [NSIndexSet indexSetWithIndex: (numRecords-1)]
			byExtendingSelection: NO];
				
	} else {
		
		groupName = [groupNameArray objectAtIndex: (selectedRow - 2)];
		
		NSNumber* groupId = [groupName identifier];		
		[recordTableController loadFromSqlDbInGroup: groupId];	
		
	}
	
	[groupTableView reloadData];

	// Update the recordDetailGroupTable in case it is displayed, so it is inline
	// with the current list of groups.
	[recordDetailGroupController updateGroupTableView];
}

- (void)undoAdd:(SHUndoGroupRecord*)in_undoRecord {
	SHSqlDb* database = [sqlDbController database];

	if( !database ) {
		[NSException raise: SHGenericException format: @"Database handler is nil"];	
	}

	[in_undoRecord deleteFromDatabase: database];

	NSUndoManager* undoManager = [groupTableView undoManager];
	
	[undoManager registerUndoWithTarget:self selector:@selector(undoDelete:) object: in_undoRecord];
	
	[self loadFromSqlDb];

	[[NSApp delegate] selectGroupRow: [in_undoRecord groupRow] recordRow: [in_undoRecord recordRow]];
}

- (void)undoDelete:(SHUndoGroupRecord*)in_undoRecord {
	SHSqlDb* database = [sqlDbController database];

	if( !database ) {
		[NSException raise: SHGenericException format: @"Database handler is nil"];	
	}

	[in_undoRecord insertIntoDatabase: database];

	NSUndoManager* undoManager = [groupTableView undoManager];
	
	[undoManager registerUndoWithTarget:self selector:@selector(undoAdd:) object: in_undoRecord];

	[self loadFromSqlDb];

	[[NSApp delegate] selectGroupRow: [in_undoRecord groupRow] recordRow: [in_undoRecord recordRow]];
	
}

- (void)undoEdit:(SHUndoGroupRecord*)in_undoRecord {
	SHSqlDb* database = [sqlDbController database];

	if( !database ) {
		[NSException raise: SHGenericException format: @"Database handler is nil"];	
	}

	SHUndoGroupRecord* undoRecord = [[SHUndoGroupRecord alloc] initWithIdentifier: [in_undoRecord identifier]
		recordType: SH_UNDO_GROUP_ITEM groupRow: [in_undoRecord groupRow] recordRow: [in_undoRecord recordRow]
		database: database];

	[in_undoRecord insertIntoDatabase: database];

	NSUndoManager* undoManager = [groupTableView undoManager];
	
	[undoManager registerUndoWithTarget:self selector:@selector(undoEdit:) object: undoRecord];	
	[undoRecord release];

	[self loadFromSqlDb];

	[[NSApp delegate] selectGroupRow: [in_undoRecord groupRow] recordRow: [in_undoRecord recordRow]];
}

@end
