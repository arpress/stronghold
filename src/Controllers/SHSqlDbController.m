/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/

#import "SHSqlDbController.h"
#import "SHRecord.h"
#import "SHSqlDb.h"
#import "SHCypher.h"

@implementation SHSqlDbController

- init {
	[super init];
	
	cypher = [[SHCypher alloc] init];
	
	passwordHash = [[NSMutableData alloc] init];	

	database = [[SHSqlDb alloc] init];
	
	isLocked = YES;
	
	return(self);
}

- (void)release {

	[database close];
	[database release];

	[cypher release];
	
	[passwordHash release];

	[super dealloc];
}

- (SHSqlDb*)database {
	return(database);
}

- (void)openDatabase {
	BOOL result;

	// Get the path of the application support directory for the current user.
	NSString* path = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0];	
	path = [path stringByAppendingPathComponent:[[NSBundle mainBundle]  objectForInfoDictionaryKey:@"CFBundleName"]];

	// Check if the application directory exists in the application support directory.
	NSFileManager* fileManager = [NSFileManager defaultManager];
	
	result = [fileManager fileExistsAtPath: path];
	if( !result ) {

		result = [fileManager createDirectoryAtPath: path attributes: nil];
		if( !result ) {
			[NSException raise: SHGenericException format: @"Application Support directory could not be created"];
		}

	}

	// Add the database name to the path and open the database.
	path = [path stringByAppendingPathComponent:@"stronghold.db3"];	
	[database open: path];
}

- (void) initTables {

	[database initRecordTable];
	[database initGroupTable];
	[database initGroupRecordCrossTable];
	[database initAuxTable];
}

- (void)closeDatabase {
	[database close];
}

- (BOOL)isLocked {
	return(isLocked);
}

- (BOOL)isUnlocked {
	return(!isLocked);
}
 
- (NSData*)passwordHash {
	return((NSData*) passwordHash);
}

- (BOOL)checkPasswordExists {
	SHSqlQuery* sqlQuery;
	
	sqlQuery = [database query: @"SELECT DISTINCT (name) FROM auxTable WHERE (name == 'key') LIMIT 1"];
	
	bool result = NO;
	
	while ([sqlQuery stepAndFinalize]) {
		result = YES;
	}
	
	return (result);
}

- (NSMutableData*)computeHashFromString:(NSString*)in_passwordString {
	NSMutableData* hashValue;
	hashValue = [[NSMutableData alloc] init];

	SHSha1* passwordSha1 = [[SHSha1 alloc] init];		
	[passwordSha1 hashWithData: [in_passwordString dataUsingEncoding: NSUTF8StringEncoding]];		
	[passwordSha1 endWithHashValue: hashValue];		
	[passwordSha1 release];
	
	[hashValue autorelease];
	
	return(hashValue);
}

- (BOOL)isValidPasswordHash:(NSData*)in_passwordHash {
		
	SHSqlQuery* query;
	query = [database query: @"SELECT contents,salt,initVector,authCode FROM auxTable \
		WHERE (name=='key')"];
	
	bool foundRecord;
	foundRecord = [query stepAndFinalize];
	
	if(!foundRecord) {
		[NSException raise: SHGenericException format: @"Hash key record not found"];
	}
		
	NSMutableData *passwordData = [[NSMutableData alloc] init]; 
	[passwordData setData: [query columnAsBlob: 0]];
	
	NSData *salt		= [query columnAsBlob: 1];
	NSData *initVector	=	[query columnAsBlob: 2];
	NSData *authCode	= [query columnAsBlob: 3];
	
	bool decryptResult = [cypher decryptData: passwordData withPassword: in_passwordHash 
		withSalt: salt withInitVector: initVector withAuthCode: authCode];
	
	if (decryptResult) {
		
		NSRange passwordRange;
		passwordRange.location = 2028;
		passwordRange.length = 20;
		
		if (![in_passwordHash isEqual: [passwordData subdataWithRange: passwordRange]]) {
			decryptResult = NO;
		}
	}
	
	[query finish];
	
	[passwordData release];
	
	return(decryptResult);
}

- (void)updatePasswordHash:(NSData*)in_passwordHash {
	
	// If we have received a password, the compute the hash
	// and store it
	if( in_passwordHash ) {
	
		[passwordHash setData: in_passwordHash];

		isLocked = NO;

	} else {

		NSRange passwordHashRange;
		passwordHashRange.location = 0;
		passwordHashRange.length = [passwordHash length];
		[passwordHash resetBytesInRange: passwordHashRange];
		
		isLocked = YES;
	}
}

- (void)storePasswordHash:(NSData*)in_passwordHash {
	
	//
	//	Get 2048 - 20 for the password hash.
	//
	SHRandomPool* randomPool = [[SHRandomPool alloc] init];
	NSMutableData* randomData = [[NSMutableData alloc] init];
	
	[randomPool randomData: randomData length: 2028];	
	[randomData appendData: in_passwordHash];
	
	[randomPool release];
	
	//
	// Encrypt the data
	//
	NSMutableData *salt = [[NSMutableData alloc] init];
	NSMutableData *initVector = [[NSMutableData alloc] init];
	NSMutableData *authCode = [[NSMutableData alloc] init];
	[cypher encryptData: randomData withPassword: in_passwordHash salt: salt
	initVector: initVector authCode: authCode];
		
	// If the identifier is not 0 we update the record in the database,
	// else we insert the new record.
	SHSqlQuery *sqlQuery;
	
	sqlQuery = [database query: @"INSERT OR REPLACE INTO auxTable (name, contents, salt, initVector, authCode) \
		VALUES (?1, ?2, ?3, ?4, ?5)"];
		
	[sqlQuery bindIndex: 1 withString: @"key"];
	[sqlQuery bindIndex: 2 withBlob: randomData];
	[sqlQuery bindIndex: 3 withBlob: salt];
	[sqlQuery bindIndex: 4 withBlob: initVector];
	[sqlQuery bindIndex: 5 withBlob: authCode];
	
	// Execute the query.
	[sqlQuery execute];	
	
	//
	//	Release all the objects.
	//
	[randomData release];
	[salt release];
	[initVector release];
	[authCode release];	
}

- (void)encryptRecordsUsingPasswordHash:(NSData*)in_oldPasswordHash withPasswordHash:(NSData*)in_newPasswordHash 
	progressIndicator:(NSProgressIndicator*)in_progressIndicator {
	
	[database createTempRecordTable];
	
	NSString *queryString;
	SHSqlQuery* query;
	
	//
	//	Get the number of rows in the record table in order to update the progress indicator.
	//
	queryString = [NSString stringWithFormat: @"SELECT count(*) FROM recordTable"];

	query = [database query: queryString];

	BOOL foundRecord = [query stepAndFinalize];
	
	if( !foundRecord ) {
		[NSException raise: SHGenericException format: @"Failed to obtain the number of records in the record table"];
	}
			
	NSNumber* numRecords = [query columnAsUnsignedLong: 0];
	
	[in_progressIndicator setMaxValue: [numRecords doubleValue]];
	[in_progressIndicator setMinValue: 0.0];
	[in_progressIndicator setDoubleValue: 0.0];
	
	[query finish];

	//
	//	Iterate throght the records to reencrypt them.
	//
	queryString = [NSString stringWithFormat: @"SELECT identifier, name, contents, salt, initVector, authCode FROM recordTable"];
	
	query = [database query: queryString];
	
	foundRecord = [query stepAndFinalize];
	
	SHRecord* record = [[SHRecord alloc] init];
	
	double numRecordsProcessed = 0.0;
	
	while( foundRecord ) {
	
		[record loadFromQuery: query withPasswordHash: in_oldPasswordHash];
		
		[record storeIn: database table: @"tempRecordTable" withPasswordHash: in_newPasswordHash];
		
		foundRecord = [query stepAndFinalize];
		
		numRecordsProcessed += 1.0;
		
		[in_progressIndicator setDoubleValue: numRecordsProcessed];
		[in_progressIndicator displayIfNeeded];
	}
	
	[record release];

	[self swapRecordTables];
	
	[self storePasswordHash: in_newPasswordHash];
}

- (void)swapRecordTables {

	SHSqlQuery* query;
	query = [database query: @"ALTER TABLE recordTable RENAME TO saveRecordTable"];
	
	[query execute];
		
	query = [database query: @"DROP TABLE IF EXISTS recordTable"];
	
	[query execute];

	query = [database query: @"ALTER TABLE tempRecordTable RENAME TO recordTable"];
	
	[query execute];
	
	query = [database query: @"DROP TABLE IF EXISTS saveRecordTable"];
	
	[query execute];

	query = [database query: @"VACUUM"];
	
	[query execute];
}

@end
