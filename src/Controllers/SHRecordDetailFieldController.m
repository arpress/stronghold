/*****************************************************************************
 *                                                                           *
 *  This file is part of Stronghold                                          *
 *                                                                           *
 *  Stronghold is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Stronghold is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Stronghold.  If not, see <http://www.gnu.org/licenses/>.          *
 *                                                                           *
 *  Created by Javier Carro on 20/12/07.                                     *
 *  Copyright Javier Carro 2007, 2008.                                       *
 *                                                                           *
 *****************************************************************************/

/*******************		FILE IDENTIFICATION		 *************************
 *
 *	$Revision$
 *
 *	$Author$
 *
 *	$Date$
 *
 ****************************************************************************/
 
#import "SHRecordDetailFieldController.h"
#import "SHAppController.h"
#import "SHRecordField.h"
#import "typedef.h"

@implementation SHRecordDetailFieldController

- init {
	[super init];
	
	return (self);
}

- (void) dealloc {

	[fieldArray release];
	
	[super dealloc];
}

- (NSMutableArray*) fieldArray {

	return(fieldArray);

}

- (void) setFieldArray: (NSMutableArray*) in_fieldArray {

	[in_fieldArray retain];
	[fieldArray release];
	
	fieldArray = in_fieldArray;
	
	[fieldTableView reloadData];

}

// Methods to operate as the data source of the recordTableView in the GUI.
- (NSInteger) numberOfRowsInTableView: (NSTableView*) in_tableView {
	
	NSInteger numRows;
	
	numRows = [fieldArray count];
	
	return(numRows);

}

- (id) tableView: (NSTableView*) in_tableView objectValueForTableColumn: (NSTableColumn*) in_column row: (NSInteger) in_row {

	NSString* value;
	SHRecordField* recordField;
	
	recordField = [fieldArray objectAtIndex: in_row];
	value = [recordField valueForKey: [in_column identifier]];
	
	return(value);	

}

- (void) tableView: (NSTableView*) in_tableView setObjectValue: (id) in_value forTableColumn: (NSTableColumn*) in_column row: (NSInteger) in_row {

	SHRecordField* recordField = [fieldArray objectAtIndex: in_row];

	id currentValue = [recordField valueForKey: [in_column identifier]];
	
	if( ![currentValue isEqual: in_value] ) {
		
		SHRecordField* previousField = [[SHRecordField alloc] initWithRecordField: recordField];
		
		SHUndoDetailField* undoDetailField = [[SHUndoDetailField alloc] 
			initWithCurrentFieldIndex: in_row previousField: previousField 
			groupRow: [[NSApp delegate] selectedGroupRow] recordRow: [[NSApp delegate] selectedRecordRow]];
			
		[previousField release];

		NSUndoManager* undoManager = [fieldTableView undoManager];	
		[undoManager registerUndoWithTarget:self selector:@selector(undoEdit:) object: undoDetailField];
		[undoManager setActionName: @"Modify Record"];

		[undoDetailField release];

		[recordField setValue: in_value forKey: [in_column identifier]];	
	}
}

- (BOOL)tableView:(NSTableView*)in_tableView shouldEditTableColumn:(NSTableColumn*)in_column row:(NSInteger)in_row {
	
	if([editFieldsButton state] == NSOffState) {
		return(NO);
	} else {
		return(YES);
	}
}

// Actions to be used by the GUI elemments.

- (IBAction)toggleEdit:(id)in_sender {

	[fieldTableView abortEditing];
	
	if( [sqlDbController isUnlocked] && ([editFieldsButton state] == NSOnState) ) {
		[addFieldsButton setEnabled: YES];
	} else {
		[addFieldsButton setEnabled: NO];
	}
}

- (IBAction) add: (id) in_sender {

	SHRecordField* newRecordField;
	
	newRecordField = [[SHRecordField alloc] initWithName: @"Unnamed" Value: @"Empty field"];
	
	[fieldArray addObject: newRecordField];
	
	[fieldTableView reloadData];

	// Select the last inserted row
	NSInteger lastRowIndex = [fieldTableView numberOfRows] - 1;
	
	[fieldTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:lastRowIndex] byExtendingSelection:NO];
	[fieldTableView editColumn:0 row:lastRowIndex withEvent:nil select:YES];

	SHUndoDetailField* undoDetailField = [[SHUndoDetailField alloc]
		initWithCurrentFieldIndex: lastRowIndex previousField: nil 
		groupRow: [[NSApp delegate] selectedGroupRow] recordRow: [[NSApp delegate] selectedRecordRow]];
	
	NSUndoManager* undoManager = [fieldTableView undoManager];	
	[undoManager registerUndoWithTarget:self selector:@selector(undoAdd:) object: undoDetailField];
	[undoManager setActionName: @"Modify Record"];
	
	[undoDetailField release];
	[newRecordField release];
}

- (IBAction) delete: (id) in_sender {
	NSInteger selectedRow = [fieldTableView selectedRow];
	
	if( selectedRow >= 0 && [sqlDbController isUnlocked] && ([editFieldsButton state] == NSOnState) ) {		
		
		// Request the user confirmation to delete the record if the
		// preferences are configured to do so.
		NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];		
		if([userDefaults boolForKey: @"waningBeforeDelete"]) {
			SHRecordField* currentRecord = [fieldArray objectAtIndex: selectedRow];

			NSString* alertMessage = [NSString stringWithFormat: @"Are you sure you want to delete the field \"%@\"?",
										[currentRecord name]];										
				
			NSAlert* alertPanel = [NSAlert alertWithMessageText: alertMessage
									defaultButton: @"Delete" alternateButton: @"Cancel" otherButton: nil
									informativeTextWithFormat: @""];

			[alertPanel setShowsHelp: YES];

			[alertPanel beginSheetModalForWindow: [NSApp mainWindow] modalDelegate:self
				didEndSelector: @selector(deleteSheetDidEnd:returnCode:contextInfo:) contextInfo: nil];			
		} else {
			[self deleteField];
		}
	} else {
		NSBeep();
	}
}

- (void)deleteSheetDidEnd:(NSWindow*)in_sheet returnCode:(int)in_returnCode contextInfo:(void*)in_contextInfo {
	
	if(in_returnCode == NSAlertDefaultReturn) {
		[self deleteField];
	}
}

- (void)deleteField {
	NSInteger selectedRow = [fieldTableView selectedRow];

	SHRecordField* currentRecord = [fieldArray objectAtIndex: selectedRow];

	SHUndoDetailField* undoDetailField = [[SHUndoDetailField alloc] 
		initWithCurrentFieldIndex: selectedRow previousField: currentRecord 
		groupRow: [[NSApp delegate] selectedGroupRow] recordRow: [[NSApp delegate] selectedRecordRow]];

	NSUndoManager* undoManager = [fieldTableView undoManager];	
	[undoManager registerUndoWithTarget:self selector:@selector(undoDelete:) object: undoDetailField];
	[undoManager setActionName: @"Modify Record"];

	[undoDetailField release];

	[fieldArray removeObjectAtIndex: selectedRow];
	
	// Get the remaining records in the list.
	ulong32 numRecords = [fieldArray count];
			
	// If we have deleted the last record in the list then
	// select the new last record.
	// Else we keep the same selection index, but we have to reload the detailGroup
	// table and the detail table (if the password is loaded).
	if( numRecords && (selectedRow >= numRecords) ) {
					
		[fieldTableView selectRowIndexes: [NSIndexSet indexSetWithIndex: (numRecords-1)]
			byExtendingSelection: NO];
			
	}

	[fieldTableView reloadData];
}

- (void)undoAdd:(SHUndoDetailField*)in_undoDetailField {
	
	[[NSApp delegate] selectGroupRow: [in_undoDetailField groupRow] recordRow: [in_undoDetailField recordRow]];
	
	//
	//	Get the index of the current record
	//
	NSUInteger currentFieldIndex = [in_undoDetailField currentFieldIndex];
	SHRecordField* previousField = [fieldArray objectAtIndex: currentFieldIndex];
	[in_undoDetailField setPreviousField: previousField];
	
	[fieldArray removeObjectAtIndex: currentFieldIndex];

	[fieldTableView reloadData];
	[fieldTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:currentFieldIndex] byExtendingSelection:NO];
				
	NSUndoManager* undoManager = [fieldTableView undoManager];	
	[undoManager registerUndoWithTarget:self selector:@selector(undoDelete:) object: in_undoDetailField];	
}

- (void)undoDelete:(SHUndoDetailField*)in_undoDetailField {

	[[NSApp delegate] selectGroupRow: [in_undoDetailField groupRow] recordRow: [in_undoDetailField recordRow]];
	
	//
	//	Get the index of the current record
	//
	SHRecordField* previousField = [in_undoDetailField previousField];
	NSUInteger currentFieldIndex = [in_undoDetailField currentFieldIndex];
	[fieldArray insertObject:previousField atIndex: currentFieldIndex];

	[fieldTableView reloadData];
	[fieldTableView selectRowIndexes:[NSIndexSet indexSetWithIndex: currentFieldIndex] byExtendingSelection:NO];
	
	[in_undoDetailField setPreviousField: nil];
		
	NSUndoManager* undoManager = [fieldTableView undoManager];	
	[undoManager registerUndoWithTarget:self selector:@selector(undoAdd:) object: in_undoDetailField];
}

- (void)undoEdit:(SHUndoDetailField*)in_undoDetailField {

	[[NSApp delegate] selectGroupRow: [in_undoDetailField groupRow] recordRow: [in_undoDetailField recordRow]];
	
	//
	//	Get the index of the current record
	//
	NSUInteger currentFieldIndex = [in_undoDetailField currentFieldIndex];
	SHRecordField* currentField = [fieldArray objectAtIndex: currentFieldIndex];
	[currentField retain];
	
	[fieldArray replaceObjectAtIndex: currentFieldIndex withObject: [in_undoDetailField previousField]];
	[in_undoDetailField setPreviousField: currentField];
	[currentField release];
	
	[fieldTableView reloadData];
	[fieldTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:currentFieldIndex] byExtendingSelection:NO];
		
	NSUndoManager* undoManager = [fieldTableView undoManager];	
	[undoManager registerUndoWithTarget:self selector:@selector(undoEdit:) object: in_undoDetailField];
	
}
@end
