
Version 0.7.2 (2008/03/30):

Corrected a problem with the type of the field values column in the interface builder. The rich text option was checked and the value returned was not a NSString.

Corrected the change password functionality, the problem was that a [query finish] was missing so the table saveRecordTable raised an exception when trying to dropt the table.

Added some more exceptions that were missing in the SHSqlDbController class.


Version 0.7.1 (2008/03/27):

Corrected critical bug: The group list was not loaded from the sql database when the application was launched.


Version 0.7 (2008/03/26):

Added exceptions to handle programming errors.

Added exception report feedback by mail.

Added feedback menu to provide feedback to the developers.

Add buttons for records and fields are enabled only when appropriated.

Commented most of the code (classes and controllers) using headerDoc.


Version 0.6 (2008/02/24):

Close the application when the last window is closed.

Modified the collapsing of the group subview. Now it only collapses when the toolbar button is pressed.
Now when dragging the other panels, the group panel does not collapse when its size drops below its minimal size.

Connected the toolbar button to collapse/expand the group panel.


Version 0.5 (2008/02/23):

Intial release.
